// BackScrollイベント処理
function doBackScroll() {
    var pos = getScrollPosition();
    window.scrollTo(Math.max(Math.floor(pos.x / 2),0), Math.max(Math.floor(pos.y - (pos.y / 5)),0));
    if (pos.x > 0 || pos.y > 0) {
        window.setTimeout("doBackScroll()", 35);
        return false;
    }
}

// スクロール量を取得し、オブジェクトとして返す
function getScrollPosition() {
    var obj = new Object();
    obj.x = document.body.scrollLeft || document.documentElement.scrollLeft;
    obj.y = document.body.scrollTop || document.documentElement.scrollTop;
    return obj;
}

// イベントリスナー登録
function addListener(elem, eventType, funcRef, capture) {
    if(!elem) { return false; }

    if(elem.addEventListener) {
        elem.addEventListener(eventType, funcRef, capture);
    } else if(elem.attachEvent) {
        elem.attachEvent('on' + eventType, funcRef);
    } else {
        return false;
    }
    return true;
}

// WindowLoad時の処理
function addLoadListener(e) {
    addListener(document.getElementById('Back2Top'), 'click', doBackScroll, false);
}

// WindowLoadのイベントリスナーをセット
addListener(window, 'load', addLoadListener, false);