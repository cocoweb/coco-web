#! /usr/local/bin/perl
###########################################################################
# サイト内検索　ライト
# Ver 1.2
# http://cgi-garage.parallel.jp/
###########################################################################
use strict;
use CGI;
require "jcode.pl";
my $cgi = CGI::new();
### 設定部分 ##############################################################
push(my @admindata,&log_read('ret.dat')); #################################
my $kensakucount = "30";#### ページごとの検索数 ###########################
my $mojiretu = "200";#### 検索結果の表示文字数 ############################
my $mojisize = "0";#### 検索結果の表示文字サイズ #########################
my $tablesize = "100%";#### テーブルサイズ #################################
###########################################################################
my $kensakutext = $cgi->param('kensaku');
	&jcode'convert(\$kensakutext,'sjis');
$kensakutext =~ s/　/ /g;
unless($kensakutext){
	&kekkaprint;
}

my @searchtext = split(/ /,$kensakutext);
my $textcount = @searchtext;
my $kensakuradio = $cgi->param('kensakuradio');
my $point = $cgi->param('point');
unless($point){
	$point = 1;
}
my $point2 = $cgi->param('point2');
unless($point2){
	$point2 = $kensakucount;
}

my $printstr;
my $hitcount = 0;
my $count = 1;
for my $filename(@admindata){
	my @basyo;
	open(DAT,"<$filename")||die &errorprint("File Open Error!","ファイルが存在すれば、パーミッションの確認をしてください。<BR>$filename");
	my @data = <DAT>;
	close(DAT);
	my $data = join("",@data);

	my $title;
	if ($data =~ m|<TITLE>\s*(.+)\s*</TITLE>|i) {
		$title = $1;
	}
	unless($title){
		my @filename = split(/\//,$filename);
		$title = pop(@filename);
	}
	$data =~ s/\n/ /g;
	$data =~ s/<!--.+?-->//g;
	$data =~ s/<.+?>//g;
	my $frag = 0;
	my @textleng;
	for my $i(@searchtext){
		my $p = index($data,$i);
		if($p >= 0){
			$frag++;
			push(@textleng,length($i));
		}
	}

	if(($kensakuradio eq "or" && $frag ne 0) || ($kensakuradio eq "and" && $frag eq $textcount)){
		$hitcount++;
		if($hitcount >= $point && $hitcount <= $point2){
			$data =~ s/\t/ /g;
			$data =~ s/　//g;
			$data =~ s/ //g;

			for my $i(@searchtext){
				my $p = index($data,$i);
				if($p >= 0){
					push(@basyo,$p)
				}
			}

			my $strleng = length($data);
			$printstr .= "<DT><FONT size='$mojisize'> $hitcount, <A href='$filename'>$title</A></FONT>\n<br>";
			for(my $p = 0;$p < $frag;$p++){
				my $start;#検索スタート場所
				my $end;
				my $beforestr;
				if(($basyo[$p]-10) < 0){
					$start = 0;
					$beforestr = 10 + ($basyo[$p] - 10);
				}
				else{
					$start = $basyo[$p]-10;
					$beforestr = 10;
				}
				$printstr .= "<DD><FONT size='$mojisize'> ・ .... ".substr($data,$start,$beforestr);
				$printstr .= "<FONT COLOR=\"RED\"><B>" . substr($data,$start+$beforestr,$textleng[$p]) . "</B></FONT>";
				$printstr .= substr($data,$start+$beforestr+$textleng[$p],$mojiretu-10) . " \n";
			}
			$printstr .= " ....</FONT><BR><BR>\n";
			$count++;
		}
	}
}
$printstr .= "</TD></TR><TR><TD align='center' colspan='2'><BR><FONT size='$mojisize'>検索結果ページ:  ";
my @cputimes = times;
my $pagecount = int($hitcount/$kensakucount);
if(($hitcount%$kensakucount) ne 0){
	$pagecount++;
}
my $pointcount = ($point + $kensakucount - 1)/$kensakucount;
for(my $i = 1;$i <= $pagecount;$i++){
	if($pointcount eq $i){
		$printstr .= "<FONT size='$mojisize'>[ $i ] </FONT>";
	}
	else{
		my $po1 = $kensakucount*($i-1)+1;
		my $po2 = $kensakucount*$i;
		$printstr .= "<FONT size='$mojisize'><A href='ret.cgi?kensaku=$kensakutext&point=$po1&point2=$po2&kensakuradio=$kensakuradio'><B>[ $i ]</B></A> </FONT>";
	}
}
$printstr .= "</TD></TR></TBODY></TABLE>";
if($hitcount < $point2){
	$point2 = $hitcount;
}
my $finalprint = "<TABLE width='$tablesize' style='border-collapse: collapse;'><TBODY>\n"
				."<TR><TD bgcolor='#cccccc'><FONT size='$mojisize'>●　<B>$kensakutext</B> の検索結果</FONT></TD>\n"
				."<TD bgcolor='#cccccc' align='right'>"
				."<FONT size='$mojisize'><B>$hitcount</B>件中<B>$point 〜 $point2</B>件\表\示　( <B>$cputimes[0]</B> 秒)</FONT></TD></TR>\n"
				."<TR><TD colspan='2' style='border-left-width : thin;border-right-width : thin;border-bottom-width : thin;"
				."border-style : none solid solid solid;'><DL>\n" . $printstr . "<BR>\n"
				."<center><FONT size='$mojisize'><A href='http://cgi-garage.parallel.jp/'>CGI-GARAGE</A></FONT></center><BR>";
my $TEMP = &readtemp('ret.temp');
$TEMP =~ s/<!--PRINT-->/$finalprint/;
print "Content-type: text/html\n\n$TEMP";
exit;
1;

sub kekkaprint(){
	my $TEMP = &readtemp('ret.temp');
	$TEMP =~ s/<!--PRINT-->/<B>検索文字列を入力してください！<\/B>/;
	print "Content-type: text/html\n\n$TEMP";
	exit;
	1;
}

### 検索ファイルデータ取得 ##########################################
sub log_read(){
	my @readlogs;
	my($datfile) = @_;
	my $fc = 0;
	if(open(FP,"<$datfile")){
		&file_lock(*FP);
		while(my $line = <FP>){
			chomp($line);
			push(@readlogs,$line);
		}
		flock(FP,8);
		close(FP);
	}
	return @readlogs;
}

### テンプレートデータ取得 ##########################################
sub readtemp{
	my($template) = @_;
	open(DAT,"<$template")||die print "Content-type: text/html\n\ntemplateData Open Error!:テンプレートファイルが存在すれば、パーミッションの確認をしてください。<BR>$template";

	&file_lock(*DAT);
	my $data;
	while(my $line = <DAT>){
		$data .= $line;
	}
	close(DAT);
	return $data;
}

### ファイルロック ##############################################################
sub file_lock{
	local(*LOGS) = @_;
	eval{flock(LOGS,2);};
	if($@){
		print "Content-type: text/html\n\nflock Error!<BR>'flock'のコマンドが使えません。";
		close(DAT);
		exit;
	}
}
exit;
1;

sub errorprint{
	my ($title,$erstr) = @_;
	print "content-type:text/html\n\n";
	print "<HTML>\n<HEAD>\n";
	print "<LINK rel=stylesheet href=cgigarage.css type=text/css>\n";
	print "<TITLE>$title</TITLE>\n</HEAD>\n";
	print "<BODY>\n<H1>$title</H1>\n";
	print "<P>$erstr<BR></P>\n<HR>\n";
	print "<A href=http://cgi-garage.parallel.jp/>CGI-Garage</A>\n";
	print "</BODY>\n</HTML>\n";
	exit;
}
