#! /usr/bin/perl

#===============================================================================
# Hi Library Ver 1.09
#                                 Copyright(C) 2003 Hisashi All Rights Reserved.
#===============================================================================
# <更新履歴>
# Date		Ver.	Memo
# ------------------------------------------------------------------------------
# 2003.05.12	1.09	エラー画面の著作権表示を削除
# 2003.03.31	1.08	デバッグフラグが立っている場合のみログを出力する
# 2003.03.26	1.07	エラー画面(errhtml)のレイアウトを変更
# 2003.02.12	1.06	デバッグログにファイル名・行数を表示するように変更
#			終了処理を削除
# 2003.02.07	1.05	クッキー保存・取得処理追加
# 2003.01.31	1.04	エラー画面出力処理追加
# 2003.01.15	1.03	ユーザ情報ログ取得処理追加
#			デバッグログ取得処理にて最大出力ログ行数の設定追加
# 2002.12.09	1.02	デコード処理にてPOST時の処理を追加
#			曜日算出処理追加
# 2002.12.03	1.01	ファイルロック・ロック解除処理追加
# 2002.12.01	1.00	ローカル公開

# 初期設定
$inflogmax  = 100;		# ユーザ情報ログ最大行数
$usrinf_log = "usrinf.log";	# ユーザ情報ログファイル名
$dbglogmax  = 100;		# デバッグログ最大行数
$dbg_log    = "dbg.log";	# デバッグログファイル名

#********************************************************************************
#* デコード処理
#********************************************************************************
sub decode{
	local($name, $value, $buffer);

	if ($ENV{'REQUEST_METHOD'} eq "POST") {
		read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
	}else{
		$buffer = $ENV{'QUERY_STRING'};
	}

	@pairs = split(/&/,$buffer);
	foreach $pair (@pairs) {
		($name, $value) = split(/=/, $pair);
		$value =~ tr/+/ /;
		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		&jcode'convert(*value,'sjis');
		$value =~ s/&/&amp;/g;
#		$value =~ s/"/&quot;/g;
#		$value =~ s/</&lt;/g;
#		$value =~ s/>/&gt;/g;
		$FORM{$name} = $value;
	}
}#decode END

#********************************************************************************
#* 時刻取得処理（$dateに時刻を代入）
#********************************************************************************
sub gettime{
	$times = time;#18時間時差があるなら、$times = time+18*60*60とする。
	($sec,$min,$hour,$mday,$month,$year,$wday,$yday,$isdst) = localtime($times);
	$year = $year + 1900;
	$month++;
	$youbi  = ('Sun','Mon','Tue','Wed','Thu','Fri','Sat') [$wday];
	$youbi2 = ('日','月','火','水','木','金','土') [$wday];
	$date = sprintf("%04d/%02d/%02d %02d:%02d:%02d", $year, $month, $mday, $hour, $min, $sec);
	$date2 = sprintf("%04d年%d月%d日（%s）%02d:%02d:%02d", $year, $month, $mday, $youbi2, $hour, $min, $sec);
	$date3 = sprintf("%d/%d %02d:%02d:%02d", $month, $mday, $hour, $min, $sec);
	$yyyymm  = sprintf("%04d%02d", $year, $month);
	$nowdate = sprintf("%04d/%02d/%02d", $year, $month, $mday) ;
	$nowtime = sprintf("%02d:%02d:%02d", $hour, $min, $sec) ;
}#gettime END

#********************************************************************************
#* ファイルロック処理
#********************************************************************************
sub lock{

	$sleepcnt = 0;
	while(1){
		if (-e $_[0]) {
		# (ロックされている場合)
			# 最大リトライ回数を超えたらエラー画面表示
			if( $sleepcnt >= $sleepmax ){
				&errhtml("ただ今混み合っています。しばらくたってから再度実行して下さい。");
				last;
			}
			sleep(1);
			$sleepcnt = $sleepcnt + 1;
		}else{
		# (ロックされていない場合)
			mkdir("$_[0]",0777);
			last;
		}
	}

}# lock END

#********************************************************************************
#* ファイルロック解除処理
#********************************************************************************
sub unlock{

	rmdir("$_[0]");

}# unlock END

#********************************************************************************
#* 曜日算出処理
#********************************************************************************
sub whatday{
	local($yyyy, $mm, $dd) = @_;
	# 曜日算出
	if ($mm < 3) {--$yyyy; $mm+=12;}
	$wday=($yyyy+int($yyyy/4)-int($yyyy/100)+int($yyyy/400)+int((13*$mm+8)/5)+$dd)%7;
	$whatday1 = ('日曜日','月曜日','火曜日','水曜日','木曜日','金曜日','土曜日') [$wday];
	$whatday2 = ('日','月','火','水','木','金','土') [$wday];
	$whatday3 = ('Sun','Mon','Tue','Wed','Thu','Fri','Sat') [$wday];
}# whatday END

#********************************************************************************
#* ページ移動処理
#********************************************************************************
sub disp_page{

	local($jump_url) = @_;

	print <<END;
Content-type: text/html

<SCRIPT>
<!--
w = location.href = "$jump_url" ;
// -->
</SCRIPT>
END
	exit;

}# disp_page END

#********************************************************************************
#* ユーザ情報取得処理
#********************************************************************************
sub get_usrinf{

	&gettime;
	$ip1 = $ENV{'REMOTE_ADDR'};
	$ip2 = $ENV{'HTTP_X_FORWARDED_FOR'};

	$host1 = gethostbyaddr(pack('C4',split(/\./,$ip1)),2);
	$host2 = gethostbyaddr(pack('C4',split(/\./,$ip2)),2);

	if( $ip1 eq "" ){	$ip1 = "unknown";}
	if( $ip2 eq "" ){	$ip2 = "unknown";}
	if( $host1 eq "" ){	$host1 = "unknown";}
	if( $host2 eq "" ){	$host2 = "unknown";}

	$usrinf = "$date<>$ip1($host1)<>$ip2($host2)<>$ENV{'HTTP_USER_AGENT'}<>$ENV{'HTTP_REFERER'}<>\n";

	open(INF, $usrinf_log);
	@infdata = <INF>;
	close(INF);
	
	unshift(@infdata, $usrinf);
	
	if( @infdata > $inflogmax ){
		$x = pop(@infdata);
	}

	&lock("${usrinf_log}loc");	
	open(INF, ">$usrinf_log");
	print INF @infdata;
	close(INF);
	&unlock("${usrinf_log}loc");	

}# get_usrinf END

#********************************************************************************
#* デバッグ用ログ出力
#* Param1:出力ログメッセージ
#* Param2:ファイル名
#* Param3:行数
#********************************************************************************
sub dbglog{
	local($dbgmsg, $file, $line) = @_;

	if( $DBG ){
		&gettime;
		open(DBG, $dbg_log);
		@dbgdata = <DBG>;
		close(DBG);
		
		if( $file ne "" and $line ne "" ){
			unshift(@dbgdata, "[$date] $file:$line > $dbgmsg\n");
		}elsif( $file ne "" ){
			unshift(@dbgdata, "[$date] $file > $dbgmsg\n");
		}elsif( $line ne "" ){
			unshift(@dbgdata, "[$date] $line > $dbgmsg\n");
		}else{
			unshift(@dbgdata, "[$date] > $dbgmsg\n");
		}
		if( @dbgdata > $dbglogmax ){
			$x = pop(@dbgdata);
		}

		&lock("${dbg_log}loc");	
		open(DBG, ">$dbg_log");
		print DBG @dbgdata;
		close(DBG);
		&unlock("${dbg_log}loc");
	}
}

#********************************************************************************
#* エラー画面出力
#* Param1:エラーメッセージ
#* Param2:エラーファイル
#* Param3:エラー行数
#********************************************************************************
sub errend{

	local($errmsg, $errfile, $errline) = @_;

	print "Content-type: text/html\n\n";
	print <<"_HTML_";

<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>エラーが発生しました。</TITLE>
</HEAD>
<BODY background="$bgimg">
<font color="red">エラーが発生しました。</font><br>
<HR size="2" color="red" width="100%">
<CENTER>
<table width="100%" border="0">
<tr>
	<td nowrap width="20%" align="right">
		エラー内容：
	</td>
	<td nowrap width="80%">
		$errmsg
	</td>
</tr>
<tr>
	<td nowrap align="right">
		エラーファイル：
	</td>
	<td nowrap>
		$errfile
	</td>
</tr>
<tr>
	<td nowrap align="right">
		エラー行数：
	</td>
	<td nowrap>
		$errline
	</td>
</tr>
<tr>
	<td nowrap colspan="2" align="center">
		<br><br>
		※\申\し訳ありませんが、実行継続不可\能\なエラーが発生しました。<br>
		ＷＥＢ管理者かプログラム作成者までご連絡ください。
	</td>
</tr>
</table><br><br>

<INPUT type="button" name="return" value="戻る" onclick="history.back();">　　　
<INPUT type="button" name="home" value="ホーム" onclick="location='$home'">
</CENTER>
</BODY>
</HTML>

_HTML_

	exit;
}

#********************************************************************************
#* エラー画面出力
#* Param1:エラーメッセージ
#********************************************************************************
sub errhtml{

	local($errmsg) = @_;

	print "Content-type: text/html\n\n";
	print <<"_HTML_";

<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<META http-equiv="Content-Style-Type" content="text/css">
<TITLE>エラーが発生しました。</TITLE>
</HEAD>
<BODY>
<HR size="2" color="red" width="80%">
<br><br>
<CENTER>
<table width="100%" border="0">
<tr>
	<td nowrap align="center">
		$errmsg
	</td>
</tr>
</table>
<br><br>
<HR size="2" color="red" width="80%">
<br><br>

<INPUT type="button" name="return" value="戻る" onclick="history.back();">　　　
<INPUT type="button" name="home" value="ホーム" onclick="location='$home'">
</CENTER>
</BODY>
</HTML>

_HTML_

	exit;
}

#********************************************************************************
#* クッキー取得処理
#********************************************************************************
sub get_cookie{
	$cooks = $ENV{'HTTP_COOKIE'};
	($cookieid, $cooks) = split(/=/, $cooks);
	($count,$dmy) = split(/<>/, $cooks);
}# get_cookie END

#********************************************************************************
#* クッキー保存処理（3ヶ月間有効）
#********************************************************************************
sub set_cookie{
	&gettime;
	local($dmy,$mdc,$monc,$yrc,$wdayc,$mc,$yc);
	($dmy,$dmy,$dmy,$mdc,$monc,$yrc,$wdayc,$dmy,$dmy) = localtime($times + 7776000);
	$yc = ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') [$wdayc];
	$mc = ('Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec') [$monc];
	$yrc = $yrc+1900;	$mdc = "0$mdc" if ($mdc < 10);
	$count ++;
	$data = "$count<>chdend";
	print "Set-Cookie: hiwatchcookie=$data; expires=$yc, $mdc-$mc-$yrc 00:00:00 GMT\n";
}# set_cookie END


1;
