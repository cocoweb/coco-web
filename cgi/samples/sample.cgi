#!/usr/local/bin/perl5

if($ENV{'REQUEST_METHOD'} !~ /^GET$/i){

	print <<_ERROR_EOD_;
Content-Type: text/plain; charset=Shift_JIS

サポートされていない要求です．
_ERROR_EOD_

	exit 1;
}

$ipaddr = $ENV{'REMOTE_ADDR'};
$hostname = &resolv($ipaddr);
$date = &getlocaldatetime;

print <<_EOD_;
Content-Type: text/plain; charset=Shift_JIS

$ENV{'SERVER_NAME'} のサンプルCGI（ステータス表\示）

アクセスされてきた方の情報は以下のとおりです．

アクセスされてきた方のクライアントIPアドレス: $ipaddr
アクセスされてきた方のクライアントホスト名: $hostname
アクセスされてきた方のブラウザ情報: $ENV{'HTTP_USER_AGENT'}

サーバソ\フトウェアは $ENV{'SERVER_SOFTWARE'} です．
接続プロトコルは $ENV{'SERVER_PROTOCOL'} です．
現在のサーバ内日付および時刻は $date です．
_EOD_

exit 0;


sub getlocaldatetime {
	local(@week) = ('日','月','火','水','木','金','土');
	local($sec,$min,$hour,$mday,$mon,$year,$wday) = localtime;
	local($shortdate);

	$mon++;
	if ($min < 10) { $min = "0$min"; }
	if ($hour < 10) { $hour = "0$hour"; }
	if ($mon < 10) { $mon = "0$mon"; }
	if ($mday < 10) { $mday = "0$mday"; }
	$wday = @week[$wday];

	$year = $year + 1900;   # Y2K

	$shortdate = "$year/$mon/$mday($wday) $hour時$min分";

	return $shortdate;
}

sub resolv {
        local($x) = @_;
        local($ip,$addr);
        if($x=~/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/){
                $ip="$1.$2.$3.$4";
                $addr=(gethostbyaddr(pack('C4',$1,$2,$3,$4),2))[0];
                if($addr ne ""){
                        return $addr;
                }else{
                        return $ip;
                }
        }
        return $x;
}
