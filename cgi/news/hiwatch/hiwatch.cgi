#! /usr/bin/perl

#-------------------------------------------------------------------------------+
# Name : HiWatch
# Ver. : Ver 1.00
# Func : リアルタイムアクセス監視
# File : hiwatch.cgi
#                                  Copyright(C) 2003 Hisashi All Rights Reserved.
# 〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜〜
# <更新履歴>
# -Date-	-Version-	-Comment-
# 2003.05.13	1.00		公開
# 2003.05.12	0.00		新規作成
#-------------------------------------------------------------------------------+
# <Function List>
# sub main()
#-------------------------------------------------------------------------------+

require "./jcode.pl" ;
require "./hilib.pl" ;
require "./config.cgi" ;

#-------------------------------------------------------------------------------+
# バージョン
$ver = "1.00";
$DBG = 0;

#********************************************************************************
# メイン
#********************************************************************************
&decode();			# デコード処理
$mode    = $FORM{'mode'};	# 動作モード

if( $mode eq "master" ){	&master;	}	# 管理画面
&main;							# メイン画面
exit;


#********************************************************************************
#* メイン画面表示処理
#********************************************************************************
sub main{
	local($acsnum);			# アクセス人数

	# Cookie取得
	&get_cookie;
	# アクセスカウント
	$acsnum = &access;

	if( $cookieflg eq "on" ){
		# Cookie保存
		&set_cookie;
	}

	$reloadurl = "${url}$script";

	$message =~ s/xx/$acsnum/ig;
	$message =~ s/\@date\@/$date3/ig;

	print <<_HTML_;
Content-type: text/html; charset=Shift_JIS

<html><head>
<title>HiWatch</title>
<META http-equiv="Refresh" content="$reload;URL=$reloadurl">
<STYLE type="text/css">
<!--
A:link{text-decoration:none;}
A:visited{text-decoration:none;}
A:hover{text-decoration:underline;color:blue;background-color:#dae6d9;}
-->
</STYLE>
</head>
<body background="$bgimg" bgcolor="$bgcolor" text="$color" link="#666666" vlink="#666666" alink="#666666">
<font size="$size">$message　</font><font color=black size="1">(C)<a href="http://www.e-hws.net/" target=_blank>M･S</a>
</body>
</html>
_HTML_

	exit;
}# main END

#********************************************************************************
#* アクセスカウント処理
#********************************************************************************
sub access{

	$ip       = $ENV{'REMOTE_ADDR'};		# IPアドレス
	$host     = $ENV{'REMOTE_HOST'};		# ホスト名
	$agent    = $ENV{'HTTP_USER_AGENT'};		# ユーザエージェント

	$cookieflg = "off";

	# アクセスのないものは削除
	&chk_acs;

	# ファイルデータ取得
	open(IN, $acsf);
	@filedata = <IN>;
	close(IN);

	# 時刻取得
	&gettime;

	# 追加データ作成
	$str = "$ip<>$host<>$count<>$agent<>$times<><><><><>\n";
	# 同じアクセスデータがあるか検索
	$find = 0;
	foreach(@filedata){
		($wip, $whost, $wcount, $wagent, $wtimes, $dmy) = split(/<>/, $_);
		if( $wip eq $ip and $whost eq $host ){
			push(@new_data, "$wip<>$whost<>$count<>$agent<>$times<><><><><>\n");
			$find = 1;
		}else{
			push(@new_data, $_);
		}
	}

	# 同じアクセスデータがない場合は追加
	if( $find == 0 ){
		$cookieflg = "on";
		push(@new_data, $str);
	}

	# ファイル書き込み
	&lock("${acsf}loc");
	open(OUT, ">$acsf");
	print OUT @new_data;
	close(OUT);
	&unlock("${acsf}loc");

	# アクセス人数取得
	$num = @new_data;

	return($num)

}# access END

#********************************************************************************
#* アクセスチェック処理
#********************************************************************************
sub chk_acs{

	local(@filedata, @new_data);
	local($wip, $whost, $wcount, $wagent, $wtimes);

	&gettime;

	# ファイルデータ取得
	open(IN, $acsf);
	@filedata = <IN>;
	close(IN);

	foreach(@filedata){
		($wip, $whost, $wcount, $wagent, $wtimes, $dmy) = split(/<>/, $_);
		if( $wtimes + $intval < $times ){
		}else{
			push(@new_data, $_);
		}
	}

	# ファイル書き込み
	&lock("${acsf}loc");
	open(OUT, ">$acsf");
	print OUT @new_data;
	close(OUT);
	&unlock("${acsf}loc");

}# chk_acs END

#********************************************************************************
#* 管理画面処理
#********************************************************************************
sub master{

	$pass = $FORM{'pass'};		# 入力された管理パスワード

	# パスワードが入力されていなければ入力画面を表示
	if( $pass eq "" ){
		&enter;
	}

	# パスワードチェック
	if( $pass ne $password ){
		&errhtml("管理パスワードが違います。");
	}

	# アクセスのないものは削除
	&chk_acs;

	# ファイル取り込み
	open(IN, $acsf);
	@filedata = <IN>;
	close(IN);

	&gettime;
	$acsnum = @filedata;
	$reloadurl2 = "${url}$script?mode=master&pass=$pass";

	print <<_HTML_;
Content-type: text/html; charset=Shift_JIS

<html><head>
<title>HiWatch管理画面</title>
<META http-equiv="Refresh" content="$reload;URL=$reloadurl2">
<STYLE type="text/css">
<!--
A:hover{text-decoration:underline;color:blue;background-color:#dae6d9;}
-->
</STYLE>
</head>
<body link="#666666" vlink="#666666" alink="#666666">
<center>
	<font color="green" size=5">HiWatch管理画面</font><br><br>
	<font color="red">現在、このサイトにアクセスしているのは<b>$acsnum人</b>です。</font>　
	<input type="button" name="reload" value="Reload" onclick="location='$reloadurl2'"><br><br>
	[$date現在]<br><br>
	<table border="1">
	<tr>
		<td nowrap align="center" bgcolor="green"><font color="white">No.</font></td>
		<td nowrap bgcolor="green"><font color="white">IP Address</font></td>
		<td nowrap bgcolor="green"><font color="white">Host</font></td>
		<td nowrap bgcolor="green"><font color="white">Visited</font></td>
		<td nowrap bgcolor="green"><font color="white">User Agent</font></td>
		<td nowrap bgcolor="green"><font color="white">Last Access</font></td>
	</tr>
_HTML_
	$no = 0;
	foreach(@filedata){
		($wip, $whost, $wcount, $wagent, $wtimes, $dmy) = split(/<>/, $_);
		$no ++;
		($wsec,$wmin,$whour,$wmday,$wmonth,$wyear,$wwday,$wyday,$wisdst) = localtime($wtimes);
		$wyear = $wyear + 1900;
		$wmonth ++;
		$wdate = sprintf("%04d/%02d/%02d %02d:%02d:%02d", $wyear, $wmonth, $wmday, $whour, $wmin, $wsec);
		print <<_HTML_;
	<tr>
		<td nowrap align="center"><font color="blue">$no</font></td>
		<td nowrap><font color="blue">$wip</font></td>
		<td nowrap><font color="blue">$whost</font></td>
		<td nowrap align="center"><font color="blue">$wcount回</font></td>
		<td nowrap><font color="blue">$wagent</font></td>
		<td nowrap><font color="blue">$wdate</font></td>
	</tr>
_HTML_

	}
	if( $acsnum == 0 ){
		print <<_HTML_;
	<tr>
		<td nowrap colspan="6" align="center"><font color="red">アクセスしているユーザなし</font></td>
	</tr>
_HTML_
	}

	print <<_HTML_;
	</table>
	<br><br>
	<INPUT type="button" name="home" value="HOME" onclick="location='$home'">


<!-- 著作権表示は消さないで！！ -->
<br>
<hr color="gray" size="1" width="100%">
<br>
<div align="right"><font size="2" color="#666666">
<u>HiWatch Ver $ver (C)<a href="http://www.e-hws.net/" target=_blank>Million System</a></u></font>
</div>
<!-- 著作権表示は消さないで！！ -->


</body>
</html>
_HTML_

	exit;

}# master END

#********************************************************************************
#* 管理画面入室画面表示処理
#********************************************************************************
sub enter{

	print <<_HTML_;
Content-type: text/html; charset=Shift_JIS

<html><head>
<META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=x-sjis">
<title>HiWatch管理画面 Enter</title>
</head>
<BODY>
<center>
<font size="5" color="green">HiWatch管理パスワードを入力してください。</font><br>
<br>
<form action="$script" method="$method">
<input type="hidden" name="mode" value="master">
<table border="0">
	<tr>
		<td>
			Password：<input type="text" name="pass">　<input type="submit" name="send" value="Login">
		</td>
	</tr>
</table>
</form>


</center>
</BODY>
</html>
_HTML_

	exit;

}
