#! /usr/local/bin/perl

#↑各プロバイダで指定しているパスを記述
#
# Real_Time（フリーソフト）
$ver = 2.00;
# Last_Modified 2002/08/02
# Copyright (C) 2002 suepon , All rights reserved.
$Distribution = "http://CGIScriptMarket.com/";

#----------------------------------- 設定部ここから -----------------------------------#

# このファイルを呼び出すURL(http:// から)を記述
$script = "http://www.coco-web.com/cgi/real_time/real_time.cgi";

# 記録ファイル設置ディレクトリ（最後に / を付けないこと）
$tmpdir = "./temp";

# 記録ファイル
$file = "data.cgi";

# ファイルロック（簡易式）
#（0:off　1:on）
# アクセスの多いサイトで、記録ファイル破損の恐れがある場合はonにする。
$lock_mode = 0;

# gifcat(v.1.61で動作確認)
require "gifcat.pl";

#----------------------------- 以下、デフォルトの表示設定 -----------------------------#

# 呼び出し時にオプション指定する場合はこの部分の設定は必要有りません
# 呼び出し時にオプション指定しない場合はこの設定が有効になります。

# 画像設置ディレクトリ（最後に / を付けないこと）
$imgdir = "./image";

# 更新時間（秒）
$reload = 60;

# 前後のテキストに対する表示位置
# （1:上揃え　2:中揃え　3:下揃え　0:指定しない）
$align = 3;

# 画像の縦サイズ（指定しない場合は 0 ）
$height = 0;

#----------------------------------- 設定部ここまで -----------------------------------#

$now = time;
$addr = $ENV{'REMOTE_ADDR'};
$ver = sprintf("v.%.2f",$ver);

@buf = split(/&/,$ENV{'QUERY_STRING'});
foreach(@buf) {
	($n,$v) = split(/=/,$_);
	$in{$n} = $v;
}

if ($in{'image'}) { $imgdir = "./$in{'image'}"; }
if ($in{'reload'}) { $reload = $in{'reload'}; }
if ($in{'real_time'}) { $mode = 1; }

if ($mode == 1) {
	if ($lock_mode) { &lock; }
	open(DATA,"$tmpdir/$file") || &error("error");
	@data = <DATA>;
	close(DATA);

	foreach(@data) {
		$_ =~ s/\s//;
		($tm,$ad) = split(/\,/,$_);
		if ($ad ne $addr && $tm > $now - ($reload + 10)) { push(@new,"$_\n"); }
	}

	push(@new,"$now,$addr\n");
	$count = @new;

	open(DATA,"> $tmpdir/$file") || &error("error");
	print DATA @new;
	close(DATA);
	if ($lock_mode) { unlink("$tmpdir/lock"); }

	while (length($count)) {
		$count =~ s/(\d)//;
		push (@img,"$imgdir/$1.gif");
	}

	print "Content-type: image/gif\n\n";
	binmode(STDOUT);
	print &gifcat'gifcat(@img);

	exit;
}
else {
	if ($in{'align'}) { $align = $in{'align'}; }
	if ($align == 1) { $align = " align=top"; }
	elsif ($align == 2) { $align = " align=absmiddle"; }
	elsif ($align == 3) { $align = " align=absbottom"; }
	else { $align = ""; }

	if ($in{'size'}) { $height = " height=$in{'size'}"; }
	elsif ($height) { $height = " height=$height"; }
	else { $height = ""; }

	if ($in{'image'}) { $dir = "image=$in{'image'}&"; }

	$script = "$script?" . $dir . "real_time=";
	$alt = "Real_Time2 $ver Reload：$reload\min.";

	print "Content-type:＼n";
	print "Pragma:no-cache\n";
	print "Cache-Control:no-cache\n";
	print "Expires:Thu, 01 Dec 1994 16:00:00 GMT\n\n";
	print "/***************************************************\n";
	print "Real Time2 $ver \nCopyright (C) 2002 suepon , All rights reserved.\n";
	print "Script found at $Distribution\n";
	print "****************************************************/\n\n";
	print "document.write(\"<a href='$Distribution' target=_blank>\"\n";
	print ",\"<img name=real_time border=0 src='$script\"\n";
	print ",escape(new Date()),\"'$align$height alt='$alt'></a>\");\n\n";
	print "function rel() {\n\timg = \"$script\" + escape(new Date());\n";
	print "\tdocument.images[\"real_time\"].src = img;\n\tsetTimeout(\"rel()\",$reload * 1000);\n}\n\nrel();\n";

	exit;
}

sub lock {

	$retry = 3;
	$sleep = 2;

	if (-e "$tmpdir/lock") {
		$lockdate = (stat("$tmpdir/lock"))[9];
		if ($lockdate < $now - ($retry * $sleep)) {
			unlink("$tmpdir/lock");
		}
	}

	while ($retry --) {
		if ($retry < 1) { &error("busy"); }
		elsif (-e "$tmpdir/lock") { sleep($sleep); }
		else {
			open(LOCK,"> $tmpdir/lock");
			close(LOCK);
			last;
		}
	}
}

sub error {

	if ($_[0] eq 'error') {
		@array = (
		'47','49','46','38','39','61','50','00','14','00','80','00','00','00','00','00',
		'00','FF','00','2C','00','00','00','00','50','00','14','00','00','02','83','84',
		'8F','A9','CB','ED','0F','A3','9C','B4','DA','8B','B3','DE','BC','FB','EF','05',
		'A2','11','80','26','55','22','E9','C9','36','62','99','AE','EF','32','93','C0',
		'78','D4','F7','BD','AA','F8','0E','DB','F1','64','C2','9C','4D','48','54','19',
		'79','C7','24','AE','57','8C','49','8F','48','28','B2','B9','CC','C6','96','CE',
		'2C','75','FB','E5','D2','B0','C3','5F','38','4A','7A','81','8B','E9','5A','12',
		'CD','16','5F','19','5D','B2','92','8A','3F','C3','AD','F3','35','D4','09','56',
		'17','27','F8','B6','76','E7','67','96','F3','03','D3','A3','F3','A6','58','E8',
		'F3','68','C2','D7','62','39','76','99','A9','B9','C9','D9','E9','F9','09','FA',
		'51','00','00','3B');
	}
	else {
		@array = (
		'47','49','46','38','39','61','28','00','14','00','80','00','00','00','00','00',
		'00','FF','00','2C','00','00','00','00','28','00','14','00','00','02','4E','84',
		'8F','A9','CB','ED','0F','A3','9C','B4','DA','1B','83','DE','27','24','6F','6C',
		'5A','F7','29','20','09','9C','E1','8A','9C','EE','A2','A6','2C','1A','BF','73',
		'5B','CA','B8','CE','78','71','27','B2','A1','52','23','13','2F','47','43','02',
		'8D','BD','5C','AD','B9','83','39','A7','D1','9B','92','27','7C','56','3F','41',
		'6E','4D','C4','C4','88','C7','E4','B2','F9','8C','A6','14','00','00','3B');
	}

	print "Content-type:image/gif\n\n";
	foreach (@array) { $data = pack('C*',hex($_)); print $data; }

	exit;
}
