<?php

$name = '';
$title_gif = '';
$title = '';
$description = '';
$keywords = '';
$originalHTML = '';
$flg = false;
$file = './shop.xml';

//ＩＤで該当店舗の情報を検索する
if(file_exists($file) && isset($_GET['shopID']) && $_GET['shopID']!=''){
	//shop.xmlファイルの読み込み
	$xml = simplexml_load_file($file);
	$shopID = $_GET['shopID'];
	foreach($xml -> shop as $shop){
		if($shopID == $shop['id']){
			$name = $shop['name'];
			$title_gif = $shop['title_gif'];
			$title = $shop -> title;
			$description = $shop -> description;
			$keywords = $shop -> keywords;
			$originalHTML = $shop -> originalhtml;
			
			//eriaの配列作成
			$erias = array();
			foreach($shop -> erias -> eria as $eria){
				$erias[] = array(
				$eria['name'],
				$eria
				);
			}
			foreach($erias as $key => $row){
				$hoge[$key] = $row[0];
			}
			//県名でソート
			array_multisort($hoge,SORT_DESC,$erias);

			//出張地域HTML作成
			$strHTML = '';
			foreach($erias as $key => $array_eria){
				$strHTML .= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"municipal\">\r\n";
				$strHTML .= "<tr><td colspan=\"6\" class=\"townT\">出張可能な都市</td></tr>\r\n";
				$strHTML .= "<caption>". $array_eria[0] ."</caption>\r\n";
	
				//市区町村名の配列作成
				$city = explode(',',$array_eria[1]);
				sort($city);

				$intcity = 0;
				foreach($city as $key => $cityname){
					if($intcity == 0){$strHTML .= "<tr>\r\n";}
					if(isset($cityname) && $cityname != ""){
						$intcity++;
						$strHTML .= "<td class=\"border01\">". $cityname ."</td>\r\n";
					}
					if($intcity == 6){$strHTML .= "</tr>\r\n";$intcity = 0;}
				}

				$intcity = 6 - $intcity;
				if($intcity!=0){
					$strHTML .= "<td class=\"border01 bgCnone\" colspan=\"". $intcity ."\">&nbsp;</td>\r\n";
					$strHTML .= "</tr>\r\n";
				}
				$strHTML .= "</table>\r\n";

			}
			$flg = true;
		}//if($shopID == $shop['id'])
			
		}//foreach($xml -> shop as $shop)
}
?>
<?php print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php print $title; ?></title>
<meta name="description" content="<?php print $description; ?>" />
<meta name="keywords" content="<?php print $keywords; ?>" />
<meta http-equiv="content-script-type" content="text/javascript" />
<link rel="stylesheet" href="../css/shop.css" type="text/css" media="all" />
<link rel="index" href=".././index.html" />
<script src="../Scripts/top.js" type="text/javascript"></script>
<script src="../js/swfobject.js" type="text/javascript"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script type="text/javascript" src="../Scripts/jquery.galleria.js" charset="UTF-8"></script>
<script type="text/javascript" src="../js/cAutoScroll.js"></script>
	<script type="text/javascript" charset="UTF-8">
	
	$(document).ready(function(){
		
		$('.gallery_demo_unstyled').addClass('gallery_demo'); // adds new class name to maintain degradability
		
		$('ul.gallery_demo').galleria({
			history   : true, // activates the history object for bookmarking, back-button etc.
			clickNext : true, // helper for making the image clickable
			insert    : '#main_image', // the containing selector for our main image
			onImage   : function(image,caption,thumb) { // let's add some image effects for demonstration purposes
				
				// fade in the image & caption
				image.css('display','none').fadeIn(1000);
				caption.css('display','none').fadeIn(1000);
				
				// fetch the thumbnail container
				var _li = thumb.parents('li');
				
				// fade out inactive thumbnail
				_li.siblings().children('img.selected').fadeTo(500,0.3);
				
				// fade in active thumbnail
				thumb.fadeTo('fast',1).addClass('selected');
				
				// add a title for the clickable image
				image.attr('title','Next image >>');
			},
			onThumb : function(thumb) { // thumbnail effects goes here
				
				// fetch the thumbnail container
				var _li = thumb.parents('li');
				
				// if thumbnail is active, fade all the way.
				var _fadeTo = _li.is('.active') ? '1' : '0.3';
				
				// fade in the thumbnail when finnished loading
				thumb.css({display:'none',opacity:_fadeTo}).fadeIn(1500);
				
				// hover effects
				thumb.hover(
					function() { thumb.fadeTo('fast',1); },
					function() { _li.not('.active').children('img').fadeTo('fast',0.3); } // don't fade out if the parent is active
				)
			}
		});
	});
	
	</script>



<!--[if lte IE 7]>
<style type="text/css">
menu ul li{
	display:inline;
	/*float:left;*/
}
</style>
<![endif]-->
<!--AZ-->
<script type="text/javascript"><!--
(function() {
var p=(('https:'==document.location.protocol)?'https:':'http:');
var s=p+'//www.accanalyze.com/js/tracking.js';
document.write('<scr'+'ipt src="'+s+'?ti=2553&');
document.write('referrer='+encodeURIComponent(document.referrer)+'&');
document.write('width='+screen.width+'&');
document.write('height='+screen.height+'&');
document.write('color='+screen.colorDepth+'"></scr'+'ipt>');
})();
// -->
</script>
</head>

<body>
<div id="container"><!-- #BeginLibraryItem "/Library/header.lbi" --><div id="header">
			<div id="header_top">
			  <a href="../index.html"><img src="../img/logo.gif" width="192" height="104" id="site_logo" alt="カーテンココ" name="site_logo" /></a>	
				<div id="logo2">
					<h1><?php print $name ?></h1>
				</div>
				
				<div id="header_right1">
					<ul>
					<li id="header_right01"><a href="../mitsumori.html">各種お問合せ</a></li>
					<li id="header_right02"><a href="../sitemap.html">サイトマップ</a></li>
					</ul>
				</div><!--メニューサイトマップ他-->	
				<img src="../img/free_dial.gif" width="265" height="23" class="right" alt="フリーダイヤル0120-499-455/AM7:00～AM0:00" />
				
				<div id="search"><!--サイト内検索-->
		  		<form action="http://www.gaichu110.com/search_lite/ret.cgi" method="post" id="serch" name="serch">
				<input name="kensaku" size="16" value="サイト内検索" tabindex="0" accesskey="a" onFocus="if (this.value == 'サイト内検索') this.value = ''; this.style.background = '#fff';" onBlur="if (this.value == '') this.value = 'サイト内検索'; this.style.background = '#f1f1f1';" />
				<input type="image" src="../img/search_button1.gif" onmouseover="this.src='img/search_button2.gif'" onmouseout="this.src='img/search_button1.gif'" alt="検索" value="" tabindex="4" accesskey="b" align="middle" />  
				</form>
  				</div><!--ここまでサイト内検索-->
				
  </div><!--ヘッダー上-->	
			
			<div id="top_menu">
				<ul>
				<li id="top_menu01"><a href="../index.html">HOME</a></li>
				<li id="top_menu02"><a href="../30min_coco.html">カーテンココって？</a></li>
				<li id="top_menu03"><a href="../flow.html">お申し込みの流れ</a></li>
				<li id="top_menu04"><a href="../area/new_area.html">出張エリア</a></li>
				<li id="top_menu05"><a href="../voice.html">お客様の声</a></li>
				</ul>
			</div>
			<!--トップメニュー-->
		</div><!--ヘッダー-->
	  <!-- #EndLibraryItem -->
      
      <div id="main2">
		<p id="pan"><a href="../">HOME</a>　&gt;　<a href="../area/new_area.html">出張可能エリア　&gt;　</a><?php print $name ?></p>
		<br />
 
<?php if($flg):?>
<img src="<?php print $title_gif ?>" alt="<?php print $name ?>" width="520" height="40" />
<div class="clear">&nbsp;</div>
<div class="clear">&nbsp;</div>
        <img src="../img/title.jpg" alt="スタッフタイトル" width="525" height="52" />
<div id="mov1" class="imagediv" onmouseover="cLM1.Stop();" onmouseout="cLM1.Restart();" >
<ul>
	<li><img src="../img/staff/002.jpg" /></li>
	<li><img src="../img/staff/007.jpg" /></li>
	<li><img src="../img/staff/001.jpg" /></li>
	<li><img src="../img/staff/027.jpg" /></li>
	<li><img src="../img/staff/028.jpg" /></li>
	<li><img src="../img/staff/006.jpg" /></li>
	<li><img src="../img/staff/008.jpg" /></li>
	<li><img src="../img/staff/009.jpg" /></li>
	<li><img src="../img/staff/030.jpg" /></li>
	<li><img src="../img/staff/031.jpg" /></li>
	<li><img src="../img/staff/032.jpg" /></li>
	<li><img src="../img/staff/033.jpg" /></li>
	<li><img src="../img/staff/010.jpg" /></li>
	<li><img src="../img/staff/011.jpg" /></li>
	<li><img src="../img/staff/017.jpg" /></li>
	<li><img src="../img/staff/018.jpg" /></li>
	<li><img src="../img/staff/019.jpg" /></li>
	<li><img src="../img/staff/022.jpg" /></li>
	<li><img src="../img/staff/023.jpg" /></li>
	<li><img src="../img/staff/024.jpg" /></li>
	<li><img src="../img/staff/025.jpg" /></li>
	<li><img src="../img/staff/sapporo00.jpg" /></li>
	<li><img src="../img/staff/miyagi00.jpg" /></li>
	<li><img src="../img/staff/miyagi02.jpg" /></li>
	<li><img src="../img/staff/miyagi03.jpg" /></li>
	<li><img src="../img/staff/miyagi01.jpg" /></li>
	<li><img src="../img/staff/wakayama00.jpg" /></li>
	<li><img src="../img/staff/nagano00.jpg" /></li>
	<li><img src="../img/staff/tochigi00.jpg" /></li>
</ul>
</div>
<script type="text/javascript">
var cLM1 = new LM("mov1","cLM1");
cLM1.MoveSpeed = 1.5;
</script>
		<div id="area">
        <div class="title-changeability">
          <p id="Tbig"><?php print $name ?>の出張可能な地域</p>
        </div>

<!--出張可能エリア↓--> 
<?php print $strHTML ?>
<!--出張可能エリア↑--> 
</div>

<?php print $originalHTML ?>

<div id="flashContents">&nbsp;</div>
<div class="title-changeability"><p id="Tbig">私たちがカーテンココのオーダーカーテンを<br />ぜひオススメしたいお客様</p></div>
<p id="shop_staff">カーテンココは、以下のような方にオーダーカーテンのご利用をおすすめしています。10年～20年も使う商品ですので慎重に選びましょうね。</p>
<br />
<dl id="policyList">
<dt id="image01">「窓周りの正確な採寸やレール等の取り付けに自信がない」「不安だ」という方。</dt>
<dd>自分で採寸して失敗したというお客様がたくさんおられます。<br />
インテリアコーディネーター多数在籍の全国31加盟店オーダーカーテンのプロ技術にお任せ下さい。</dd>
<dt id="image02">「子供が小さいのでなかなか外出できない」「忙しくてショールームに行く時間がない」という方</dt>
<dd>ご自宅ならカーテン見本帳などでゆっくり選べます。</dd>
<dt id="image03">「生地やインテリア全体を、色んな相談・アドバイスを受けて決めたい」</dt>
<dd>インテリアコーディネーターの多数在籍しているカーテンココなら、プロの的確なアドバイスに定評があります。</dd>
<dt id="image04">「カーテンレールの取り付けが出来ないんだけど・・」</dt>
<dd>下地の問題や取付位置などカーテンレールも弊社のプロ施工スタッフにお任せ下さい。</dd>
<dt id="image05">「窓の数が多くて採寸や取り付けをとても自分達ではできない」</dt>
<dd>当店のお客様の60％は新築一戸建て。<br />対応スピード・実績数に自信がございます。</dd>
</dl>
<div class="center"><table border="0" summary="カーテンココの嬉しい特典！">
  <tr>
    <td><img src="../img/shop/tokuten.gif" alt="嬉しい特典！" width="96" height="89" /></td>
    <td>
	<ol>
		<li class="ol">出張＆採寸＆見積りまで<strong>無料</strong>です！</li>
		<li class="ol"><a href="../new_first.html#cushion">内覧同行サービス</a>が助かる！（一日80件まで可能）</li>
		<li class="ol"><a href="../new_first.html#cushion">クッションカバープレゼント</a>がうれしい！</li>
	</ol>
	
	</td>
  </tr>
</table>
</div>

<!--/お問い合わせはここから-->
<div id="s_moushikomi">
<p class="moushikomi2"><?php print $name ?>へお申し込みはこちらから</p>
<a href="../toiawase.html#saisun">出張・採寸のお申し込み</a>
<p class="moushikomi">※正式なご注文をいただいてから、約1週間から10日で取り付けいたします。<br />
　一部の商品によっては少々お時間を頂く事もございます。<br />
※土日祝などご希望される方は、当日や前日ですとお伺い出来ない場合もござい<br />
　ますので、事前にお知らせ下さい。</p>
</div>
<div id="tell_contact"><img src="../img/mazu.gif" alt="まずはお電話下さい" width="498" height="56" /></div>
<!--/フォームページへのボタン-->
<div id="web_tel">
<img src="../img/tel_call.gif" alt="お電話でもOK！0120-499-455（AM7：00～AM0：00）" width="354" height="130" />
</div>
<!--/WEB電話-->

<div id="page_up"><a href="./" onclick="backToTop(); return false" onkeypress="backToTop(); return false">ページの上へ</a></div>
<?php else: print "該当店舗がありません。"; ?>
<?php endif; ?>
</div><!--/メイン-->

<div id="left"><!--/レフト-->
<div id="left0">
<h2 id="left1">出張から採寸 見積りまで無料！</h2>
<div id="left2"><a href="../toiawase.html#saisun">出張・採寸のお申し込み</a></div><div class="hurry_bnr"><a href="../hurry.html"><img src="../img/hurry_banner.jpg" alt="オーダーカーテンをお急ぎの方へ" /></a></div>
<div id="clip1"> <a href="../iroha.html">カーテン選びのイロハ</a> </div>
<p class="left_chousei">オーダーカーテン と一口に言って<br />
もいろいろあるんですよ</p>
<ul class="left_listmenu">
<li><a href="../ka-ten.html">カーテンの種類</a></li>
<li><a href="../reru.html">カーテンレールの種類と特徴</a></li>
<li><a href="../heyaau.html">各部屋に合うカーテン</a></li>
<li><a href="../kinou.html">生地の機能</a></li>
<li><a href="../sousyoku.html">装飾の種類</a></li>
<li><a href="../oteire.html">お手入れについて</a></li>
</ul>
<div id="clip2"> <a href="../toritsuke.html">カーテン取付け施工例</a> </div>
<p class="left_chousei"> 採寸から取り付まで、カーテン・<br />
ココが施した事例です。</p>
<div id="clip12" class="leftNavi"><a href="../maker/">取り扱いメーカー</a></div>
<p class="left_chousei">カーテンココは国内外様々なメーカーを取り扱っております</p>
<div id="clip3"> <a href="../mihonchou.html">メーカーデジタルカタログ</a> </div>
<p class="left_chousei"> ネットで見られる！便利な見本帳<br />
をメーカー別にご紹介します。 </p>

<div id="clip5"> <a href="../area/new_area.html">出張エリア</a> </div>
<p class="left_chousei"> 現在お伺いできる地域です。</p>
<div id="clip6"> <a href="../voice.html">お客様の声</a> </div>
<p class="left_chousei"> 過分なお誉めの言葉を頂いており<br />
ます。ありがとうございます</p>
<div id="clip7"> <a href="../mascom.html">マスコミ紹介実績</a> </div>
<p class="left_chousei"> カーテンココは多くのメディア<br />
で紹介されています</p>
<div id="clip8"> <a href="../showroom_main.html">ショールームのご案内</a> </div>
<p class="left_chousei"> お時間のある方は是非どうぞ</p>
<div id="clip10"> <a href="../btob.html">法人・企業の方へ</a> </div>
<p class="left_chousei mgn0">法人向け販売も受付けております。<br />まずはご相談ください</p>
</div>
<!--レフト背景付-->

<div id="left_bottom">&nbsp;</div>
<!--レフト背景エンド-->
<!--△△△leftNavi End△△△-->

<table class="tbl" border="0" cellpadding="0" cellspacing="0" width="185" summary="カウンター">         
<tr>
<td class="text">ご来店総数</td>
<td class="text2"><img src="http://www.coco-web.com/cgi/daycount/daycount.cgi?gif" width="63" height="11" alt="ご来店総数" /></td>
<td class="text">名様</td>
</tr>
<tr>
<td class="text">昨日のご来店</td>
<td class="text2"><img src="http://www.coco-web.com/cgi/daycount/daycount.cgi?yes" width="45" height="11" alt="昨日のご来店" /></td>
<td class="text">名様</td>
</tr>
<tr>
<td class="text">本日のご来店</td>
<td class="text2"><img src="http://www.coco-web.com/cgi/daycount/daycount.cgi?today" width="45" height="11" alt="本日のご来店" /></td>
<td class="text">名様</td>
</tr>
</table>

<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 10,
  interval: 4000,
  width: 190,
  height: 600,
  theme: {
    shell: {
      background: '#990000',
      color: '#ffffff'
    },
    tweets: {
      background: '#fff2ff',
      color: '#52484c',
      links: '#990000'
    }
  },
  features: {
    scrollbar: true,
    loop: true,
    live: true,
    hashtags: true,
    timestamp: true,
    avatars: true,
    behavior: 'default'
  }
}).render().setUser('curtainCOCO').start();
</script>

<div id="blog" class="bnr"><a href="../jump.php?01" target="_blank">ココブログ</a></div>
<div id="qrcode">
<img src="../img/qrcode_image.gif" alt="携帯サイト" width="190" height="190" />
</div>
<div><a href="http://www.tempokagu.com" target="_blank"><img id="tempo_b" src="../img/tempo_banner.jpg" alt="店舗家具ドットコム" style="width:190;height:150;"/></a></div>


</div><!--レフト-->


<!-- #BeginLibraryItem "/Library/footer.lbi" --><div id="footer">
	<ul>
		<li id="footer_menu1"><a href="../kaisha.html">会社概要</a></li>
		<li id="footer_menu2"><a href="../hyoji.html">特定商取引法の表示</a></li>		
		<li id="footer_menu3"><a href="../kameiten.html">加盟店募集</a></li>
		<li id="footer_menu4"><a href="../privacy.html">プライバシーポリシー</a></li>
		<li id="footer_menu5"><a href="../mitsumori.html">各種お問い合せ</a></li>
		<li id="footer_menu6"><a href="../link/link.html">リンク集</a></li>
		<li id="footer_menu7"><a href="../staff.html">採用情報</a></li>
	</ul>
		<p>copyright(c)2000-2014cartain coco all rights reserved.<br />
Produced by <a href="http://www.townet.jp/" rel="nofollow" title="タウネットワン" target="_blank">タウネットワン</a><br />当サイトの全ての画像・文章の無断転載・複製を禁止します</p>
	
	</div><!-- #EndLibraryItem --><!--/フッター-->
</div>
<!--コンテナー-->

<script type="text/javascript">
	// <![CDATA[
	var so = new SWFObject("swf/staff.swf", "staffimage", "525px", "183ppx", "8", "#FFF");
	so.write("flashContent"); 
	// ]]>
</script>
<script type="text/javascript">
	// <![CDATA[
	var so = new SWFObject("swf/suminoe.swf", "flashimage", "500px", "320ppx", "8", "#FFF");
	so.write("flashContents"); 
	// ]]>
</script>

<!-- ここからGoogleAnalystics記述 //-->		
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-745009-1";
urchinTracker();
</script>
<!-- ここまでGoogleAnalystics記述 //-->		

<!-- Google Code for &#12522;&#12510;&#12540;&#12465;&#12486;&#12451;&#12531;&#12464; &#12479;&#12464; -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1072435630;
var google_conversion_label = "5YHxCJSH-AQQrqOw_wM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1072435630/?value=0&amp;label=5YHxCJSH-AQQrqOw_wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
