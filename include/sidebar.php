<?php


$string1 = <<< EOM

<div id="left">
	<p><a href="toiawase.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_measurements.gif" alt="出張から採寸、見積りまで無料！ 出張・採寸のお申し込み" /></a></p>
	<p><a href="showroom_main.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_showroom.gif" alt="ショールーム見学お申し込み" /></a></p>
    <div id="side-news">
    	<img src="http://genius-test.sakura.ne.jp/1409coco/img/side_ttl_news.gif" alt="お知らせ" />
        <ul>
        <li><a href="#">臨時休業のお知らせ</a></li>
        <li><a href="http://www.coco-web.com/staff.html">スタッフ募集のお知らせ</a></li>
        <li><a href="http://www.coco-web.com/kameiten.html">加盟店募集のお知らせ</a></li>
        </ul>
    </div>
    <p><a href="hurry.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/hurry_banner.jpg" alt="オーダーカーテンをお急ぎの方へ" /></a></p>
    <p><a href="new_first.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_01.jpg" alt="初めての方へ" /></a></p>
    <p><a href="new_first.html#Tailored"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_02.jpg" alt="オーダーカーテンとは?" /></a></p>
    <p><a href="faq/faq.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_03.jpg" alt="よくある質問と回答" /></a></p>
    <p><a href="iroha.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_04.jpg" alt="カーテン選びのイロハ" /></a></p>

<ul class="left_listmenu">
	<li><a href="ka-ten.html">カーテンの種類</a></li>
	<li><a href="reru.html">レールの種類</a></li>
	<li><a href="heyaau.html">お部屋に合うカーテン選び</a></li>
	<li><a href="kinou.html">生地の機能</a></li>
	<li><a href="sousyoku.html">装飾の種類</a></li>
    <li><a href="oteire.html">お手入れについて</a></li>
	<li><a href="word/word-a.html">用語集</a></li>
</ul>

    <p><a href="toritsuke.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_05.jpg" alt="カーテン取付施工事例" /></a></p>
    <p><a href="maker"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_06.jpg" alt="取り扱いメーカー" /></a></p>
    <p><a href="mihonchou.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_07.jpg" alt="デジタルカタログ" /></a></p>
    <p><a href="area/new_area.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_08.jpg" alt="無料出張可能エリア" /></a></p>

<p>全国の店舗</p>
<ul class="left_listmenu">
<li><a href="shop/k-sapporo.html">カーテンココ札幌店</a></li>
<li><a href="shop/k-aomori.html">カーテンココ青森店</a></li>
<li><a href="shop/k-sendaikita.html">カーテンココ仙台北店</a></li>

<li><a href="shop/k-miyagi.html">カーテンココ宮城店</a></li>
<li><a href="shop/k-hukushima.html">カーテンココ会津若松店</a></li>
<li><a href="shop/k-fukushimaiwaki.html">カーテンココ福島いわき店</a></li>
<li><a href="shop/k-ibaraki.html">カーテンココ茨城店</a></li>

<li><a href="shop/k-tokyo.html">カーテンココ東京本部</a></li>
<li><a href="shop/k-yokohama.html">カーテンココ横浜店</a></li>
<li><a href="shop/k-honbu.html">カーテンココ千葉・船橋店</a></li>
<li><a href="shop/k-chibakita.html">カーテンココ千葉北店</a></li>
<li><a href="shop/k-saitama.html">カーテンココさいたま店</a></li>
<li><a href="shop/k-gunma.htm">カーテンココ群馬店</a></li>
<li><a href="shop/k-nagano.html">カーテンココ長野店</a></li>
<li><a href="shop/k-yamanashi.html">カーテンココ山梨店</a></li>
<li><a href="shop/k-totigi.html">カーテンココ栃木宇都宮店</a></li>
<li><a href="shop/k-ni-gata.html">カーテンココ新潟店</a></li>
<li><a href="shop/k-nagaoka.html">カーテンココ長岡店</a></li>


<li><a href="shop/k-kitakinki.html">カーテンココ北近畿店</a></li>
<li><a href="shop/k-toyama.html">カーテンココ富山・石川店</a></li>
<li><a href="shop/k-shizuoka.html">カーテンココ静岡中央店</a></li>
<li><a href="shop/k-fujinomiya.html">カーテンココ静岡富士宮店</a></li>
<li><a href="shop/k-nagoya.html">カーテンココ名古屋店</a></li>
<li><a href="shop/k-kyoto.html">カーテンココ京都店</a></li>
<li><a href="shop/k-osaka.html">カーテンココ大阪・神戸</a></li>
<li><a href="shop/k-okayama.html">カーテンココ岡山店</a></li>
<li><a href="shop/k-yamaguchi.html">カーテンココ山口店</a></li>
<li><a href="shop/k-fukuoka.html">カーテンココ福岡店</a></li>
<li><a href="shop/k-ooita.html">カーテンココ大分店</a></li>
<li><a href="shop/k-kagoshima.html">カーテンココ鹿児島店</a></li>
</ul>

    <p><a href="kameiten.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_09.jpg" alt="オーダーカーテン販売加盟店募集中！" /></a></p>
    <p><a href="voice.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_10.jpg" alt="お客様の声" /></a></p>
    <p><a href="mascom.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_11.jpg" alt="マスコミ紹介実績" /></a></p>
    <p><a href="showroom_main.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_12.jpg" alt="ショールームご案内" /></a></p>
    <p><a href="btob.html"><img src="http://genius-test.sakura.ne.jp/1409coco/img/bnr_side_13.jpg" alt="法人・企業の方へ" /></a></p>
    <p>ホテル・旅館など業務用オーダーカーテンの法人向け販売も受付けております。<br />まずはご相談ください</p>		

<div class="visit"><script type="text/javascript" src="now_visitor/index.php"></script></div>
<div id="blog" class="bnr"><a href="jump.php?01" target="_blank">ココブログ</a></div>
<div id="qrcode">
<img src="http://genius-test.sakura.ne.jp/1409coco/img/qrcode_image.gif" alt="携帯サイト" width="190" height="190" />
</div>
<div><a href="http://www.tempokagu.com" target="_blank"><img id="tempo_b" src="http://genius-test.sakura.ne.jp/1409coco/img/tempo_banner.jpg" alt="店舗家具ドットコム" style="width:190;height:150;"/></a></div>

        
</div><!--//left-->


EOM;


$string1 = mb_convert_encoding($string1, "SHIFT-JIS", "auto");

echo $string1;
?>