#! /usr/local/bin/perl

#↑各プロバイダで指定しているパスを記述
#
# Real_Time v.1.0（フリーソフト）
# Last Modified:2001/06/04
# Copyright (C) 2001 suepon , All rights reserved.
# http://CGIScriptMarket.com/
#
#--------------------------------初期設定--------------------------------#

# このスクリプトの名前
$script = "http://www.coco-web.com/real_time/real_time.cgi";

# ログ書込用ファイル
$file = "real_time.dat";

# ログ書込用ファイルの設置ディレクトリ
# （基本設定のままならこのままでよい）
$tmp = "./temp";

# ファイルロック
# （人の出入りが頻繁にあるサイトや、カウントがおかしくなる場合は1にする）
$lock_mode = 0;	# ファイルロックOFF = 0  ファイルロックON = 1

# リアルタイムカウンターの<BODY>設定
$body = '<body text=#000000 bgcolor=#ffffff>';

# リアルタイムカウンターの<TABLE>設定
$table = "<table bgcolor=#fff0f5 border=0 cellspacing=0 cellpadding=3>";

# リロード（更新）時間(あまり小さくしないこと)
$reload = 60; #（秒で指定）

#----------------------------初期設定ここまで----------------------------#

$logout = $reload + 10;

($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);

$month = ($mon + 1);
if ($year < 2000) { $year+=1900; }
if ($month < 10) { $month = "0$month"; }
if ($mday < 10)  { $mday  = "0$mday";  }
if ($sec < 10)  { $sec =  "0$sec";  }
if ($min < 10)  { $min =  "0$min";  }
if ($hour < 10) { $hour = "0$hour"; }

$date_now = "$year$month$mday$hour$min$sec";
$times = "$hour時$min分$sec秒";
($lsec,$lmin,$lhour,$lmday,$lmon,$lyear,$lwday,$lyday,$lisdst) = localtime(time - $logout);

$lmonth = ($lmon + 1);
if ($lyear < 2000) { $lyear+=1900; }
if ($lmonth < 10) { $lmonth = "0$lmonth"; }
if ($lmday < 10)  { $lmday  = "0$lmday";  }
if ($lsec < 10)  { $lsec =  "0$lsec";  }
if ($lmin < 10)  { $lmin =  "0$lmin";  }
if ($lhour < 10) { $lhour = "0$lhour"; }

$date_last = "$lyear$lmonth$lmday$lhour$lmin$lsec";

$host = $ENV{'REMOTE_HOST'};
$addr = $ENV{'REMOTE_ADDR'};
if ($host eq '') { $host = $addr; }
if ($host eq $addr) { $host = gethostbyaddr(pack('C4',split(/\./,$host)),2) || $addr; }

if($lock_mode) { &lock1; }

open(READ,"$tmp$file") || &error("[ $file ]が開けませんでした。");
@lines = <READ>;
close(READ);

foreach $line (@lines) {

	($number,$log_host) = split(/\,/,$line);

	if ($log_host ne $host && $number > $date_last) { push(@new,$line); }

}

$value = "$date_now\,$host\,\n";
unshift(@new,$value);

if($lock_mode) { $lock = "$$\.lock"; }
else { $lock = $file; }

open(WRITE,"> $tmp$lock") || &error("[ $file ]が開けませんでした。");
print WRITE @new;
close(WRITE);

if($lock_mode) { &lock2; }

$count = @new;

print "Content-type:text/plain＼n";
print "<html>\n<head>\n";
print "<META HTTP-EQUIV=\"Content-type\" CONTENT=\"text/html; charset=x-sjis\">\n";
print "<META HTTP-EQUIV=\"refresh\" CONTENT=\"$reload; URL=$script\">\n";
print "</head>\n$body\n<center>\n";
print "$table<tr><td>\n";
print "<font size=2>現在 $count人の方が、<br>\n";
print "ここを訪れています<br>\@$times</font>\n";
print "</td></tr></table>\n</center>\n";
print "<div align=right><font size=2><a href=\"http://CGIScriptMarket.com/\" target=_blank>Real_Time</a></font></div>\n";
#　↑この１行著作表示に付き削除禁止
print "</body>\n</html>\n";

exit;

sub error {

	print "Content-type:text/plain＼n";
	print "<html>\n<head>\n";
	print "</head>\n$body\n<center>\n";
	print "<font size=1>エラー<br>$_[0]</font>\n";
        print "</center></body></html>\n";
        exit;

}
sub lock1 {

	local($list,@lists);
	local($retry) = 3;

	$list = `ls $tmp$ls` || &error("渋滞中！ファイルをロックできません");
	@lists = grep(/\.lock/,split(/\s+/,$list));

	while (@lists) {

		if (--$retry <= 0) {

			foreach (@lists) { unlink "$tmp$_" || &error("渋滞中！ファイルをロックできません"); }
			&error("渋滞中！ファイルをロックできません");
		}

		sleep(1);

		$list = `ls $tmp$ls` || &error("渋滞中！ファイルをロックできません");
		@lists = grep(/\.lock/,split(/\s+/,$list));
	}
}

sub lock2 {

	local($list,@lists);

	$list = `ls $tmp$ls` || &error("渋滞中！書込に失敗した可能性があります");
	@lists = grep(!/$lock/,grep(/\.lock/,split(/\s+/,$list)));

	if (@lists) {

		if (-e "$tmp$lock") { unlink("$tmp$lock"); }
		&error("渋滞中！書込に失敗した可能性があります");
	}

	rename("$tmp$lock","$tmp$file") || &error("渋滞中！書込に失敗した可能性があります");

}
