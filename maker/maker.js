[
{"division":"国内メーカー","corp":[
{
"name":"サンゲツ",
"kana":"サンゲツ",
"banner":"../img/m01_img.png",
"link":"sangetsu.html",
"info":"「トータルインテリア」という考え方にこだわり、様々な商品バリエーションを提案し、それらを自由にコーディネイトするメーカー。"
},
{
"name":"リリカラ",
"kana":"リリカラ",
"banner":"../img/m02_img.png",
"link":"lilycolor.html",
"info":"通産省選定グッドデザイン賞の家具・インテリア部門の大賞を受賞、以後8年連続選定した実績の高いメーカー。"
},
{
"name":"東リ",
"kana":"トリ",
"banner":"../img/m03_img.png",
"link":"toli.html",
"info":"創業は大正8年（1919年）。床材やカーペットをはじめ、壁紙、カーテンなど、インテリアの内外装をトータルに扱う総合メーカー。"
},
{
"name":"シンコール",
"kana":"シンコール",
"banner":"../img/m04_img.png",
"link":"sincol.html",
"info":"様々なコントラクトユースの全てに幅広く対応している、インテリア商品全般を扱う総合インテリアメーカー。"
},
{
"name":"アスワン",
"kana":"アスワン",
"banner":"../img/m05_img.png",
"link":"aswan.html",
"info":"常にオンリーワンの道を目指し、そのオリジナリティを高め続けることに企業の存在価値を求めているメーカー。"
},
{
"name":"ニチベイ",
"kana":"ニチベイ",
"banner":"../img/m06_img.png",
"link":"nichi-bei.html",
"info":"窓まわりから間仕切に至る豊富な商品ラインナップで、新築からリフォームまで、空間・人・モノが調和するようにコーディネートするメーカー。"
},
{
"name":"トーソー",
"kana":"トーソー",
"banner":"../img/m07_img.png",
"link":"toso.html",
"info":"1949年の設立以来、カーテンレールで国内シェアナンバーワンを守り続けてきた総合ウインドウトリートメントメーカー。"
},
{
"name":"MOLZA",
"kana":"モルザ",
"banner":"../img/m09_img.png",
"link":"molza.html",
"info":"和紙のアイデンティティをコアとして、より広い分野において創造と開発を進めていくメーカー。"
},
{
"name":"タチカワブラインド",
"kana":"タチカワブラインド",
"banner":"../img/m08_img.png",
"link":"tachikawablind.html",
"info":"「より快適な居住空間づくり」のためにユーザーの皆様の満足と厚い信頼を得られる製品開発を行うメーカー。"
},
{
"name":"川島織物セルコン",
"kana":"カワシマオリモノセルコン",
"banner":"../img/m10_img.png",
"link":"kawashima.html",
"info":"多様化するニーズに最適に応えられるよう、高質で快適な空間を提供するメーカー。"
},
{
"name":"キロニー",
"kana":"キロニー",
"banner":"../img/m12_img.png",
"link":"kirony.html",
"info":"創業大正3年の、インテリアの変遷、発展の歴史と共に歩み続けてきたブランドメーカー。"
},
{
"name":"スミノエ",
"kana":"スミノエ",
"banner":"../img/m11_img.png",
"link":"suminoe.html",
"info":"豊かな生活に潤いを与えるカーテンをはじめ、伝統と技術が織りなす緞帳、美術工芸織物などあらゆるニーズに応えるメーカー。"
},
{
"name":"五洋インテックス",
"kana":"ゴヨウインテックス",
"banner":"../img/m14_img.png",
"link":"goyo.html",
"info":"1979年の創立以来、窓とカーテンを通じてさまざまな提案をし続けているインテリアテキスタイルのメーカー。"
},
{
"name":"フェデポリマーブル",
"kana":"フェデポリマーブル",
"banner":"../img/m13_img.png",
"link":"fede.html",
"info":"常に時代と環境と人々の暮らしに直結した新しいインテリアの創造を目指すインテリアのメーカー。"
},
{
"name":"Nanik",
"kana":"ナニキ",
"banner":"../img/m16_img.png",
"link":"nanik.html",
"info":"ウッドブラインド、ウッドシャターのパイオニア、そしてリーディングカンパニーとして、高感度、高品質な製品を届けるメーカー。"
},
{
"name":"ファイバーアートステューディオ",
"kana":"ファイバーアートステューディオ",
"banner":"../img/m46_img.png",
"link":"fiberart.html",
"info":"職人によるオールハンドメイドで永く人に愛される製品を届けるメーカー。"
}

]
},

{"division":"海外ブランド","corp":[

{
"name":"maharamu",
"kana":"マハラム",
"banner":"../img/m20_img.png",
"link":"maharamu.html",
"info":"テキスタイルの生産を先駆け、柄、素材、技術の全てを駆使して常に斬新なテキスタイルを制作するブランド。"
},

{
"name":"CASSARO",
"kana":"カッサーロ",
"banner":"../img/m21_img.png",
"link":"cassaro.html",
"info":"他のブランドには見られない鮮やかで大胆な発色が、中東における貿易・商業の中心地としてのドバイの活気よさを連想させてくれるブランド。"
},

{
"name":"ODEGARD",
"kana":"オデガード",
"banner":"../img/m22_img.png",
"link":"odegard.html",
"info":"斬新なデザインで世界中のデザイナーから注目を浴びる、ニューヨークのブランド。"
},

{
"name":"ETAMINE",
"kana":"エタミン",
"banner":"../img/m29_img.png",
"link":"etamine.html",
"info":"あなたのインテリアを自然の香りが漂う上品な空間を作り出すブランド。"
},

{
"name":"Q-Designs",
"kana":"キューデザイン",
"banner":"../img/m31_img.png",
"link":"qdesigns.html",
"info":"パステルカラー中心とした色彩と、女性的で華やかなモチーフとの組み合わせで、気品の漂うアクセントを付加してくれるブランド。"
},

{
"name":"Colefax and Fowler",
"kana":"コールファックスアンドフォーラー",
"banner":"../img/m33_img.png",
"link":"colefax-fowler.html",
"info":"ファブリック、壁紙を世界で展開している有名ファブリックブランド。"
},

{
"name":"ANDREW MARTIN",
"kana":"アンドリューマーチン",
"banner":"../img/m35_img.png",
"link":"andrewmartin.html",
"info":"独特の色使い、テクスチャー感あるファブリックを用い、シンプルでありながら革新的なデザインを提供するブランド。"
},

{
"name":"HODSOLL MCKENZIE",
"kana":"ホッドソルマッケンジー",
"banner":"../img/m36_img.png",
"link":"hodsollmckenzie.html",
"info":"高品質な素材とうるさくない控えめなモチーフを巧みに組み合わせることで、時代を超えた高級感を演出するブランド。"
},

{
"name":"WARNER FABLICS",
"kana":"ワーナーファブリックス",
"banner":"../img/m37_img.png",
"link":"warnerfabrics.html",
"info":"パステル調の色彩と、優雅で気品にあふれるデザインが特徴のブランド。"
},

{
"name":"theBradleycollection",
"kana":"ザブラッドリーコレクション",
"banner":"../img/m38_img.png",
"link":"thebradleycollection.html",
"info":"スタイリッシュ、かつ普遍的、そして機能性も兼ね備えたカーテンポールブランド。"
},

{
"name":"BIGGIE BEST",
"kana":"ビギーベスト",
"banner":"../img/m42_img.png",
"link":"biggiebest.html",
"info":"数十人のデザイナー達が生み出す上質なファブリックからインテリア小物に至るまでトータルにコーディネイトするブランド。"
},

{
"name":"HAKS",
"kana":"ハックス",
"banner":"../img/m43_img.png",
"link":"haks.html",
"info":"他のブランドで有りそうでないギリシャ発のデザイン・素材感を提供するブランド。"
},

{
"name":"Ardecora",
"kana":"アルデコラ",
"banner":"../img/m45_img.png",
"link":"ardecora.html",
"info":"酔いしれるような美しさ、そして細部までこだわった華やかさを追求し、ブルジョワの香りを感じさせるブランド。"
}

]
}

]
