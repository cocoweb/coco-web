<?php
/*
Plugin Name: wp-hatena
Plugin URI: http://wppluginsj.sourceforge.jp/wp-hatena/
Description: エントリをはてなブックマーク等に追加するリンクタグを挿入します。
Author: hiromasa
Version: 0.93j
Author URI: http://hiromasa.zone.ne.jp/blog/
Special Thanks: Castaway. (http://bless.babyblue.jp/wp/)
Bug Report: Masayan (http://wp.mmrt-jp.net/)
Bug Report: kohaku (http://aoiro-blog.com/)
*/

/*  Copyright 2006 hiromasa  (email : webmaster@hiromasa.zone.ne.jp)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/******************************************************************************
 * 使い方 :
 *  プラグインを有効にした後 WP テーマ内の **エントリ表示位置** に、
 *   はてなの場合      : <?php if(isset($wph)) $wph->addHatena(); ?>
 *   del.icio.usの場合 : <?php if(isset($wph)) $wph->adddelicious(); ?>
 *  を挿入してください。
 *****************************************************************************/

/******************************************************************************
 * WpHatena function define.
 *****************************************************************************/
if(class_exists('WpHatena')) {
	
	$wph = & new WpHatena();
	
	// JavaScript 画像説明ポップアップ用
	// add_action('wp_head', array(&$wph, 'addScript'));
	
}

/******************************************************************************
 * WpHatena
 * 
 * @author		hiromasa
 * @version	0.93j
 * 
 *****************************************************************************/
class WpHatena {
	
	var $plugin_path;
	var $popup_jsname;
	var $blog_charset;
	
	/**
	 * The Constructor
	 * 
	 * @param none
	 * @return Object reference
	 */
	function WpHatena() {
		
		$this->plugin_path  = get_settings('siteurl');
		$this->plugin_path .= '/wp-content/plugins/wp-hatena/';
		$this->popup_jsname = 'popup.js';
		$this->blog_charset = get_settings('blog_charset');
		
	}
	
	/**
	 * WP interface.
	 * 
	 * @param none
	 * @return none (はてなブックマーク用のタグを echo)
	 */
	function addHatena() {
		
		echo
			$this->makeBookmarkURL(
				'はてなブックマーク',
				'http://b.hatena.ne.jp/append?',
				'hatena.gif',
				16, 12,
				''
			);
		
	}
	
	/**
	 * WP interface.
	 * 
	 * @param none
	 * @return none (del.icio.us 用のタグを echo)
	 */
	function adddelicious() {
		
		$title = $this->utf8_encode(get_the_title());
		
		echo
			$this->makeBookmarkURL(
				'del.icio.us',
				'http://del.icio.us/post?url=',
				'delicious.12px.gif',
				12, 12,
				'&amp;title=' . urlencode($title)
			);
		
	}
	
	/**
	 * Bookmark URL maker.
	 * 
	 * @param $sitename (サイト名称文字列)
	 * @param $url (URL)
	 * @param $iconfile (画像ファイル URL)
	 * @param $width (画像 width)
	 * @param $height (画像 height)
	 * @param $ext_url (その他の追加 URL)
	 * @return $tag (画像リンクタグ)
	 */
	function makeBookmarkURL($sitename, $url, $iconfile, $width, $height, $ext_url) {
		
		$tag  = '<a';
		$tag .= ' href="' . $url . get_permalink() . $ext_url . '"';
		$tag .= ' target="_blank"';
		$tag .= '>';
		$tag .= '<img';
		$tag .= ' src="' . $this->plugin_path . $iconfile . '"';
		$tag .= ' alt="このエントリを' . $sitename . 'に追加"';
		$tag .= ' title="このエントリを' . $sitename . 'に追加"';
		$tag .= ' width="' . $width . '" height="' . $height . '"';
		$tag .= ' style="border: 0;margin: 0;padding: 0;margin-right: 8px;vertical-align: baseline;" ';
		//$tag .= ' onmouseover="wpHatenaPopup()"';
		$tag .= '/>';
		$tag .= '</a>';
		
		return $tag;
		
	}
	
	/**
	 * UTF-8 encoder.
	 * 
	 * @param $text
	 * @return $text (UTF-8 に変換した文字列)
	 */
	function utf8_encode($text) {
		
		if(!preg_match ("/UTF-8/i", $this->blog_charset)) {
			if(function_exists('mb_convert_encoding')) {
				$text = 
					mb_convert_encoding(
						$text,
						'UTF-8',
						$this->blog_charset
					);
			}
		}
		
		return $text;
		
	}
	
	/**
	 * WP filter interface.(wp_head)
	 *  - 未使用 (popup.js 未実装のため)
	 * 
	 * @param none
	 * @return none (画像説明ポップアップ JavaScript を echo)
	 */
	function addScript() {
		
		echo '<script type="text/javascript"';
		echo ' src="'. $this->plugin_path . $this->popup_jsname . '"';
		echo '>';
		echo '</script>' . "\n";
		
	}
	
}
?>
