<?php
/*
Plugin Name: Title Suffix
Plugin URI: http://coderseye.com/files/wp-title-suffix
Description: Allows you to have a suffix at the end of your wp_title
Version: 1.0
Date: January 24, 2006
Author: Bruce Kroeze
Author URI: http://www.coderseye.com
*/

// change this to suit your needs
$add_suffix = ' - ';
$default_sep = "&raquo;";

function bjk_suffix_wp_title($text, $sep="") {

    global $add_suffix;
    global $default_sep;

    if (strlen($sep) == 0) {
      $sep = $default_sep;
    }

    $pos = strpos($text, $sep);
    if ($pos != FALSE) {
      $work = substr($text, $pos + strlen($sep));
      $text = $work;
    }
    if (strlen($text) > 0) {
        $text .= $add_suffix;
    }

    return $text;
}

add_filter('wp_title', 'bjk_suffix_wp_title');

?>
