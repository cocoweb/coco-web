<?php get_header(); $options = get_desing_plus_option(); ?>

 <div id="headline">
  <h2><?php _e('Search results for ', 'design-plus'); echo '[ <span id="keyword">' .$s. "</span> ]"; ?> - <?php $my_query =& new WP_Query("s=$s & showposts=-1"); echo $my_query->post_count; _e(' hit', 'design-plus'); ?></h2>
 </div><!-- END #archive_headline -->

 <div id="content" class="cf">

  <div id="left_col">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

   <div class="archive_post cf">

    <div class="post_info">
     <div class="title_area cf">
      <h3 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
      <p class="date"><?php the_time(__('m/d', 'design-plus')) ?></p>
     </div>
     <p class="excerpt"><?php new_excerpt(130); ?></p>
     <ul class="meta cf">
      <li class="post_category"><?php the_category(','); ?></li>
      <?php if ($options['show_tag']) : ?><?php the_tags('<li class="post_tag">',',','</li>'); ?><?php endif; ?>
      <?php if ($options['show_author']) : ?><li class="post_author"><?php the_author_posts_link(); ?></li><?php endif; ?>
      <?php if ($options['show_comment']) : ?><li class="post_comment"><?php comments_popup_link(__('Write comment', 'design-plus'), __('1 comment', 'design-plus'), __('% comments', 'design-plus')); ?></li><?php endif; ?>
     </ul>
     <a class="arrow_link" href="<?php the_permalink() ?>"><?php _e('Read more', 'design-plus'); ?></a>
    </div>

    <a class="image" href="<?php the_permalink() ?>"><?php if ( has_post_thumbnail()) { the_post_thumbnail('mid_size1'); } else { echo '<img src="'; bloginfo('template_url'); echo '/img/common/no_image1.gif" alt="" title="" />'; }; ?></a>

   </div><!-- END .archive_post -->

   <?php endwhile; else: ?>

   <p style="margin-bottom:35px;"><?php _e("Sorry, but you are looking for something that isn't here.","design-plus"); ?></p>

   <?php endif; ?>

   <?php include('navigation.php'); ?>

  </div><!-- #left_col -->

  <?php include('sidebar.php'); ?>

 </div><!-- END #content -->

<?php get_footer(); ?>