<?php

add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * オプション初期値
 * @var array 
 */
global $dp_default_options;
$dp_default_options = array(
	'pickedcolor' => '333333',
	'content_font_size' => '12',
	'show_author' => 1,
	'show_tag' => 1,
	'show_comment' => 1,
	'show_bookmark' => 1,
	'show_trackback' => 1,
	'show_next_post' => 1,
	'show_thumbnail' => 1,
	'layout'  => 'right',
	'twitter_url' => '',
	'facebook_url' => '',
	'custom_search_id' => '',
	'index_headline_latest_article' => __('Latest Article', 'design-plus'),
	'index_headline_recommend' => __('Recommend', 'design-plus'),
	'index_headline_recent_article' => __('Recent Article', 'design-plus'),
	'single_headline_related_article' => __('Related Article', 'design-plus'),
	'logotop' => 0,
	'logoleft' => 0,
	'google_ad1' => '',
	'ad_url1' => '',
	'ad_image1' => false,
	'google_ad2' => '',
	'ad_url2' => '',
	'ad_image2' => false,
	'google_ad3' => '',
	'ad_url3' => '',
	'ad_image3' => false
);

/**
 * Design Plusのオプションを返す
 * @global array $dp_default_options
 * @return array 
 */
function get_desing_plus_option(){
	global $dp_default_options;
	return shortcode_atts($dp_default_options, get_option('dp_options', array()));
}



// カラーピッカーの準備 javascriptの読み込み
add_action('admin_print_scripts', 'my_admin_print_scripts');
function my_admin_print_scripts() {
  wp_enqueue_script('jscolor', get_template_directory_uri().'/admin/jscolor.js');
  wp_enqueue_script('jquery.cookieTab', get_template_directory_uri().'/admin/jquery.cookieTab.js');
}



// 画像アップロードの準備
function wp_gear_manager_admin_scripts() {
wp_enqueue_script('dp-image-manager', get_template_directory_uri().'/admin/image-manager.js', array('jquery', 'jquery-ui-draggable', 'imgareaselect'));
}
function wp_gear_manager_admin_styles() {
wp_enqueue_style('imgareaselect');
}
add_action('admin_print_scripts', 'wp_gear_manager_admin_scripts');
add_action('admin_print_styles', 'wp_gear_manager_admin_styles');



// 登録
function theme_options_init(){
 register_setting( 'design_plus_options', 'dp_options', 'theme_options_validate' );
}


// ロード
function theme_options_add_page() {
 add_theme_page( __( 'Theme Options', 'design-plus' ), __( 'Theme Options', 'design-plus' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}


/**
 * レイアウトの初期設定
 * @var array 
 */
global $layout_options;
$layout_options = array(
 'right' => array(
  'value' => 'right',
  'label' => __( 'Right', 'design-plus' ),
  'img' => 'right_side'
 ),
 'left' => array(
  'value' => 'left',
  'label' => __( 'Left', 'design-plus' ),
  'img' => 'left_side'
 )
);



// テーマオプション画面の作成
function theme_options_do_page() {
 global $layout_options, $dp_upload_error;
 $options = get_desing_plus_option(); 

 if ( ! isset( $_REQUEST['settings-updated'] ) )
  $_REQUEST['settings-updated'] = false;


?>

<div class="wrap">
 <?php screen_icon(); echo "<h2>" . __( 'Theme Options', 'design-plus' ) . "</h2>"; ?>

 <?php // 更新時のメッセージ
       if ( false !== $_REQUEST['settings-updated'] ) :
 ?>
 <div class="updated fade"><p><strong><?php _e('Updated', 'design-plus');  ?></strong></p></div>
 <?php endif; ?>

 <?php /* ファイルアップロード時のメッセージ */ if(!empty($dp_upload_error['message'])): ?>
  <?php if($dp_upload_error['error']): ?>
   <div id="error" class="error"><p><?php echo $dp_upload_error['message']; ?></p></div>
  <?php else: ?>
   <div id="message" class="updated fade"><p><?php echo $dp_upload_error['message']; ?></p></div>
  <?php endif; ?>
 <?php endif; ?>
 
 <script type="text/javascript">
  jQuery(document).ready(function($){
   $('#my_theme_option').cookieTab({
    tabMenuElm: '#theme_tab',
    tabPanelElm: '#tab-panel'
   });
  });
 </script>

 <div id="my_theme_option">

 <div id="theme_tab_wrap">
  <ul id="theme_tab" class="cf">
   <li><a href="#tab-content1"><?php _e('Basic Setup', 'design-plus');  ?></a></li>
   <li><a href="#tab-content2"><?php _e('Logo', 'design-plus');  ?></a></li>
   <li><a href="#tab-content3"><?php _e('Banner', 'design-plus');  ?></a></li>
  </ul>
 </div>

<form method="post" action="options.php" enctype="multipart/form-data">
 <?php settings_fields( 'design_plus_options' ); ?>

 <div id="tab-panel">

  <!-- #tab-content1 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  -->
  <div id="tab-content1">

   <?php // サイトカラー ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Site color', 'design-plus');  ?></h3>
    <div class="theme_option_input">
     <input type="text" class="color" name="dp_options[pickedcolor]" value="<?php esc_attr_e( $options['pickedcolor'] ); ?>" />
     <input type="submit" class="button-primary" value="<?php echo __( 'Save Color', 'design-plus' ); ?>" />
     <p><?php _e('Sample Color', 'design-plus');  ?></p>
     <ul id="color_scheme" class="cf">
      <li class="black"><?php _e('Default', 'design-plus');  ?> ：333333</li>
      <li class="red"><?php _e('Red', 'design-plus');  ?> ：871818</li>
      <li class="purple"><?php _e('Purple', 'design-plus');  ?> ：585161</li>
      <li class="blue"><?php _e('Blue', 'design-plus');  ?> ：0e3647</li>
      <li class="green"><?php _e('Green', 'design-plus');  ?> ：1e471b</li>
     </ul>
    </div>
   </div>

   <?php // フォントサイズ ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Font size', 'design-plus');  ?></h3>
    <p><?php _e('Font size of single page and wp-page.', 'design-plus');  ?></p>
    <div class="theme_option_input">
     <input id="dp_options[content_font_size]" class="font_size" type="text" name="dp_options[content_font_size]" value="<?php esc_attr_e( $options['content_font_size'] ); ?>" /><span>px</span>
    </div>
   </div>

   <?php // 投稿者名・タグ・コメント ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Display Setup', 'design-plus');  ?></h3>
    <div class="theme_option_input">
     <ul>
      <li><label><input id="dp_options[show_author]" name="dp_options[show_author]" type="checkbox" value="1" <?php checked( '1', $options['show_author'] ); ?> /> <?php _e('Display author name', 'design-plus');  ?></label></li>
      <li><label><input id="dp_options[show_tag]" name="dp_options[show_tag]" type="checkbox" value="1" <?php checked( '1', $options['show_tag'] ); ?> /> <?php _e('Display tags', 'design-plus');  ?></label></li>
      <li><label><input id="dp_options[show_comment]" name="dp_options[show_comment]" type="checkbox" value="1" <?php checked( '1', $options['show_comment'] ); ?> /> <?php _e('Display comment', 'design-plus');  ?></label></li>
      <li><label><input id="dp_options[show_bookmark]" name="dp_options[show_bookmark]" type="checkbox" value="1" <?php checked( '1', $options['show_bookmark'] ); ?> /> <?php _e('Display social bookmarks', 'design-plus');  ?></label></li>
      <li><label><input id="dp_options[show_trackback]" name="dp_options[show_trackback]" type="checkbox" value="1" <?php checked( '1', $options['show_trackback'] ); ?> /> <?php _e('Display trackbacks', 'design-plus');  ?></label></li>
      <li><label><input id="dp_options[show_next_post]" name="dp_options[show_next_post]" type="checkbox" value="1" <?php checked( '1', $options['show_next_post'] ); ?> /> <?php _e('Display next previous post link', 'design-plus');  ?></label></li>
      <li><label><input id="dp_options[show_thumbnail]" name="dp_options[show_thumbnail]" type="checkbox" value="1" <?php checked( '1', $options['show_thumbnail'] ); ?> /> <?php _e('Display thumbnail in single post page', 'design-plus');  ?></label></li>
     </ul>
    </div>
   </div>

   <?php // 見出しの設定 ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Headline Setup', 'design-plus');  ?></h3>
    <div class="theme_option_input">
     <ul>
      <li>
       <label style="margin-right:20px;"><?php _e('Index page Latest Article headline', 'design-plus');  ?></label>
       <input id="dp_options[index_headline_latest_article]" class="regular-text" type="text" name="dp_options[index_headline_latest_article]" value="<?php esc_attr_e( $options['index_headline_latest_article'] ); ?>" />
      </li>
      <li>
       <label style="margin-right:20px;"><?php _e('Index page Recommend headline', 'design-plus');  ?></label>
       <input id="dp_options[index_headline_recommend]" class="regular-text" type="text" name="dp_options[index_headline_recommend]" value="<?php esc_attr_e( $options['index_headline_recommend'] ); ?>" />
      </li>
      <li>
       <label style="margin-right:20px;"><?php _e('Index page Recent Article headline', 'design-plus');  ?></label>
       <input id="dp_options[index_headline_recent_article]" class="regular-text" type="text" name="dp_options[index_headline_recent_article]" value="<?php esc_attr_e( $options['index_headline_recent_article'] ); ?>" />
      </li>
      <li>
       <label style="margin-right:20px;"><?php _e('Single page Related Article headline', 'design-plus');  ?></label>
       <input id="dp_options[single_headline_related_article]" class="regular-text" type="text" name="dp_options[single_headline_related_article]" value="<?php esc_attr_e( $options['single_headline_related_article'] ); ?>" />
      </li>
     </ul>
    </div>
   </div>

   <?php // サイドコンテンツの表示位置 ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Layout', 'design-plus');  ?></h3>
    <div class="theme_option_input layout_option">
     <fieldset class="cf"><legend class="screen-reader-text"><span><?php _e('Layout', 'design-plus');  ?></span></legend>
     <?php
          if ( ! isset( $checked ) )
          $checked = '';
          foreach ( $layout_options as $option ) {
          $layout_setting = $options['layout'];
           if ( '' != $layout_setting ) {
            if ( $options['layout'] == $option['value'] ) {
             $checked = "checked=\"checked\"";
            } else {
             $checked = '';
            }
           }
     ?>
      <label class="description">
       <input type="radio" name="dp_options[layout]" value="<?php esc_attr_e( $option['value'] ); ?>" <?php echo $checked; ?> />
       <img src="<?php bloginfo('template_url'); ?>/admin/<?php echo $option['img']; ?>.gif" alt="" title="" />
       <?php echo $option['label']; ?>
      </label>
     <?php
          }
     ?>
     </fieldset>
    </div>
   </div>

   <?php // facebook twitter ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('twitter and facebook setup', 'design-plus');  ?></h3>
    <div class="theme_option_input">
     <p><?php _e('When it is blank, twitter and facebook icon will not displayed on a site.', 'design-plus');  ?></p>
     <ul>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('your twitter URL', 'design-plus');  ?></label>
       <input id="dp_options[twitter_url]" class="regular-text" type="text" name="dp_options[twitter_url]" value="<?php esc_attr_e( $options['twitter_url'] ); ?>" />
      </li>
      <li>
       <label style="display:inline-block; min-width:140px;"><?php _e('your facebook URL', 'design-plus');  ?></label>
       <input id="dp_options[facebook_url]" class="regular-text" type="text" name="dp_options[facebook_url]" value="<?php esc_attr_e( $options['facebook_url'] ); ?>" />
      </li>
     </ul>
    </div>
   </div>

   <?php // 検索の設定 ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Using Google custom search', 'design-plus');  ?></h3>
    <div class="theme_option_input">
     <p><?php _e('If you wan\'t to use google custom search for your wordpress, enter your google custom search ID.<br /><a href="http://www.google.com/cse/" target="_blank">Read more about Google custom search page.</a>', 'design-plus');  ?></p>
     <label style="display:inline-block; margin:0 20px 0 0;"><?php _e('Google custom search ID', 'design-plus');  ?></label>
     <input id="dp_options[custom_search_id]" class="regular-text" type="text" name="dp_options[custom_search_id]" value="<?php esc_attr_e( $options['custom_search_id'] ); ?>" />
    </div>
   </div>

   <p class="submit"><input type="submit" class="button-primary" value="<?php echo __( 'Save Changes', 'design-plus' ); ?>" /></p>

  </div><!-- END #tab-content1 -->




  <!-- #tab-content2 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  -->
  <div id="tab-content2">

   <?php // ステップ１ ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Step 1 : Upload image to use for logo.', 'design-plus');  ?></h3>
    <div class="theme_option_input">
     <p><?php _e('Upload image to use for logo from your computer.<br />You can resize your logo image in step 2 and adjust position in step 3.', 'design-plus');  ?></p>
     <div class="button_area">
      <label for="dp_image"><?php _e('Select image to use for logo from your computer.', 'design-plus');  ?></label>
      <input type="file" name="dp_image" id="dp_image" value="" />
      <input type="submit" class="button" value="<?php _e('Upload', 'design-plus');  ?>" />
     </div>
     <?php if(dp_logo_exists()): $info = dp_logo_info(); ?>
     <div class="uploaded_logo">
      <h4><?php _e('Uploaded image.', 'design-plus');  ?></h4>
      <div class="uploaded_logo_image" id="original_logo_size">
       <?php dp_logo_img_tag(false, '', '', 9999); ?>
      </div>
      <p><?php printf(__('Original image size : width %1$dpx, height %2$dpx', 'design-plus'), $info['width'], $info['height']); ?></p>
     </div>
     <?php else: ?>
     <div class="uploaded_logo">
      <h4><?php _e('The image has not been uploaded yet.<br />A normal text will be used for a site logo.', 'design-plus');  ?></h4>
     </div>
     <?php endif; ?>
    </div>
   </div>

   <?php // ステップ２ ?>
   <?php if(dp_logo_exists()): ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Step 2 : Resize uploaded image.', 'design-plus');  ?></h3>
    <div class="theme_option_input">
    <?php if(dp_logo_exists()): ?>
     <p><?php _e('You can resize uploaded image.<br />If you don\'t need to resize, go to step 3.', 'design-plus');  ?></p>
     <div class="uploaded_logo">
      <h4><?php _e('Please drag the range to cut off.', 'design-plus');  ?></h4>
      <div class="uploaded_logo_image">
       <?php dp_logo_resize_base(9999); ?>
      </div>
      <div class="resize_amount">
       <label><?php _e('Ratio', 'design-plus');  ?>: <input type="text" name="dp_resize_ratio" id="dp_resize_ratio" value="100" />%</label>
       <label><?php _e('Width', 'design-plus');  ?>: <input type="text" name="dp_resized_width" id="dp_resized_width" />px</label>
       <label><?php _e('Height', 'design-plus');  ?>: <input type="text" name="dp_resized_height" id="dp_resized_height" />px</label>
      </div>
      <div id="resize_button_area">
       <input type="submit" class="button-primary" value="<?php _e('Resize', 'design-plus'); ?>" />
      </div>
     </div>
     <?php if($info = dp_logo_info(true)): ?>
     <div class="uploaded_logo">
      <h4><?php printf(__('Resized image : width %1$dpx, height %2$dpx', 'design-plus'), $info['width'], $info['height']); ?></h4>
      <div class="uploaded_logo_image">
       <?php dp_logo_img_tag(true, '', '', 9999); ?>
      </div>
     </div>
     <?php endif; ?>
    <?php endif; ?>
    </div>
   </div>
   <?php endif; ?>

   <?php // ステップ３ ?>
   <?php if(dp_logo_exists()): ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Step 3 : Adjust position of logo.', 'design-plus');  ?></h3>
    <div class="theme_option_input">
    <?php if(dp_logo_exists()): ?>
     <p><?php _e('Drag the logo image and adjust the position.', 'design-plus');  ?></p>
     <div id="design-plus-logo-adjuster" class="ratio-<?php echo '760-760'; ?>">
      <?php if(dp_logo_resize_tag(760, 760, $options['logotop'], $options['logoleft'])): ?>
      <?php else: ?>
      <span><?php _e('Logo size is too big. Please resize your logo image.', 'design-plus');  ?></span>
      <?php endif; ?>
     </div>
     <div class="hide">
      <label><?php _e('Top', 'design-plus');  ?>: <input type="text" name="dp_options[logotop]" id="dp-options-logotop" value="<?php esc_attr_e( $options['logotop'] ); ?>" />px </label>
      <label><?php _e('Left', 'design-plus');  ?>: <input type="text" name="dp_options[logoleft]" id="dp-options-logoleft" value="<?php esc_attr_e( $options['logoleft'] ); ?>" />px </label>
      <input type="button" class="button" id="dp-adjust-realvalue" value="adjust" />
     </div>
     <p><input type="submit" class="button" value="<?php _e('Save the position', 'design-plus');  ?>" /></p>
    <?php endif; ?>
    </div>
   </div>
   <?php endif; ?>

   <?php // 画像の削除 ?>
   <?php if(dp_logo_exists()): ?>
   <div class="theme_option_field cf">
    <h3 class="theme_option_headline"><?php _e('Delete logo image.', 'design-plus');  ?></h3>
    <div class="theme_option_input">
     <p><?php _e('If you delete the logo image, normal text will be used for a site logo.', 'design-plus');  ?></p>
     <p><a class="button" href="<?php echo wp_nonce_url(admin_url('themes.php?page=theme_options'), 'dp_delete_image_'.  get_current_user_id()); ?>" onclick="if(!confirm('<?php _e('Are you sure to delete logo image?', 'design-plus'); ?>')) return false;"><?php _e('Delete Image', 'design-plus');  ?></a></p>
    </div>
   </div>
   <?php endif; ?>

  </div><!-- END #tab-content2 -->




  <!-- #tab-content3 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  -->
  <div id="tab-content3">

  <?php for($i = 1; $i <= 3; $i++): ?>
  <div class="banner_wrapper">
   <h3 class="banner_headline"><?php _e('Side banner setup', 'design-plus');  ?><?php echo $i; ?></h3>
   <div class="theme_option_field cf">
    <div class="theme_option_input">
     <div class="sub_box">
      <h4><?php _e('Banner code', 'design-plus');  ?></h4>
      <p><?php _e('If you are using google adsense, enter all code below.', 'design-plus');  ?></p>
      <div class="theme_option_input">
       <textarea id="dp_options[google_ad<?php echo $i; ?>]" class="large-text" cols="50" rows="10" name="dp_options[google_ad<?php echo $i; ?>]"><?php echo esc_textarea( $options['google_ad'.$i] ); ?></textarea>
      </div>
     </div>
     <p><?php _e('If you are not using google adsense, you can register your banner image and affiliate code individually.', 'design-plus');  ?></p>
     <div class="sub_box cf" style="margin:0 0 10px 0;">
      <h4><?php _e('Register banner image', 'design-plus');  ?></h4>
      <div class="image_box cf">
       <div class="upload_banner_button_area">
        <div class="hide"><input type="text" size="36" name="dp_options[ad_image<?php echo $i; ?>]" value="<?php esc_attr_e( $options['ad_image'.$i] ); ?>" /></div>
        <input type="file" name="ad_image_file_<?php echo $i?>" id="ad_image_file_<?php echo $i?>" />
        <input type="submit" class="button-primary" value="<?php echo __( 'Save Image', 'design-plus' ); ?>" />
       </div>
       <?php if($options['ad_image'.$i]) { ?>
        <div class="uploaded_banner_image">
         <img src="<?php esc_attr_e( $options['ad_image'.$i] ); ?>" alt="" title="" />
        </div>
        <?php if(dp_is_uploaded_img($options['ad_image'.$i])): ?>
        <div class="delete_uploaded_banner_image">
         <a href="<?php echo wp_nonce_url(admin_url('themes.php?page=theme_options'), 'dp_delete_ad_'.$i) ?>" class="button" onclick="if(!confirm('<?php _e('Are you sure to delete this image?', 'design-plus'); ?>')) return false;"><?php _e('Delete Image', 'design-plus'); ?></a>
        </div>
       <?php endif; ?>
       <?php }; ?>
      </div>
     </div>
     <div class="sub_box">
      <h4><?php _e('Register affiliate code', 'design-plus');  ?></h4>
      <div class="theme_option_input">
       <input id="dp_options[ad_url<?php echo $i; ?>]" class="regular-text" type="text" name="dp_options[ad_url<?php echo $i; ?>]" value="<?php esc_attr_e( $options['ad_url'.$i] ); ?>" />
      </div>
     </div>
    </div>
   </div>
  </div>
  <?php endfor; ?>

  <p class="submit"><input type="submit" class="button-primary" value="<?php echo __( 'Save Changes', 'design-plus' ); ?>" /></p>

  </div><!-- END #tab-content3 -->

  </div><!-- END #tab-panel -->

 </form>

</div>

</div>

<?php

 }


/**
 * チェック
 */
function theme_options_validate( $input ) {
 global $layout_options;

 // メインカラー
 $input['pickedcolor'] = wp_filter_nohtml_kses( $input['pickedcolor'] );

 // フォントサイズ
 $input['content_font_size'] = wp_filter_nohtml_kses( $input['content_font_size'] );

 // 投稿者・タグ・コメント
 if ( ! isset( $input['show_author'] ) )
  $input['show_author'] = null;
  $input['show_author'] = ( $input['show_author'] == 1 ? 1 : 0 );
 if ( ! isset( $input['show_tag'] ) )
  $input['show_tag'] = null;
  $input['show_tag'] = ( $input['show_tag'] == 1 ? 1 : 0 );
 if ( ! isset( $input['show_comment'] ) )
  $input['show_comment'] = null;
  $input['show_comment'] = ( $input['show_comment'] == 1 ? 1 : 0 );
 if ( ! isset( $input['show_bookmark'] ) )
  $input['show_bookmark'] = null;
  $input['show_bookmark'] = ( $input['show_bookmark'] == 1 ? 1 : 0 );
 if ( ! isset( $input['show_trackback'] ) )
  $input['show_trackback'] = null;
  $input['show_trackback'] = ( $input['show_trackback'] == 1 ? 1 : 0 );
 if ( ! isset( $input['show_next_post'] ) )
  $input['show_next_post'] = null;
  $input['show_next_post'] = ( $input['show_next_post'] == 1 ? 1 : 0 );
 if ( ! isset( $input['show_thumbnail'] ) )
  $input['show_thumbnail'] = null;
  $input['show_thumbnail'] = ( $input['show_thumbnail'] == 1 ? 1 : 0 );

 // 見出しの設定
 $input['index_headline_latest_article'] = wp_filter_nohtml_kses( $input['index_headline_latest_article'] );
 $input['index_headline_recommend'] = wp_filter_nohtml_kses( $input['index_headline_recommend'] );
 $input['index_headline_recent_article'] = wp_filter_nohtml_kses( $input['index_headline_recent_article'] );
 $input['sindle_headline_related_article'] = wp_filter_nohtml_kses( $input['single_headline_related_article'] );

 // レイアウトの設定
 if ( ! isset( $input['layout'] ) )
  $input['layout'] = null;
 if ( ! array_key_exists( $input['layout'], $layout_options ) )
  $input['layout'] = null;

 // twitter,facebook URL
 $input['twitter_url'] = wp_filter_nohtml_kses( $input['twitter_url'] );
 $input['facebook_url'] = wp_filter_nohtml_kses( $input['facebook_url'] );

 // 検索の設定
 $input['custom_search_id'] = wp_filter_nohtml_kses( $input['custom_search_id'] );

 //ロゴの位置
 if(isset($input['logotop'])){
	 $input['logotop'] = intval($input['logotop']);
 }
 if(isset($input['logoleft'])){
	 $input['logoleft'] = intval($input['logoleft']);
 }
 
 // 広告バナー1
 $input['ad_image1'] = wp_filter_nohtml_kses( $input['ad_image1'] );
 $input['ad_url1'] = wp_filter_nohtml_kses( $input['ad_url1'] );
 $input['google_ad1'] = $input['google_ad1'];

 // 広告バナー2
 $input['ad_image2'] = wp_filter_nohtml_kses( $input['ad_image2'] );
 $input['ad_url2'] = wp_filter_nohtml_kses( $input['ad_url2'] );
 $input['google_ad2'] = $input['google_ad2'];

 // 広告バナー3
 $input['ad_image3'] = wp_filter_nohtml_kses( $input['ad_image3'] );
 $input['ad_url3'] = wp_filter_nohtml_kses( $input['ad_url3'] );
 $input['google_ad3'] = $input['google_ad3'];
 
 //ファイルアップロード
 if(isset($_FILES['dp_image'])){
	$message = _dp_upload_logo();
	add_settings_error('design_plus_options', 'default', $message['message'], ($message['error'] ? 'error' : 'updated'));
 }

 //画像リサイズ
 if(isset($_REQUEST['dp_logo_resize_left'], $_REQUEST['dp_logo_resize_top']) && is_numeric($_REQUEST['dp_logo_resize_left']) && is_numeric($_REQUEST['dp_logo_resize_top'])){
	$message = _dp_resize_logo();
	add_settings_error('design_plus_options', 'default', $message['message'], ($message['error'] ? 'error' : 'updated'));
 }
 //広告画像の登録
 for($i = 1; $i <= 3; $i++){
	 if(isset($_FILES['ad_image_file_'.$i])){
		 //画像のアップロードに問題はないか
		 if($_FILES['ad_image_file_'.$i]['error'] === 0){
			 $name = sanitize_file_name($_FILES['ad_image_file_'.$i]['name']);
			 //ファイル形式をチェック
			 if(!preg_match("/\.(png|jpe?g|gif)$/i", $name)){
				 add_settings_error('design_plus_options', 'dp_uploader', sprintf(__('You uploaded %s but allowed file format is PNG, GIF and JPG.', 'design-plus'), $name), 'error');
			 }else{
				//ディレクトリの存在をチェック
				if(
					(
						(file_exists(dp_logo_basedir()) && is_dir(dp_logo_basedir()) && is_writable(dp_logo_basedir()) )
							||
						@mkdir(dp_logo_basedir())
					)
						&&
					move_uploaded_file($_FILES['ad_image_file_'.$i]['tmp_name'], dp_logo_basedir().DIRECTORY_SEPARATOR.$name)
				){
					$input['ad_image'.$i] = dp_logo_baseurl().'/'.$name;
				}else{
					add_settings_error('default', 'dp_uploader', sprintf(__('Directory %s is not writable. Please check permission.', 'design-plus'), dp_logo_basedir()), 'error');
					break;
				}
			 }
		 }elseif($_FILES['ad_image_file_'.$i]['error'] !== UPLOAD_ERR_NO_FILE){
			 add_settings_error('default', 'dp_uploader', _dp_get_upload_err_msg($_FILES['ad_image_file_'.$i]['error']), 'error');
			 continue;
		 }
	 }
 }	 
 return $input;
}

?>