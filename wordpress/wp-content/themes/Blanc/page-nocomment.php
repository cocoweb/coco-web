<?php
/*
Template Name:No comment
*/
?>
<?php get_header(); $options = get_desing_plus_option(); ?>

 <div id="headline">
  <h2><?php the_title(); ?></h2>
 </div><!-- END #archive_headline -->

 <div id="content" class="cf">

  <div id="left_col">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

   <div class="single_post">

    <div class="post cf">
     <?php the_content(__('Read more', 'design-plus')); ?>
     <?php wp_link_pages(); ?>
    </div>

   </div><!-- END .single_post -->

   <?php endwhile; endif; ?>

   </div><!-- #left_col -->

   <?php include('sidebar.php'); ?>

  </div><!-- END #content -->

<?php get_footer(); ?>