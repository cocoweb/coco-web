<?php
function enDate_init(){
	global $wp_locale;

if (strtoupper(get_locale()) == 'JA') : // we only use this function in japanese wordpress

		$wp_locale->weekday[0] = ('Sunday');
		$wp_locale->weekday[1] = ('Monday');
		$wp_locale->weekday[2] = ('Tuesday');
		$wp_locale->weekday[3] = ('Wednesday');
		$wp_locale->weekday[4] = ('Thursday');
		$wp_locale->weekday[5] = ('Friday');
		$wp_locale->weekday[6] = ('Saturday');

		$wp_locale->weekday_initial[('Sunday')]    = ('Sun');
		$wp_locale->weekday_initial[('Monday')]    = ('Mon');
		$wp_locale->weekday_initial[('Tuesday')]   = ('Tue');
		$wp_locale->weekday_initial[('Wednesday')] = ('Wed');
		$wp_locale->weekday_initial[('Thursday')]  = ('Thu');
		$wp_locale->weekday_initial[('Friday')]    = ('Fri');
		$wp_locale->weekday_initial[('Saturday')]  = ('Sat');


		$wp_locale->weekday_abbrev[('Sunday')]    = ('Sun');
		$wp_locale->weekday_abbrev[('Monday')]    = ('Mon');
		$wp_locale->weekday_abbrev[('Tuesday')]   = ('Tue');
		$wp_locale->weekday_abbrev[('Wednesday')] = ('Wed');
		$wp_locale->weekday_abbrev[('Thursday')]  = ('Thu');
		$wp_locale->weekday_abbrev[('Friday')]    = ('Fri');
		$wp_locale->weekday_abbrev[('Saturday')]  = ('Sat');

endif;

}

add_action('init', 'enDate_init');

?>