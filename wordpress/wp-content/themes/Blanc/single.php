<?php get_header(); $options = get_desing_plus_option(); ?>

 <div id="headline">
  <h2><?php _e('Blog', 'design-plus'); ?></h2>
 </div><!-- END #archive_headline -->

 <div id="content" class="cf">

  <div id="left_col">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

   <div class="single_post">

    <div class="post_info cf">
     <div class="title_area">
      <h3 class="title"><?php the_title(); ?></h3>
      <ul class="meta cf">
       <li class="post_category"><?php the_category(','); ?></li>
       <?php if ($options['show_tag']) : ?><?php the_tags('<li class="post_tag">',',','</li>'); ?><?php endif; ?>
       <?php if ($options['show_comment']) : ?><li class="post_comment"><?php comments_popup_link(__('Write comment', 'design-plus'), __('1 comment', 'design-plus'), __('% comments', 'design-plus')); ?></li><?php endif; ?>
       <?php edit_post_link(__('EDIT', 'design-plus'), '<li class="post_edit">[ ',' ]</li>' ); ?>
      </ul>
     </div>
     <p class="date"><?php the_time(__('m/d', 'design-plus')) ?></p>
    </div>

    <div class="post cf">
     <?php if ($options['show_thumbnail']) : ?>
     <?php if ( has_post_thumbnail()) { echo '<div class="single_post_thumb">'; echo the_post_thumbnail('large_size'); echo '</div>'; }; ?>
     <?php endif; ?>
     <?php the_content(__('Read more', 'design-plus')); ?>
     <?php wp_link_pages(); ?>
    </div>

    <?php if ($options['show_bookmark'] || $options['show_trackback']) : ?>
    <div id="bookmark_area">
     <?php if ($options['show_bookmark']): include('bookmark.php'); endif; ?>
     <?php if (pings_open()) { if ($options['show_trackback']): ?>
     <div id="trackback_url_area">
      <label for="trackback_url"><?php _e('TRACKBACK URL' , 'design-plus'); ?></label>
      <input type="text" name="trackback_url" id="trackback_url" size="60" value="<?php trackback_url() ?>" readonly="readonly" onfocus="this.select()" />
     </div>
     <?php endif; }; ?>
    </div>
    <?php endif; ?>

   </div><!-- END .single_post -->

   <?php endwhile; endif; ?>

   <?php // related post
         $categories = get_the_category($post->ID);
         if ($categories) {
         $category_ids = array();
          foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
           $args=array(
                        'category__in' => $category_ids,
                        'post__not_in' => array($post->ID),
                        'showposts'=>4,
                        'orderby' => 'rand'
                      );
          $my_query = new wp_query($args);
          $i = 1;
          if($my_query->have_posts()) {
   ?>
   <div id="related_post">
    <h3 class="headline"><?php esc_attr_e( $options['single_headline_related_article'] ); ?></h3>
    <ul class="cf">
     <?php while ($my_query->have_posts()) { $my_query->the_post(); ?>
     <li class="cf <?php echo 'post' . $i; ?>">
      <a class="image" href="<?php the_permalink() ?>"><?php if ( has_post_thumbnail()) { the_post_thumbnail('small_size'); } else { echo '<img src="'; bloginfo('template_url'); echo '/img/common/no_image3.gif" alt="" title="" />'; }; ?></a>
      <div class="meta">
       <p class="date"><?php the_time('Y.m.d'); ?></p>
       <h4 class="title"><a href="<?php the_permalink() ?>"><?php trim_str_by_chars( get_the_title(), 50); ?></a></h4>
      </div>
     </li>
     <?php $i++; }; ?>
    </ul>
   </div>
   <?php }; }; wp_reset_query(); ?>

   <?php if ($options['show_comment']) : ?>
   <?php if (function_exists('wp_list_comments')) { comments_template('', true); } else { comments_template(); } ?>
   <?php endif; ?>

   <?php if ($options['show_next_post']) : ?>
   <div id="previous_next_post" class="cf">
    <p id="previous_post"><?php previous_post_link('%link') ?></p>
    <p id="next_post"><?php next_post_link('%link') ?></p>
   </div>
   <?php endif; ?>

  </div><!-- #left_col -->

  <?php include('sidebar.php'); ?>

 </div><!-- END #content -->

<?php get_footer(); ?>