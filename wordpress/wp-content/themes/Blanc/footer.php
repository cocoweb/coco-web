
  <div id="breadcrumb_area">
   <?php include('breadcrumb.php'); ?>
   <a id="return_top" href="#header"><img src="<?php bloginfo('template_url'); ?>/img/footer/return_top.gif" alt="RETURN TOP" title="RETURN TOP" class="rollover" /></a>
  </div>

 </div><!-- END #main_content -->

 <div id="footer_wrap">
  <div id="footer">

   <?php if(is_active_sidebar('footer_widget')){ ?>
   <div id="footer_widget_area" class="cf">
    <?php dynamic_sidebar('footer_widget'); ?>
   </div>
   <?php }; ?>

   <?php if(has_nav_menu('footer-menu')) {  if (function_exists('wp_nav_menu')) { wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'footer-menu' , 'container' => 'div' , 'depth' => '1', 'container_id' => 'footer_menu', 'menu_class' => 'cf' ) ); }; }; ?>

   <p id="copyright"><?php _e('Copyright &copy;&nbsp; ', 'design-plus'); ?><a href="<?php bloginfo('wpurl'); ?>/"><?php bloginfo('name'); ?></a></p>

  </div><!-- END #footer -->
 </div>

<?php wp_footer(); ?>

<?php $options = get_desing_plus_option(); if ($options['show_bookmark']): ?>
<div id="fb-root"></div>
<script type="text/javascript">(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php endif; ?>

</body>
</html>