<?php $options = get_desing_plus_option(); if (!is_paged()):  get_header(); ?>

 <div id="index_top" class="cf">

  <?php // latest post -------------------------------------------------------------- ?>
  <div id="index_latest">
   <h2 class="headline"><?php esc_attr_e( $options['index_headline_latest_article'] ); ?></h2>
   <?php
        $args = array('post_type' => 'post', 'numberposts' => 3);
        $latest_post=get_posts($args);
        if ($latest_post) {
   ?>
   <ul class="cf">
    <?php
         foreach ($latest_post as $post) : setup_postdata ($post);
    ?>
    <li>
     <a class="image" href="<?php the_permalink() ?>"><?php if ( has_post_thumbnail()) { the_post_thumbnail('mid_size1'); } else { echo '<img src="' . get_bloginfo('template_url') . '/img/common/no_image1.gif" alt="" title="" />'; }; ?></a>
     <h3 class="title"><a href="<?php the_permalink() ?>"><?php trim_str_by_chars( get_the_title(), 50); ?></a></h3>
     <p class="date"><?php the_time('Y.m.d'); ?></p>
     <p class="excerpt"><?php new_excerpt(40); ?></p>
     <a class="arrow_link" href="<?php the_permalink() ?>"><?php _e('Read more', 'design-plus'); ?></a>
    </li>
    <?php endforeach; ?>
   </ul>
   <?php } else { ?>
   <p><?php _e("There is no registered post.","design-plus"); ?></p>
   <?php }; wp_reset_query(); ?>
  </div><!-- END #index_latest -->

  <?php // recommend post -------------------------------------------------------------- ?>
  <div id="index_recommend">
   <h2 class="headline"><?php esc_attr_e( $options['index_headline_recommend'] ); ?></h2>
   <?php
        $args = array('post_type' => 'post', 'numberposts' => 3, 'meta_key' => 'recommend_post', 'meta_value' => 'on', 'orderby' => 'rand');
        $recommend_post=get_posts($args);
        if ($recommend_post) {
   ?>
   <ul>
    <?php
         foreach ($recommend_post as $post) : setup_postdata ($post);
    ?>
    <li class="cf">
     <div class="meta">
      <p class="date"><?php the_time('Y.m.d'); ?></p>
      <h4 class="title"><a href="<?php the_permalink() ?>"><?php trim_str_by_chars( get_the_title(), 50); ?></a></h4>
     </div>
     <a class="image" href="<?php the_permalink() ?>"><?php if ( has_post_thumbnail()) { the_post_thumbnail('small_size'); } else { echo '<img src="'; bloginfo('template_url'); echo '/img/common/no_image3.gif" alt="" title="" />'; }; ?></a>
    </li>
    <?php endforeach; ?>
   </ul>
   <?php } else { ?>
   <p><?php _e("There is no registered post.","design-plus"); ?></p>
   <?php }; wp_reset_query(); ?>
  </div><!-- END #index_featured -->

 </div><!-- END #index_top -->

 <?php // recent post -------------------------------------------------------------- ?>
 <div id="index_bottom">
  <h2 class="headline"><?php esc_attr_e( $options['index_headline_recent_article'] ); ?></h2>
   <?php
        $args = array('post_type' => 'post', 'numberposts' => 6, 'offset' => 3);
        $recent_post=get_posts($args);
        if ($recent_post) {
   ?>
   <ul class="cf">
    <?php
         foreach ($recent_post as $post) : setup_postdata ($post);
    ?>
    <li class="cf">
     <div class="meta">
      <p class="date"><?php the_time('Y.m.d'); ?></p>
      <h3 class="title"><a href="<?php the_permalink() ?>"><?php trim_str_by_chars( get_the_title(), 50); ?></a></h3>
      <a class="arrow_link" href="<?php the_permalink() ?>"><?php _e('Read more', 'design-plus'); ?></a>
     </div>
     <a class="image" href="<?php the_permalink() ?>"><?php if ( has_post_thumbnail()) { the_post_thumbnail('mid_size2'); } else { echo '<img src="'; bloginfo('template_url'); echo '/img/common/no_image2.gif" alt="" title="" />'; }; ?></a>
    </li>
    <?php endforeach; ?>
   </ul>
   <?php } else { ?>
   <p><?php _e("There is no registered post.","design-plus"); ?></p>
   <?php }; wp_reset_query(); ?>
   <div id="archive_button"><?php next_posts_link(__('Older Entries', 'design-plus')) ?></div>
 </div>

<?php get_footer(); else: include('archive.php'); endif; ?>