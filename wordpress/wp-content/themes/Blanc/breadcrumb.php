<?php if(!is_front_page()||is_paged()) { ?>

<ul id="bread_crumb" class="cf">
 <li><a href="<?php bloginfo('wpurl'); ?>/"><?php _e('Home', 'design-plus'); ?></a></li>

<?php if(is_paged()) { ?>
 <li class="last"><?php _e('Blog Archives', 'design-plus'); ?></li>

<?php } elseif (is_category()) { ?>
 <li class="last"><?php echo single_cat_title('', false); ?></li>

<?php } elseif(is_tag()) { ?>
 <li class="last"><?php echo single_tag_title('', false); ?></li>

<?php } elseif(is_day()) { ?>
 <li class="last"><?php echo get_the_time(__('F jS, Y', 'design-plus')); ?></li>

<?php } elseif(is_month()) { ?>
 <li class="last"><?php echo get_the_time(__('F, Y', 'design-plus')); ?></li>

<?php } elseif(is_year()) { ?>
 <li class="last"><?php echo get_the_time(__('Y', 'design-plus')); ?></li>

<?php } elseif(is_author()) { global $wp_query; $curauth = $wp_query->get_queried_object(); //get the author info ?>
 <li class="last"><?php echo $curauth->display_name; ?></li>

<?php } elseif(is_single()) { ?>
 <li><?php the_category(','); ?></li>
 <li class="last"><?php the_title(); ?></li>

<?php } elseif(is_page()) { ?>
 <li class="last"><?php the_title(); ?></li>

<?php }; ?>
</ul>

<?php }; ?>