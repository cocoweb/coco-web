<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php
global $page, $paged; wp_title( '|', true, 'right' ); bloginfo( 'name' );
$site_description = get_bloginfo( 'description', 'display' ); if ( $site_description && ( is_home() || is_front_page() ) ) echo " | $site_description";
if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s', 'design-plus' ), max( $paged, $page ) );
?></title>
<meta name="description" content="<?php if (!is_paged() && is_front_page()): echo bloginfo('description'); else: echo the_title(); endif; ?>" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" /> 
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php $options = get_desing_plus_option(); ?>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
<?php if ($options['show_comment']): ?>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/comment-style.css" type="text/css" />
<?php endif; ?>
<?php if (strtoupper(get_locale()) == 'JA') ://to fix the font-size for japanese font ?>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/japanese.css" type="text/css" />
<?php endif; ?>

<style type="text/css">
body { font-size:<?php echo $options['content_font_size']; ?>px; }

#top_line, .flexslider .slides, #archive_button a, .page_navi a:hover, .archive_post .date,
 .page_navi p.back a:hover, .single_post .date, #wp-calendar td a:hover, #wp-calendar #prev a:hover,
  #wp-calendar #next a:hover, #footer #wp-calendar td a:hover, #submit_comment:hover, .wpcf7 input.wpcf7-submit:hover
   { background:#<?php echo $options['pickedcolor']; ?>; }

a:hover, a.arrow_link:hover, #logo a:hover, #index_latest ul li .title a:hover, #index_recommend ul li .title a:hover,
 #index_bottom ul li .title a:hover, #footer a:hover, #bread_crumb li a:hover, .archive_post .title a:hover,
  .archive_post .meta li a:hover, .single_post .meta li a:hover, #right_col a:hover, #related_post ul li .title a:hover
   { color:#<?php echo $options['pickedcolor']; ?>; }

#comment_textarea textarea:focus, #guest_info input:focus, .wpcf7 input:focus, .wpcf7 textarea:focus
   { border:1px solid #<?php echo $options['pickedcolor']; ?>; }
</style>

<?php wp_enqueue_script( 'jquery' ); ?>
<?php if ($options['show_comment']): if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); endif; ?> 
<?php wp_head(); ?>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.insetBorderEffect.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/rollover.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jscript.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scroll.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/comment.js"></script>

<!--[if IE 7]>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/ie7.css" type="text/css" />
<![endif]-->

<?php if(!is_paged() && is_front_page()) { ?>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider.js"></script>
<script type="text/javascript" charset="utf-8">
jQuery(document).ready(function($) {
 $('.flexslider').flexslider();
});
</script>
<?php }; ?>

</head>
<body<?php if(!is_paged() && is_front_page()) { echo ' id="index"'; } elseif(is_page_template('page-noside.php')||is_page_template('page-noside-nocomment.php')) { echo ' id="no_side"'; } elseif(is_page()) { echo ' id="page"'; }; if($options['layout'] == 'left') { echo ' class="layout2"'; }; ?>>

 <div id="container">

  <div id="header">

   <!-- logo -->
   <?php the_dp_logo(); ?>

   <!-- global menu -->
   <div id="global_menu" class="cf">
    <?php
         if (function_exists('wp_nav_menu')) {
          if(has_nav_menu('header-menu')) {
           wp_nav_menu( array( 'sort_column' => 'menu_order', 'theme_location' => 'header-menu' , 'container' => '' ) );
          } else { 
    ?>
    <ul>
     <li><a href="<?php echo bloginfo('url'); ?>/"><?php _e('Home', 'design-plus'); ?></a></li>
     <?php wp_list_pages('title_li='); ?>
    </ul>
   <?php }; ?>
   <?php } else { ?>
    <ul>
     <li><a href="<?php echo bloginfo('url'); ?>/"><?php _e('Home', 'design-plus'); ?></a></li>
     <?php wp_list_pages('title_li='); ?>
    </ul>
   <?php }; ?>
   </div>

   <!-- social button -->
   <ul class="social_link cf"<?php if (!$options['twitter_url']&&!$options['facebook_url']) : echo ' id="social_link2"'; endif; ?>>
    <?php if ($options['twitter_url']) : ?>
    <li class="twitter_button"><a class="target_blank" href="<?php echo $options['twitter_url']; ?>"><img src="<?php bloginfo('template_url'); ?>/img/header/twitter.gif" alt="twitter" title="twitter" class="rollover" /></a></li>
    <?php endif; ?>
    <?php if ($options['facebook_url']) : ?>
    <li class="facebook_button"><a class="target_blank" href="<?php echo $options['facebook_url']; ?>"><img src="<?php bloginfo('template_url'); ?>/img/header/facebook.gif" alt="facebook" title="facebook" class="rollover" /></a></li>
    <?php endif; ?>
    <li class="rss_button"><a class="target_blank" href="<?php bloginfo('rss2_url'); ?>"><img src="<?php bloginfo('template_url'); ?>/img/header/rss.gif" alt="rss" title="rss" class="rollover" /></a></li>
   </ul>

   <!-- search area -->
   <div class="search_area"<?php if (!$options['twitter_url']&&!$options['facebook_url']) : echo ' id="search_area2"'; endif; ?>>
    <?php if ($options['custom_search_id']) { ?>
    <form action="http://www.google.com/cse" method="get" id="searchform">
     <div>
      <input id="search_button" class="rollover" type="image" src="<?php bloginfo('template_url'); ?>/img/header/search_button.gif" name="sa" alt="<?php _e('SEARCH','design-plus'); ?>" title="<?php _e('SEARCH','design-plus'); ?>" />
      <input type="hidden" name="cx" value="<?php echo $options['custom_search_id']; ?>" />
      <input type="hidden" name="ie" value="UTF-8" />
     </div>
     <div><input id="search_input" type="text" value="<?php _e('SEARCH','design-plus'); ?>" name="q" onfocus="if (this.value == '<?php _e('SEARCH','design-plus'); ?>') this.value = '';" onblur="if (this.value == '') this.value = '<?php _e('SEARCH','design-plus'); ?>';" /></div>
    </form>
    <?php } else { ?>
    <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
     <div><input id="search_button" class="rollover" type="image" src="<?php bloginfo('template_url'); ?>/img/header/search_button.gif" alt="<?php _e('SEARCH','design-plus'); ?>" title="<?php _e('SEARCH','design-plus'); ?>" /></div>
     <div><input id="search_input" type="text" value="<?php _e('SEARCH','design-plus'); ?>" name="s" onfocus="if (this.value == '<?php _e('SEARCH','design-plus'); ?>') this.value = '';" onblur="if (this.value == '') this.value = '<?php _e('SEARCH','design-plus'); ?>';" /></div>
    </form>
    <?php }; ?>
   </div>

  </div><!-- END #header -->

  <p id="top_line">&nbsp;</p>

 </div><!-- #container -->

 <?php if(!is_paged() && is_front_page()) { ?>
 <!-- slider -->
 <div class="flexslider">
  <?php
       $args = array('post_type' => 'post', 'numberposts' => 5, 'meta_key' => 'recommend_post', 'meta_value' => 'on', 'orderby' => 'rand');
       $header_recommend_post=get_posts($args);
       if ($header_recommend_post) {
  ?>
  <ul class="slides cf">
   <?php foreach ($header_recommend_post as $post) : setup_postdata ($post); ?>
   <li>
    <div class="left">
     <p class="date"><?php the_time('m/d'); ?></p>
     <p class="year"><?php the_time('Y-D'); ?></p>
     <h3 class="title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
     <a class="link" href="<?php the_permalink() ?>"><?php _e('Read more', 'design-plus'); ?></a>
    </div>
    <div class="image">
     <a href="<?php the_permalink() ?>"><?php if ( has_post_thumbnail()) { echo the_post_thumbnail('large_size'); } else { echo '<img src="'; bloginfo('template_url'); echo '/img/index/no_image.gif" alt="" title="" />'; }; ?></a>
    </div>
   </li>
   <?php endforeach; wp_reset_query(); ?>
  </ul>
  <?php } else { ?>
  <p class="no_recommend"><?php _e('Please select at least 5 recommend post from post edit page.', 'design-plus');  ?></p>
  <?php }; ?>
 </div>
 <?php }; ?>

 <div id="main_content" class="cf">
