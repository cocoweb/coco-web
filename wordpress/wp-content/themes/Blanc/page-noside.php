<?php
/*
Template Name:No sidebar
*/
?>
<?php get_header(); $options = get_desing_plus_option(); ?>

 <div id="headline">
  <h2><?php the_title(); ?></h2>
 </div><!-- END #archive_headline -->

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

   <div class="single_post">

    <div class="post cf" style="margin:0 0 50px 0;">
     <?php the_content(__('Read more', 'design-plus')); ?>
     <?php wp_link_pages(); ?>
    </div>

   </div><!-- END .single_post -->

   <?php if ($options['show_comment']) : ?>
   <?php if (function_exists('wp_list_comments')) { comments_template('', true); } else { comments_template(); } ?>
   <?php endif; ?>

   <?php endwhile; endif; ?>

<?php get_footer(); ?>