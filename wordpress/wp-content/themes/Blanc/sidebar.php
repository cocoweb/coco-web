<div id="right_col">

 <?php if(is_active_sidebar('side_widget')){ dynamic_sidebar('side_widget'); } else { ?>

  <div class="side_widget">
   <h3 class="headline">Recent Post</h3>
   <ul>
    <?php $myposts = get_posts('numberposts=5'); foreach($myposts as $post) : ?>
    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
    <?php endforeach; ?>
   </ul>
  </div>

  <div class="side_widget">
   <h3 class="headline">Calendar</h3>
   <?php get_calendar(true); ?>
  </div>

  <div class="side_widget">
   <h3 class="headline">Archives</h3>
   <ul>
    <?php wp_get_archives('type=monthly'); ?>
   </ul>
  </div>

  <div class="side_widget">
   <h3 class="headline">Category</h3>
   <ul>
    <?php wp_list_categories('orderby=name'); ?>
   </ul>
  </div>

 <?php }; ?>

</div>