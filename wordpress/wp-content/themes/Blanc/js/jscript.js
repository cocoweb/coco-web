jQuery(document).ready(function($){

  $("a").bind("focus",function(){if(this.blur)this.blur();});
  $('.rollover').rollover();
  $("a.target_blank").attr("target","_blank");



  $("#footer_menu > ul > li:last-child").addClass("last");



  var n_size=$("#global_menu > ul > li").size();
  var aWidth = 715/n_size;
  $("#global_menu > ul > li").css("width", aWidth+"px");
  $("#global_menu ul ul li").css("width", (aWidth+25)+"px");

  $("#global_menu ul li").hover(function(){
   $(">ul:not(:animated)",this).slideDown("fast");
   $(this).addClass("active_menu");
  },
  function(){
   $(">ul",this).slideUp("fast");
   $(this).removeClass("active_menu");
  });



  $("#comment_area ol > li:even").addClass("even_comment");
  $("#comment_area ol > li:odd").addClass("odd_comment");
  $(".even_comment > .children > li").addClass("even_comment_children");
  $(".odd_comment > .children > li").addClass("odd_comment_children");
  $(".even_comment_children > .children > li").addClass("odd_comment_children");
  $(".odd_comment_children > .children > li").addClass("even_comment_children");
  $(".even_comment_children > .children > li").addClass("odd_comment_children");
  $(".odd_comment_children > .children > li").addClass("even_comment_children");

  $("#trackback_switch").click(function(){
    $("#comment_switch").removeClass("comment_switch_active");
    $(this).addClass("comment_switch_active");
    $("#comment_area").animate({opacity: 'hide'}, 0);
    $("#trackback_area").animate({opacity: 'show'}, 1000);
    return false;
  });

  $("#comment_switch").click(function(){
    $("#trackback_switch").removeClass("comment_switch_active");
    $(this).addClass("comment_switch_active");
    $("#trackback_area").animate({opacity: 'hide'}, 0);
    $("#comment_area").animate({opacity: 'show'}, 1000);
    return false;
  });



});

jQuery(window).load(function() {
 jQuery('#index_latest .image img, #index_recommend .image img, #index_bottom .image img, .archive_post .image img, .recommend_widget .image img, .slides .image img').insetBorder({
   inset : 10,
   borderColor : '#fff',
   speed : 200,
   borderType : 'solid'
 });
});