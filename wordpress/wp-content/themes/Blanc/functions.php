<?php

// 言語ファイルの読み込み
load_textdomain('design-plus', dirname(__FILE__).'/languages/' . get_locale() . '.mo');


// テーマオプション
require_once ( get_stylesheet_directory() . '/admin/theme-options.php' );

// 更新通知
require_once ( get_stylesheet_directory() . '/admin/update_notifier.php' );

// ウィジェットの読み込み
require_once ( get_stylesheet_directory() . '/widget/recommend_post.php' );
require_once ( get_stylesheet_directory() . '/widget/ad.php' );
require_once ( get_stylesheet_directory() . '/widget/EnglishDate.php' );

// おすすめ記事
require_once ( get_stylesheet_directory() . '/admin/theme-recommend.php' );

//ロゴ画像用関数
get_template_part('functions/header-logo');

// スタイルシートの読み込み
add_action('admin_print_styles', 'my_admin_CSS');
function my_admin_CSS() {
 wp_enqueue_style('myAdminCSS', get_bloginfo('stylesheet_directory').'/admin/my_admin.css');
};


// 記事タイトルのトリミング
function trim_str_by_chars( $str, $len, $echo = true, $suffix = '…', $encoding = 'UTF-8' ) {
 if ( ! function_exists( 'mb_substr' ) || ! function_exists( 'mb_strlen' ) ) {
  return $str;
 }
 $len = (int)$len;
 if ( mb_strlen( $str = wp_specialchars_decode( strip_tags( $str ), ENT_QUOTES, $encoding ), $encoding ) > $len ) {
  $str = esc_html( mb_substr( $str, 0, $len, $encoding ) . $suffix );
 }
 if ( $echo ) {
  echo $str;
 } else {
  return $str;
 }
};


// オリジナルの抜粋記事
function new_excerpt($a) {

 $base_content = get_the_content();
 $base_content = preg_replace('!<style.*?>.*?</style.*?>!is', '', $base_content);
 $base_content = preg_replace('!<script.*?>.*?</script.*?>!is', '', $base_content);
 $base_content = strip_tags($base_content);
 $trim_content = mb_substr($base_content, 0, $a ,"utf-8");
 $trim_content = mb_ereg_replace('&nbsp;', '', $trim_content);

 if(preg_match("/。/", $trim_content)) { //指定した文字数内にある、最後の「。」以降をカットして表示
    mb_regex_encoding("UTF-8"); 
    $trim_content = mb_ereg_replace('。[^。]*$', '。', $trim_content);
    echo $trim_content;
 }else{ //指定した文字数内に「。」が無い場合は、指定した文字数の文章を表示し、末尾に「…」を表示
    echo $trim_content . '…';
 };

};


// ページナビ用
function show_posts_nav() {
global $wp_query;
return ($wp_query->max_num_pages > 1);
};


// アイキャッチに文言を追加
add_filter( 'admin_post_thumbnail_html', 'add_featured_image_instruction');
function add_featured_image_instruction( $content ) {
 return $content .= '<p>' . __('Upload post thumbnail from here.', 'design-plus') . '</p>';
};


// カスタムメニューの設定
if(function_exists('register_nav_menu')) {
 register_nav_menu( 'header-menu', __( 'Header menu', 'design-plus' ) );
 register_nav_menu( 'footer-menu', __( 'Footer menu', 'design-plus' ) );
}


// ウィジェットの設定
if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
        'before_widget' => '<div class="side_widget %2$s" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="headline">',
        'after_title' => "</h3>\n",
        'name' => __('Side widget', 'design-plus'),
        'id' => 'side_widget'
    ));
    register_sidebar(array(
        'before_widget' => '<div class="footer_widget" id="%1$s">'."\n",
        'after_widget' => "</div>\n",
        'before_title' => '<h3 class="headline">',
        'after_title' => "</h3>\n",
        'name' => __('Footer widget', 'design-plus'),
        'description' => __('Registration is possible to three.', 'design-plus'),
        'id' => 'footer_widget'
    ));
}


//　ヘッダーから余分なMETA情報を削除
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wp_generator');


//　サムネイルの設定
if (function_exists('add_theme_support')) {
add_theme_support('post-thumbnails');
add_image_size( 'large_size', 507, 277, true );
add_image_size( 'mid_size1', 225, 120, true );
add_image_size( 'mid_size2', 139, 127, true );
add_image_size( 'small_size', 64, 64, true );
}
// メディアアップローダーから記事へのリンクボタンを消す
add_filter('attachment_fields_to_edit',  'new_attachment_fields_to_edit', 1,  2);
function new_attachment_fields_to_edit($fields, $post){
        if (substr($post->post_mime_type,0,5) == 'image'){
                $html = "<input type='text' class='text urlfield' name='attachments[$post->ID][url]' value='" . esc_attr(wp_get_attachment_url($post->ID)) . "' /><br />
                         <button type='button' class='button urlnone' title=''>" . __('None') . "</button>
                         <button type='button' class='button urlfile' title='" . esc_attr(wp_get_attachment_url($post->ID)) . "'>" . __('File URL') . "</button>
                        ";
                $fields['url']['html'] = $html;
        }
        return $fields;
}


// ダッシュボードのウィジェットを消す
function remove_dashboard_widgets(){
  global$wp_meta_boxes;
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
//  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
//  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets');


// ダッシュボードのアクションを消す
function my_favorite_actions_menu($actions) {
    $actions = array();
     return $actions;
}
add_filter('favorite_actions', 'my_favorite_actions_menu');


//　フッターの文章を変更
function custom_admin_footer() {

}
add_filter('admin_footer_text', 'custom_admin_footer');



// Original custom comments function is written by mg12 - http://www.neoease.com/

if (function_exists('wp_list_comments')) {
	// comment count
	add_filter('get_comments_number', 'comment_count', 0);
	function comment_count( $commentcount ) {
		global $id;
		$_commnets = get_comments('post_id=' . $id);
		$comments_by_type = &separate_comments($_commnets);
		return count($comments_by_type['comment']);
	}
}


function custom_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	global $commentcount;
	if(!$commentcount) {
		$commentcount = 0;
	}
?>

 <li class="comment <?php if($comment->comment_author_email == get_the_author_meta('email')) {echo 'admin-comment';} else {echo 'guest-comment';} ?>" id="comment-<?php comment_ID() ?>">
  <div class="comment-meta">
   <div class="comment-meta-left">
  <?php if (function_exists('get_avatar') && get_option('show_avatars')) { echo get_avatar($comment, 35); } ?>
  
    <ul class="comment-name-date">
     <li class="comment-name">
<?php if (get_comment_author_url()) : ?>
<a id="commentauthor-<?php comment_ID() ?>" class="url <?php if($comment->comment_author_email == get_the_author_meta('email')) {echo 'admin-url';} else {echo 'guest-url';} ?>" href="<?php comment_author_url() ?>" rel="external nofollow">
<?php else : ?>
<span id="commentauthor-<?php comment_ID() ?>">
<?php endif; ?>

<?php comment_author(); ?>

<?php if(get_comment_author_url()) : ?>
</a>
<?php else : ?>
</span>
<?php endif;  $options = get_option('design-plus_options'); ?>
     </li>
     <li class="comment-date"><?php echo get_comment_time(__('F jS, Y', 'design-plus')); if ($options['time_stamp']) : echo get_comment_time(__(' g:ia', 'design-plus')); endif; ?></li>
    </ul>
   </div>

   <ul class="comment-act">
<?php if (function_exists('comment_reply_link')) { 
        if ( get_option('thread_comments') == '1' ) { ?>
    <li class="comment-reply"><?php comment_reply_link(array_merge( $args, array('add_below' => 'comment-content', 'depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => '<span><span>'.__('REPLY','design-plus').'</span></span>'))) ?></li>
<?php   } else { ?>
    <li class="comment-reply"><a href="javascript:void(0);" onclick="MGJS_CMT.reply('commentauthor-<?php comment_ID() ?>', 'comment-<?php comment_ID() ?>', 'comment');"><?php _e('REPLY', 'design-plus'); ?></a></li>
<?php   }
      } else { ?>
    <li class="comment-reply"><a href="javascript:void(0);" onclick="MGJS_CMT.reply('commentauthor-<?php comment_ID() ?>', 'comment-<?php comment_ID() ?>', 'comment');"><?php _e('REPLY', 'design-plus'); ?></a></li>
<?php } ?>
    <li class="comment-quote"><a href="javascript:void(0);" onclick="MGJS_CMT.quote('commentauthor-<?php comment_ID() ?>', 'comment-<?php comment_ID() ?>', 'comment-content-<?php comment_ID() ?>', 'comment');"><?php _e('QUOTE', 'design-plus'); ?></a></li>
    <?php edit_comment_link(__('EDIT', 'design-plus'), '<li class="comment-edit">', '</li>'); ?>
   </ul>

  </div>
  <div class="comment-content" id="comment-content-<?php comment_ID() ?>">
  <?php if ($comment->comment_approved == '0') : ?>
   <span class="comment-note"><?php _e('Your comment is awaiting moderation.', 'design-plus'); ?></span>
  <?php endif; ?>
  <?php comment_text(); ?>
  </div>

<?php } ?>