<?php get_header(); ?>
</head>
<body class="category double">
<div id="header">
<p class="siteName"><a href="<?php bloginfo('home'); ?>" title="<?php bloginfo('name'); ?> Indexへ戻る"><?php bloginfo('name'); ?></a></p>

</div>
<div id="content">
	<div id="main">
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a><?php
	if ($categories = get_vicuna_upper_category($cat, ' | ') ) {
		echo ' &gt; '. $categories;
	}
?> &gt; <span class="current"><?php single_cat_title(); ?></span></p>
		<h1><?php echo single_cat_title(); ?></h1>
<?php
	if (have_posts()) :
		while (have_posts()) : the_post();
?>

		<div class="section entry" id="entry<?php the_ID(); ?>">
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<ul class="info">
				<li class="date"><?php the_time('Y-m-d (D)') ?></li>
				<li class="category"><?php the_category(' | ') ?></li>
				<?php if (function_exists('the_tags')) : the_tags('<li class="tags">', ' | ', '</li>'); endif; ?>
				<?php edit_post_link('Edit.', '<li class="admin">', '</li>'); ?>
			</ul>
			<div class="textBody">
<?php the_content('Continue reading'); ?>
<?php if(isset($wph)) $wph->addHatena(); ?>
<p>ブログランキングに参加しています。よかったらクリックして下さい！<br />
<a href="http://interior.blogmura.com/" target="_blank"><img src="http://interior.blogmura.com/img/interior88_31.gif" width="88" height="31" border="0" alt="にほんブログ村 インテリアブログへ" /></a></p>
			</div>
			<ul class="reaction">
<?php
	$trackpingCount = get_vicuna_pings_count();

?>





				<?php if ('open' == $post->ping_status) : ?><li class="trackback"><a href="<?php the_permalink() ?>#trackback" title="<?php the_title(); ?>へのトラックバック" rel="nofollow">TrackBack</a>: <span class="count"><?php echo $trackpingCount; ?></span></li><?php else : ?><li>TrackBack (Close): <span class="count"><?php echo $trackpingCount; ?></span></li><?php endif; ?>
			</ul>
		</div>
<?php
		endwhile;
	endif;
?>
<?php vicuna_paging_link('next_label=Older Entries&prev_label=Newer Entries&indent=2') ?>
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a><?php
	if ($categories) echo ' &gt; '.$categories;
?> &gt; <span class="current"><?php single_cat_title(); ?></span></p>
	</div><!-- end main-->

<?php	get_sidebar(); ?>

<?php	get_footer(); ?>
