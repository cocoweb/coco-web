<?php	get_header(); ?>
</head>
<?php	if (have_posts()) : the_post(); ?>
<body class="individual double" id="entry<?php the_ID(); ?>">
<div id="header">
	<p class="siteName"><a href="<?php bloginfo('home'); ?>" title="<?php bloginfo('name'); ?> Indexへ戻る"><?php bloginfo('name'); ?></a></p>
<ul id="globalNavi">
<li><a href="http://www.coco-web.com/wordpress/">HOME</a></li>
<li><a href="http://www.coco-web.com/wordpress/about/">カーテンココ</a></li>
<li><a href="http://www.coco-web.com/wordpress/privacy-policy/">プライバシーポリシー</a></li>
<li><a href="http://www.coco-web.com/wordpress/law/">特定商取引法</a></li>
<li><a href="http://www.coco-web.com/wordpress/sitemap/">サイトマップ</a></li>
<li><a href="http://www.coco-web.com/wordpress/contactus/">お問い合わせ</a></li>
</ul>
</div>

<div id="content">
	<div id="main">
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a><?php
		$parent_pages = get_vicuna_page_navigation('sort_column=menu_order');
		if ($parent_pages) : ?> &gt; <?php echo $parent_pages; ?><?php endif; ?> &gt; <span class="current"><?php the_title(); ?></span></p>
		<h1><?php the_title(); ?></h1>
		<div class="entry">
			<ul class="info">
				<li class="date"><?php echo get_the_modified_time('Y-m-d (D) G:i'); ?></li>
				<?php edit_post_link('Edit.', '<li class="admin">', '</li>'); ?>
			</ul>
			<div class="textBody">
<?php the_content('Continue reading'); ?>
			</div>

<?php comments_template(); ?>

<p><a href="http://www.coco-web.com/" target="_blank"><img src="http://www.coco-web.com/wordpress/img/blogebanner.jpg" width="510" height="200" alt="オーダーカーテンのお仕立て" /></a></p>

		</div><!--end entry-->
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a><?php if ($parent_pages) : ?> &gt; <?php echo $parent_pages; ?><?php endif; ?> &gt; <span class="current"><?php the_title(); ?></span></p>
	</div><!-- end main-->



<?php	get_footer(); ?>
<?php endif; ?>
