<?php
/**
 * function.php for wp.Vicuna
 * author: ma38su
 */


/**
 * Display a tag clouds.
 */
function vicuna_tag_cloud( $args = '' ) {

	global $wp_rewrite;
	$defaults = array( 'levels' => 6, 'orderby' => 'name', 'order' => 'ASC', 'exclude' => '', 'include' => '' );

	$args = wp_parse_args( $args, $defaults );

	$tags = get_tags( array_merge($args, array('orderby' => 'count', 'order' => 'ASC') ) ); // Always query top tags

	if ( empty($tags) )
		return;

	extract($args);

	if ( !$tags )
		return;
	$counts = $tag_links = array();
	foreach ( (array) $tags as $tag ) {
		$counts[$tag->name] = $tag->count;
		$tag_links[$tag->name] = get_tag_link( $tag->term_id );
		if ( is_wp_error( $tag_links[$tag->name] ) )
			return $tag_links[$tag->name];
		$tag_ids[$tag->name] = $tag->term_id;
	}

	$min_count = min($counts);
	$step = (int) ((max($counts) - $min_count) / $levels) + 1;
	
	if ( $step <= 1 )
		$step = 1;

	// SQL cannot save you; this is a second (potentially different) sort on a subset of data.
	if ( 'name' == $orderby )
		uksort($counts, 'strnatcasecmp');
	else
		asort($counts);

	if ( 'DESC' == $order )
		$counts = array_reverse( $counts, true );

	$a = array();

	$rel = ( is_object($wp_rewrite) && $wp_rewrite->using_permalinks() ) ? ' rel="tag"' : '';

	foreach ( $counts as $tag => $count ) {
		$tag_id = $tag_ids[$tag];
		$tag_link = clean_url($tag_links[$tag]);
		$level = $levels - (int) (($count - $min_count) / $step);
		$tag = str_replace(' ', '&nbsp;', wp_specialchars( $tag ));
		$a[] = "<li class=\"level".$level."\"><a href=\"$tag_link\" title=\"" . attribute_escape( sprintf( __('%d Entries'), $count ) ) . "\"$rel>$tag</a></li>";
	}

	$return = "<ul class=\"tagCloud\">\n\t";
	$return .= join("\n\t", $a);
	$return .= "\n</ul>\n";

	if ( is_wp_error( $return ) )
		return false;
	else 
		echo apply_filters( 'vicuna_tag_cloud', $return, $tags, $args );
}


/**
 * Display a description for the blog.
 */
function vicuna_description () {
	$description = get_bloginfo('description');
	if ( !empty($description) ) {
		echo '<p class="description">' . $description . '</p>';
	}
}

/**
 * Display a pager(Newer | Older).
 */
function vicuna_paging_link($args = '') {
	global $paged, $wpdb, $wp_query;

	if (is_array($args))
		$r = &$args;
	else
		parse_str($args, $r);

	$defaults = array('next_label' => 'Next Page', 'prev_label' => 'Previous Page', 'indent' => '');
	$r = array_merge($defaults, $r);
	extract($r);

	if ($indent != '') {
		$indent = (int) $indent;

		for ($i = 0; $i < $indent; $i ++)
			$indentText .= "\t";
	}

	// get max_page
	if (!$max_page)
		$max_page = $wp_query->max_num_pages;

	// get paged
	if (!$paged)
		$paged = 1;

	// set next page number
	$nextpage = intval($paged) + 1;

	if(!is_single()) {
		if ($max_page > 1)
			echo "$indentText<ul class=\"flip pager\" id=\"flip2\">\n";
		if ($paged > 1) {
			echo "$indentText\t<li class=\"newer\"><a href=\"";
			previous_posts();
			echo '">'. preg_replace('/&([^#])(?![a-z]{1,8};)/', '&#038;$1', $prev_label) ."</a></li>\n";
		}
		if (empty($paged) || $nextpage <= $max_page) {
			echo "$indentText\t<li class=\"older\"><a href=\"";
			next_posts($max_page);
			echo '">'. preg_replace('/&([^#])(?![a-z]{1,8};)/', '&#038;$1', $next_label) ."</a></li>\n";
		}
	

		if ($max_page > 1)
			echo $indentText . '</ul>' . "\n";
	}
}

/**
 * Indent the body text. 
 */
function indent_entry_body($content) {
	// インデント数 (div.textBody p から見て)
	$indent = 4;
	for ($i = 0; $i < $indent; $i ++)
		$indentText .= "\t";

	$pre_flag = false;
	$arr_content = split("\n", $content);

	foreach ($arr_content as $line) {
		if(!$pre_flag) {
			if (strpos($line, "<pre") !== false)
				$pre_flag = true;

			$mes .= $indentText . $line . "\n";
		}
		else {
			if (strpos($line, "</pre>") !== false)
				$pre_flag = false;

			$mes .= $line . "\n";
		}
	}

	return rtrim($mes) . "\n";
}

add_action('the_content', indent_entry_body, 99);


/**
 * Replace the body text with some vicuna style.
 */
function replace_entry_body($content) {
	// インデント数 (div.textBody から見て)
	$indent = 3;
	for ($i = 0; $i < $indent; $i ++)
		$indentText .= "\t";

	// get the title of entry.
	$entry_title = get_the_title();

	// [for ver.2.2]
	// $content = preg_replace('/\s*<p><span id="more-([0-9]+?)"><\/span>(.*?)<\/p>/', "\n\t\t\t</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n$indentText\t<p>\\2</p>", $content);

	// [for ver.2.2]
	// $content = preg_replace('/\s*<span id="more-([0-9]+?)"><\/span>(.*?)<\/p>/', "\t</p>\n\t\t\t</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n$indentText\t<p>\\2</p>", $content);

	// [for ver.2.2]
	// $content = preg_replace('/\s*<p><span id="more-([0-9]+?)"><\/span>(.*?)<br\s*\/>/', "\n\t\t\t</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n<p>$indentText\t<p>\\2</p>", $content);

	if (is_page() || is_single()) {
		// Replace '<p><a id="more-**"></p>' by '<div class="textBody" id="extended">'.
		// $content = preg_replace('/\t<p(\s.+?=".+?">|>)<a id="more-([0-9]+?)"><\/a>(.*?)<\/p>/', "</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n$indentText\t<p\\1\\3</p>", $content);

		// <a class="more-link">が、<p>から始まらない場合の<a id="more-**">を<div class="textBody" id="extended">に置き換える
		// $content = preg_replace('/<a id="more-([0-9]+?)"><\/a>(.*?)<\/p>/', "</p>\n\t\t\t</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n$indentText\t<p>\\2</p>", $content);

		$content = preg_replace('/\s*<p><span id="more-([0-9]+?)"><\/span>(.*?)<\/p>/', "\n\t\t\t</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n$indentText\t<p>\\2</p>", $content);

		$content = preg_replace('/\s*<span id="more-([0-9]+?)"><\/span>(.*?)<\/p>/', "\t</p>\n\t\t\t</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n$indentText\t<p>\\2</p>", $content);

		$content = preg_replace('/\s*<p><span id="more-([0-9]+?)"><\/span>(.*?)<br\s*\/>/', "\n\t\t\t</div>\n$indentText<div class=\"textBody\" id=\"extended\">\n<p>$indentText\t<p>\\2</p>", $content);

	} else {
		// Replace '<p><a class="more-link">' by '<p class="continue"><a>'.
		// (href="hoge#more-**"を、href="hoge#extended"に置換)
		$content = preg_replace('/<p(\s.+?=".+?">|>)\s?<a href="(.+?)#more-([0-9]+?)" class="more-link">(.+?)<\/a><\/p>/', '<p class="continue"><a href="\2#extended" title="' . $entry_title . ' 全文を読む" rel="nofollow">\4</a></p>', $content);

		// <p>から始まらない場合の<a class="more-link">を<p class="continue"><a>に置き換える
		$content = preg_replace('/\s*<a href="(.+?)#more-([0-9]+?)" class="more-link">(.+?)<\/a><\/p>/', "</p>\n\t\t\t\t".'<p class="continue"><a href="\1#extended" title="' . $entry_title . ' 全文を読む" rel="nofollow">\3</a></p>', $content);

	}
	// Delete the tags '<p></p>', '<p class="hoge"></p>', '<p>&nbsp;</p>' and '<p class="hoge">&nbsp;</p>'
	$content = preg_replace('/\t*<p(\s.+?=".+?">|>)\s*?<\/p>\n/', '', $content);

	// Delete the tag "<br /></p>".
	$content = preg_replace("/<br \/>\s*<\/p>/", "</p>", $content); 

	return $content;
}


add_action('the_content', replace_entry_body, 100);

/**
 * Indent the comment body.
 */
function indent_comment_body($content) {
	// インデント数 (dl.log dd p から見て)
	$indent = 7;
	for ($i = 0; $i < $indent; $i ++)
		$indentText .= "\t";

	$pre_flag = false;
	$arr_content = split("\n", $content);

	foreach ($arr_content as $line) {
		if(!$pre_flag) {
			if (strpos($line, "<pre") !== false)
				$pre_flag = true;

			$mes .= $indentText . $line . "\n";
		}
		else {
			if (strpos($line, "</pre>") !== false)
				$pre_flag = false;

			$mes .= $line . "\n";
		}
	}

	return rtrim($mes) . "\n";
}

add_filter('comment_text', indent_comment_body, 100);

/**
 * Return a URI of the javascript for VICUNA.
 */
function get_vicuna_javascript_uri() {
	$javascript_uri = get_stylesheet_directory_uri() . "/script.js";
	return $javascript_uri;
}

/**
 * Return a title of Archive page.
 */
function get_vicuna_archive_title() {
	if ( is_day() ) /* If this is a daily archive */
		// [2007-04-03 変更部分]
		// return get_the_time('Y年 m月 d日');
		return get_the_time('Y-m-d');
	elseif ( is_month() ) /* If this is a monthly archive */
		// [2007-04-03 変更部分]
		// return get_the_time('Y年 m月');
		return get_the_time('Y-m');
	elseif ( is_year() ) /* If this is a yearly archive */
		// [2007-04-03 変更部分]
		// return get_the_time('Y年');
		return get_the_time('Y');
	elseif ( is_author() )
		return 'Author';
}

/**
 * Return a page navigation.
 */
function get_vicuna_page_navigation($args = '') {
	if ( is_array($args) )
		$r = &$args;
	else
		parse_str($args, $r);

	$defaults = array('depth' => 0, 'show_date' => '', 'date_format' => get_option('date_format'),
		'child_of' => 0, 'exclude' => '', 'echo' => 1, 'authors' => '', 'separator' => ' | ');
	$r = array_merge($defaults, $r);

	$output = '';
	$current_page = 0;

	// sanitize, mostly to keep spaces out
	$r['exclude'] = preg_replace('[^0-9,]', '', $r['exclude']);

	// Allow plugins to filter an array of excluded pages
	$r['exclude'] = implode(',', apply_filters('wp_list_pages_excludes', explode(',', $r['exclude'])));
	$separator = $r['separator'];
	// Query pages.
	$pages = get_pages($r);
	if ( !empty($pages) ) {
		global $wp_query;
		if ( is_page() ) {
			$current_page = $wp_query->get_queried_object_id();
			$flag = false;
			$output = '';
			$family = get_vicuna_upper_page($pages, array(), $current_page);
			array_shift($family);
			foreach ($family as $page) {
				if ( $flag ) {
					$output = $separator . $output;
				} else {
					$flag = true;
				}
				$output = '<a href="' .get_permalink($page->ID). "\">$page->post_title</a>". $output;
			}
			return $output;
		}
	}
}

/**
 * Return a page upper the page.
 */
function get_vicuna_upper_page($pages, $family, $page_id) {
	foreach ($pages as $page) {
		if ($page_id == $page->ID) {
			if (array_push($family, $page)) {
				$family = get_vicuna_upper_page($pages, $family, $page->post_parent);
				break;
			}
		}
	}
	return $family;
}

/**
 * Return a link for archives.
 */
function vicuna_archives_link($limit = '') {
	global $wp_locale, $wpdb;

	if ( '' != $limit ) {
		$limit = (int) $limit;
		$limit = ' LIMIT '.$limit;
	}

	$arcresults = $wpdb->get_results("SELECT DISTINCT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'publish' GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC" . $limit);
	if ( $arcresults ) {
		foreach ( $arcresults as $arcresult ) {
			$url    = get_month_link($arcresult->year,      $arcresult->month);
			$text	= sprintf("%04d-%02d", $arcresult->year, $arcresult->month);
			echo "\t<li><a href=\"$url\" title=\"$arcresult->posts\" rel=\"nofollow\">$text</a></li>\n";
		}
	}
}

/**
 * Return the upper category.
 */
function get_vicuna_upper_category($cat_id, $category_split = ' | ') {
	$parent = &get_category($cat_id);
	$name = $parent->cat_name;
	$flag = true;
	$parent_category = '';
	while ( $parent->category_parent ) {
		$tmp = '';
		$parent = &get_category($parent->category_parent);
		$tmp .= '<a href="'. get_category_link($parent->cat_ID).'">'.$parent->cat_name.'</a>';
		if ($flag) {
			$flag = false;
		} else {
			$tmp .= $category_split;
		}
		$parent_category = $tmp.$output;
	}
	return $parent_category;
}

/**
 * Return the total amount of pings.
 */
function get_vicuna_pings_count() {
	global $post, $wpdb, $id;
	$comments = $wpdb->get_results("SELECT * FROM $wpdb->comments WHERE comment_post_ID = '$post->ID' AND comment_approved = '1' AND comment_type != '' ORDER BY comment_date");
	// $comments = apply_filters( 'comments_array', $comments, $post->ID );
	return count($comments);
}

/**
 * Return all the posts.
 */
function get_all_posts() {
	global $wpdb;
	$posts = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_type = 'post' AND post_status = 'publish' ORDER BY ID DESC");
	// $comments = apply_filters( 'comments_array', $comments, $post->ID );
	return $posts;
}

/**
 * Return all the comments.
 */
function get_all_comments() {
	global $wpdb;
	$comments = $wpdb->get_results("SELECT * FROM $wpdb->comments WHERE comment_approved = '1' AND comment_type = '' ORDER BY comment_date");
	// $comments = apply_filters( 'comments_array', $comments, $post->ID );
	return $comments;
}

/**
 * Display a link to edit the comments for every posts.
 * (unsupported in page)
 */
function vicuna_edit_comments_link($link = 'Edit This Comments.', $before = '', $after = '') {
	global $post;
	if ( is_attachment() )
		return;

	if( $post->post_type == 'page' ) {
		return;
	} else {
	        if ( ! current_user_can('edit_post', $post->ID) )
	                return;
	        $file = 'post';
	}
	$location = get_option('siteurl') . "/wp-admin/edit.php?p=$post->ID&amp;c=1";
	echo $before . "<a href=\"$location\">$link</a>" . $after;
}


/**
 * Replace the category list with some vicuna style.
 */
function indent_lists($content) {
    // インデント数
    $indent = 6;

    // サブカテゴリのulのクラス名
    $childClassName = '';

    $param      = parse_args('indent=' . $indent, array('indent' => 6));
    $indentText = createIndentText($param['indent']);

    // replace tab
    $content = preg_replace('/\t/', '', $content);

    // replace a
    $content = preg_replace('/(>)(<a\s)/', "\\1\n\t\\2", $content);

    // replace a
    $content = preg_replace('/<\/a><\/li>/', "</a>\n</li>", $content);

    // replace count
    $content = preg_replace('/<\/a>\s\(([0-9]+)\)/', '</a><span class="count">\1</span>', $content);

    // replace <ul class='children'> -> <ul class="children">
    $content = preg_replace('/<ul class=\'children\'>/', '<ul' . (($childClassName) ? ' class="' . $childClassName . '"' : '') . '>', $content);

    // replace <li class="..."> -> <li>
    $content = preg_replace('/<li.+?>/', "<li>", $content);

    $ul_flag     = false;
    $arr_content = split("\n", rtrim($content));
    $dump        = '';

    foreach ($arr_content as $line) {
        if (strpos($line, '<ul') !== false || strpos($line, "</ul>") !== false) {
            $ul_flag  = ($ul_flag) ? false : true;
            $dump    .= $indentText . "\t" . $line . "\n";
        }
        else {
            $dump .= $indentText . (($ul_flag) ? "\t\t" : '' ) . $line . "\n";
        }
    }

    return $dump;
}

add_filter('wp_list_categories', indent_lists, 99);
add_filter('wp_list_pages', indent_lists, 99);

function createIndentText($n) {
    $str = '';

    for ($i = 0; $i < $n; $i++) {
        $str .= "\t";
    }

    return $str;
}

function parse_args($args = '', $defaults) {
    if (is_array($args)) {
        $r = &$args;
    }
    else {
        parse_str($args, $r);
    }

    return array_merge($defaults, $r);
}













?>
<?php
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
?>