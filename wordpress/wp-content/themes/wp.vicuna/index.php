<?php get_header(); ?>

</head>
<body class="mainIndex double">
<div id="header">
<p class="siteName"><a href="<?php bloginfo('home'); ?>"><?php bloginfo('name'); ?></a></p>

<ul id="globalNavi">
<li><a href="http://www.coco-web.com/wordpress/">HOME</a></li>
<li><a href="http://www.coco-web.com/wordpress/about/">カーテンココって？</a></li>
<li><a href="http://www.coco-web.com/wordpress/privacy-policy/">プライバシーポリシー</a></li>
<li><a href="http://www.coco-web.com/wordpress/law/">特定商取引法</a></li>
<li><a href="http://www.coco-web.com/wordpress/sitemap/">サイトマップ</a></li>
<li><a href="http://www.coco-web.com/wordpress/contactus/">お問い合わせ</a></li>
</ul>

</div>

<div id="content">
	<div id="main">
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>" title="Home">Home</a></p>
		<h1><?php bloginfo('name'); ?></h1>
<?php
	if (have_posts()) :
		while (have_posts()) : the_post();
?>

		<div class="section entry" id="entry<?php the_ID(); ?>">
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<ul class="info">
				<li class="date"><?php the_time('Y-m-d (D)') ?></li>
				<li class="category"><?php the_category(' | ') ?></li>
				<?php if (function_exists('the_tags')) : the_tags('<li class="tags">', ' | ', '</li>'); endif; ?>
				<?php edit_post_link('Edit.', '<li class="admin">', '</li>'); ?>
			</ul>
			<div class="textBody">
<?php the_content('Continue reading'); ?>

<p><a href="http://www.coco-web.com/" target="_blank"><img src="http://www.coco-web.com/wordpress/img/blogebanner.jpg" width="510" height="200" alt="オーダーカーテンのお仕立て" /></a></p>

<p>ブログランキングに参加しています。よかったらクリックして下さい！<br />
<a href="http://interior.blogmura.com/interior_coordinate/"><img src="http://interior.blogmura.com/interior_coordinate/img/interior_coordinate88_31.gif" width="88" height="31" border="0" alt="にほんブログ村 インテリアブログ インテリアコーディネートへ" /></a>
<a href="http://interior.blogmura.com/interior_curtain/"><img src="http://interior.blogmura.com/interior_curtain/img/interior_curtain88_31.gif" width="88" height="31" border="0" alt="にほんブログ村 インテリアブログ カーテン・ブラインドへ" /></a>
<a href="http://interior.blogmura.com/fabrics/"><img src="http://interior.blogmura.com/fabrics/img/fabrics88_31.gif" width="88" height="31" border="0" alt="にほんブログ村 インテリアブログ インテリアファブリックへ" /></a></p>

<p><a href="http://blog.with2.net/link.php?1408186">人気ブログランキングへ</a></p>

<p>下からこのブログをブックマークすることができます。</p>
<div style="width:480px; height:50px; padding:5px 5px 5px 5px; background-color:#ffffff; border:1px dotted #333333;">
<div style="width:480px;">
<a href='javascript:location.href="http://b.hatena.ne.jp/add?mode=confirm&amp;url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/hatenasbm.gif" border="0" alt="" />はてなに追加</a>
<a href='javascript:location.href="http://bookmarks.yahoo.co.jp/bookmarklet/showpopup?ei=UTF-8&amp;u="%2bencodeURIComponent(location.href)%2b"&amp;t="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/myyahoosbm.gif" border="0" alt="" />MyYahoo!に追加</a>
<a href='javascript:location.href="http://del.icio.us/post?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/delicioussbm.png" border="0" alt="" />del.icio.usに追加</a>
<a href='javascript:location.href="http://clip.livedoor.com/redirect?link="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/livedoorsbm.gif" border="0" alt="" />livedoorClipに追加</a>
</div>
<div style="float:left; padding-top:5px;">
<select name="jumpit" onChange="document.location.href=this.value">
<option selected value="#">その他ブックマークに追加</option>
<option value='javascript:location.href="http://www.google.com/bookmarks/mark?op=add&amp;bkmk="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Googleに追加</option>
<option value='javascript:location.href="http://www.technorati.com/faves?add="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Technoratiに追加</option>
<option value='javascript:location.href="http://buzzurl.jp/config/add/confirm?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Buzzurlに追加</option>
<option value='javascript:location.href="http://digg.com/submit?phase=2&amp;url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Diggに追加</option>
<option value='javascript:location.href="http://www.bloglines.com/sub?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Bloglinesに追加</option>
<option value='javascript:location.href="http://rss.drecom.jp/shortcut/add_clip?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>DRECOMに追加</option>
<option value='javascript:location.href="http://clip.nifty.com/create?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>niftyクリップに追加</option>
<option value='javascript:location.href="http://pookmark.jp/post?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>pookmarkに追加</option>
<option value='javascript:location.href="http://pingking.jp/bookmark/create?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>pingkingに追加</option>
<option value='javascript:location.href="http://www.choix.jp/submit?bookurl="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Choixに追加</option>
</select>
</div>
</div>

			</div>




			<ul class="reaction">
<?php
			$trackpingCount = get_vicuna_pings_count();
			$commentCount = (int) get_comments_number() - (int) $trackpingCount;
			if ('open' == $post->comment_status) : ?>















<?php
			endif;
			if ('open' == $post->ping_status) :
?>
				<li class="trackback"><a href="<?php the_permalink() ?>#trackback" title="<?php the_title(); ?>へのトラックバック" rel="nofollow">TrackBack</a>: <span class="count"><?php echo $trackpingCount; ?></span></li>
<?php			else : ?>
				<li>TrackBack (Close): <span class="count"><?php echo $trackpingCount; ?></span></li>
<?php			endif ?>
				
			</ul>






		</div>
<?php
		endwhile;
	endif;
?>
<?php vicuna_paging_link('next_label=Older Entries&prev_label=Newer Entries&indent=2') ?>
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>" title="Home">Home</a></p>
	</div><!-- end main-->

<?php	get_sidebar(); ?>

<?php	get_footer(); ?>
