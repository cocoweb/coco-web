<?php	get_header(); ?>
</head>
<body class="individual double" id="siteSearch">
<div id="header">
	<p class="siteName"><a href="<?php bloginfo('home'); ?>" title="<?php bloginfo('name'); ?> Indexへ戻る"><?php bloginfo('name'); ?></a></p>
<ul id="globalNavi">
<li><a href="http://www.coco-web.com/wordpress/">HOME</a></li>
<li><a href="http://www.coco-web.com/wordpress/about/">カーテンココ</a></li>
<li><a href="http://www.coco-web.com/wordpress/privacy-policy/">プライバシーポリシー</a></li>
<li><a href="http://www.coco-web.com/wordpress/law/">特定商取引法</a></li>
<li><a href="http://www.coco-web.com/wordpress/contactus/">お問い合わせ</a></li>
</ul>
</div>

<div id="content">
	<div id="main">
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a> &gt; <span class="current">Search</span></p>
		<h1><?php bloginfo('name'); ?> - <em><?php echo wp_specialchars($s, 1); ?></em> による検索結果 : <span class="count"><?php $allsearch =& new WP_Query("s=$s&showposts=-1"); echo $allsearch->post_count; ?></span>件</h1>
<?php
	if (have_posts()) :
		while (have_posts()) : the_post(); ?>
		<div class="section entry">
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<ul class="info">
				<li class="date"><?php the_time('Y-m-d (D)') ?></li>
				<li class="category"><?php the_category(' | ') ?></li>
				<?php if (function_exists('the_tags')) : the_tags('<li class="tags">', ' | ', '</li>'); endif; ?>
				<?php edit_post_link('Edit.', '<li class="admin">', '</li>'); ?>
			</ul>
			<div class="textBody">
<?php the_content('Continue reading'); ?>
			</div>
			<ul class="reaction">
<?php
			$trackpingCount = get_vicuna_pings_count();
			$commentCount = (int) get_comments_number() - (int) $trakpingsCount;
			if ('open' == $post->comment_status) : ?>
				<li class="comment"><a href="<?php the_permalink() ?>#comments" title="&quot;<?php the_title(); ?>&quot;へのコメント" rel="nofollow">Comments</a>: <span class="count"><?php echo $commentCount; ?></span></li>
<?php			else : ?>
				<li>Comments (Close): <span class="count"><?php echo $commentCount; ?></span></li>
<?php
			endif;
			if ('open' == $post->ping_status) :
?>
				<li class="trackback"><a href="<?php the_permalink() ?>#trackback" title="&quot;<?php the_title(); ?>&quot;へのトラックバック" rel="nofollow">TrackBack</a>: <span class="count"><?php echo $trackpingCount; ?></span></li>
<?php			else : ?>
				<li>TrackBack (Close): <span class="count"><?php echo $trackpingCount; ?></span></li>
<?php			endif ?>
			</ul>
		</div>
<?php
		endwhile;
	elseif (0) : ?>
		<div class="section entry">
				<h2>検索結果</h2>
				<div class="textBody">
					<p>検索キーワードが入力されていません。</p>
				</div>
		</div>
<?php	else : ?>
		<div class="section entry">
				<h2>検索結果</h2>
				<div class="textBody">
					<p><em><?php echo wp_specialchars($s, 1); ?></em>というキーワードを含む記事は見つかりませんでした。次項のヒントを参考にして、検索キーワードを変えてもう一度検索してみてください。</p>
					<ul>
						<li>キーワードに誤字や脱字がありませんか ?</li>
						<li>キーワードの意味はそのままで、表現や言い回しを変えてみてください。</li>
						<li>専門的なキーワードだったり、長い文字列のキーワードだったりした場合は、より一般的で短いキーワードにしてください。</li>
						<li>キーワードを複数指定している場合は、キーワードの数を減らしてみてください。</li>
					</ul>
				</div>
		</div>
<?php	endif; ?>
<?php vicuna_paging_link('next_label=Older Entries&prev_label=Newer Entries&indent=2') ?>
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a> &gt; <span class="current">Search</span></p>
	</div><!-- end main -->

<?php	get_sidebar(); ?>

<?php	get_footer(); ?>
