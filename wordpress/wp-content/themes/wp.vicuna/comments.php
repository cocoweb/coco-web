<?php // Do not delete these lines
	if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if (!empty($post->post_password)) { // if there's a password
		if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie?>
			<p class="nocomments">This post is password protected. Enter the password to view comments.<p>
<?php
			return;
		}
	}

	/* This variable is for alternating comment background */
	$oddcomment = 'alt';
?>
<?php	/* コメントとトラックバック、ピンバックを分離します。*/
	$trackpingCount = 0;
	$commentCount = 0;
	if ($comments) :
		foreach ($comments as $comment) {
			$type = get_comment_type();
			switch( $type ) {
				case 'trackback' :
				case 'pingback' :
					$trackpingArray[$trackpingCount++] = $comment;
					break;
				default :
					$commentArray[$commentCount++] = $comment;
			}
		}
	endif;
?>
<?php	if ($commentCount > 0 || 'open' == $post->comment_status) : ?>
			<div class="section" id="comments">
				<h2>Comments:<span class="count"><?php echo $commentCount ?></span></h2>
<?php		if ($commentCount > 0) : ?>
<?php vicuna_edit_comments_link('Edit this comments.', '<p class="admin">', '</p>'); ?>
				<dl class="log">
				<?php			foreach ($commentArray as $comment) : ?>
<dt id="comment<?php comment_ID() ?>"><span class="name"><?php comment_author_link() ?></span> <span class="date"><?php comment_date('y-m-d (D) G:i') ?></span> <?php edit_comment_link('Edit.','<span class="admin">','</span>'); ?></dt>
				<dd>
					<?php comment_text() ?>
				</dd>
				<?php 			endforeach; ?>
				
				</dl>
<?php		endif; ?>
<?php	endif; ?>
<?php if ('open' == $post->comment_status) : ?>
			<form class="post" method="post" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" id="commentsForm" onsubmit="if (this.bakecookie[0].checked) rememberMe(this)">
				<fieldset>
				<legend>Comment Form</legend>
				<div>
					<input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
				</div>
				<dl id="name-email">
<?php 	if ( $user_ID ) : ?>
					<dt>Logged in</dt>
					<dd><?php echo $user_identity; ?> (<a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Logout</a>)</dd>
<?php	else : ?>
					<dt><label for="comment-author">Name<?php if ($req) echo " (required)"; ?></label></dt>
					<dd><input type="text" class="inputField" id="comment-author" name="author" size="20" value="" /></dd>
					<dt><label for="comment-email">Mail address (will not be published)<?php if ($req) echo " (required)"; ?></label></dt>
					<dd><input type="text" class="inputField" size="20" id="comment-email" name="email" value="" /></dd>
<?php	endif; ?>
				</dl>
				<dl>
<?php 	if ( !$user_ID ) : ?>
					<dt><label for="comment-url"><abbr title="Uniform Resource Identifer">URI</abbr></label></dt>
					<dd><input type="text" class="inputField" id="comment-url" name="url" size="20" value="http://" /></dd>
					<dt>Remember personal info</dt>
					<dd><input type="radio" class="radio" id="bakecookie" name="bakecookie" /> <label for="bakecookie">Yes</label><input type="radio" class="radio" id="forget" name="bakecookie" onclick="forgetMe(this.form)" onkeypress="forgetMe(this.form)" value="Forget Info" /> <label for="forget">No</label></dd>
<?php	endif; ?>
					<dt>
					<label for="comment-text">
					Comment
<?php	if ( allowed_tags() ) : ?>
					<span>スタイル指定用の一部の <abbr title="Hyper Text Markup Language">HTML</abbr>タグが使用できます。</span>
<?php	else : ?>
					<span><abbr title="Hyper Text Markup Language">HTML</abbr>タグは使用できません</span>
<?php	endif; ?>
					</label>
					</dt>
					<dd><textarea id="comment-text" name="comment" rows="8" cols="50" onfocus="if (this.value == 'Add Your Comment') this.value = '';" onblur="if (this.value == '') this.value = 'Add Your Comment';">Add Your Comment</textarea></dd>
				</dl>
				<div class="action">
					<input type="submit" class="submit post" id="comment-post" name="post" value="Post" />
				</div>
				</fieldset>
<?php if ( !$user_ID ) : ?>
				<script type="text/javascript">
					applyCookie ('comments_form', '<?php echo $_SERVER['HTTP_HOST']; ?>');
				</script>
<?php endif; ?>
			</form>
			</div><!-- end div#comment -->
<?php	endif; ?>

<?php	if ($trackpingCount > 0 || 'open' == $post->ping_status) : ?>
			<div class="section" id="trackback">
				<h2>Trackback+Pingback:<span class="count"><?php echo $trackpingCount; ?></span></h2>
<?php	if ('open' == $post->ping_status) : ?>
				<dl class="info">
				<dt>TrackBack URL for this entry</dt>
				<dd class="URL"><?php trackback_url(); ?></dd>
				<dt>Listed below are links to weblogs that reference</dt>
				<dd><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> from <a href="<?php bloginfo('home'); ?>"><?php bloginfo('name'); ?></a></dd>
				</dl>
<?php	endif; ?>
<?php	if ($trackpingCount > 0) : ?>
				<dl class="log">
				<?php			foreach ($trackpingArray as $comment) : ?>
<dt id="ping<?php comment_ID() ?>"><span class="name"><?php /* <a href="<$MTPingURL$>"><$MTPingTitle$> from <$MTPingBlogName$></a> */ ?><?php comment_type(); ?> from <?php echo comment_author_link(); ?></span> <span class="date"><?php comment_date('y-m-d (D) G:i'); ?></span></dt>
				<dd>
					<?php comment_text() ?>
				</dd>
				<?php		endforeach; ?>
				
				</dl>
<?php	endif; ?>
			</div><!-- end div#trackback -->
<?php	endif; ?>
