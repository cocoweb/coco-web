<?php	get_header(); ?>
</head>
<body class="category double">
<div id="header">
	<p class="siteName"><a href="<?php bloginfo('home'); ?>" title="<?php bloginfo('name'); ?> Indexへ戻る"><?php bloginfo('name'); ?></a></p>
</div>
<div id="content">
	<div id="main">
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a> &gt; Tags &gt; <span class="current"><?php single_tag_title(); ?></span></p>
		<h1><?php single_cat_title(); ?></h1>
<?php
	if (have_posts()) :
		while (have_posts()) : the_post();
?>

		<div class="section entry" id="entry<?php the_ID(); ?>">
			<h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
			<ul class="info">
				<li class="date"><?php the_time('Y-m-d (D)') ?></li>
				<li class="category"><?php the_category(' | ') ?></li>
				<?php if (function_exists('the_tags')) : the_tags('<li class="tags">', ' | ', '</li>'); endif; ?>
				<?php edit_post_link('Edit.', '<li class="admin">', '</li>'); ?>
			</ul>
			<div class="textBody">
<?php the_content('Continue reading'); ?>
			</div>
			<ul class="reaction">
<?php
	$trackpingCount = get_vicuna_pings_count();
	$commentCount = (int) get_comments_number() - (int) $trackpingCount;
?>
				<?php if ('open' == $post->comment_status) : ?><li class="comment"><a href="<?php the_permalink() ?>#comments" title="<?php the_title(); ?>へのコメント" rel="nofollow">Comments</a>: <span class="count"><?php echo $commentCount ?></span></li><?php else : ?><li>Comments (Close): <span class="count"><?php echo $commentCount ?></span></li><?php endif; ?>
				<?php if ('open' == $post->ping_status) : ?><li class="trackback"><a href="<?php the_permalink() ?>#trackback" title="<?php the_title(); ?>へのトラックバック" rel="nofollow">TrackBack</a>: <span class="count"><?php echo $trackpingCount; ?></span></li><?php else : ?><li>TrackBack (Close): <span class="count"><?php echo $trackpingCount; ?></span></li><?php endif; ?>
			</ul>
		</div>
<?php
		endwhile;
	endif;
?>
<?php vicuna_paging_link('next_label=Older Entries&prev_label=Newer Entries&indent=2') ?>
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a> &gt; Tags &gt; <span class="current"><?php single_cat_title(); ?></span></p>
	</div><!-- end main-->

<?php	get_sidebar(); ?>

<?php	get_footer(); ?>
