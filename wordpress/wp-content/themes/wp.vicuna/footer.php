	<p class="return"><a href="#header" title="このページの先頭へ戻る">Return to page top</a></p>
</div><!--end content-->

<div id="footer">
	<ul class="support">
		<li>Powered by <a href="http://wordpress.org/">WordPress <?php bloginfo('version'); ?></a></li>
		<li class="template"><a href="http://vicuna.jp/">vicuna CMS</a> - <a href="http://wp.vicuna.jp/" title="ver.1.5.3">WordPress Theme</a></li>

<li>Produced by <a href="http://www.townet.jp/" rel="nofollow" target="_blank" title="タウネットワン">タウネットワン</a></li>
	</ul>
	<address>Copyright &copy; <?php bloginfo('name'); ?> All Rights Reserved.</address>
</div>
<?php	wp_footer(); ?>
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
var pageTracker = _gat._getTracker("UA-745009-3");
pageTracker._initData();
pageTracker._trackPageview();
</script>
</body>
</html>
