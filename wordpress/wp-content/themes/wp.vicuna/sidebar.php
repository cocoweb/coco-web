<div id="utilities">
<dl class="navi">

<dt>About</dt>
<dd>
<p><img src="http://www.coco-web.com/wordpress/wp-content/themes/wp.vicuna/style-flat/images/staff.gif" width="200" height="147" alt="カーテンココスタッフ" /></p>
<p>カーテンココブログではオーダーカーテン専門店<a href="http://www.coco-web.com/" title="カーテンココ" target="_blank">カーテンココ</a>のイベントなど最新情報やスタッフからのお知らせ、カーテンの枠を超えたインテリア全般の情報をご紹介しています。</p>
<p>オーダーカーテン専門店<br />カーテンココ</p>
<p>受付時間：AM10:00〜PM6:00</p>
<p>電話番号：0120-499-455</p>
<div id="coco-banner"><a href="http://www.coco-web.com/" target="_blank"></a></div>
<div id="coco-banner2"><a href="http://www.coco-decor.com/" target="_blank"></a></div>
<div id="coco-banner3"><a href="http://www.coco-luxe.jp/" target="_blank"></a></div>
</dd>

<script src="http://widgets.twimg.com/j/2/widget.js"></script>
<script>
new TWTR.Widget({
  version: 2,
  type: 'profile',
  rpp: 7,
  interval: 6000,
  width: 190,
  height: 600,
  theme: {
    shell: {
      background: '#990000',
      color: '#ffffff'
    },
    tweets: {
      background: '#fff2ff',
      color: '#52484c',
      links: '#990000'
    }
  },
  features: {
    scrollbar: true,
    loop: false,
    live: true,
    hashtags: true,
    timestamp: true,
    avatars: false,
    behavior: 'all'
  }
}).render().setUser('curtainCOCO').start();
</script>

<iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2FcurtainCOCO&amp;width=190&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=true&amp;header=true&amp;height=500" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:190px; height:500px;" allowTransparency="true"></iframe>


<dt>Categories</dt>
<dd>
<ul class="category">
<?php wp_list_cats('sort_column=name&optioncount=0&hierarchical=1'); ?>
</ul>
</dd>


<dt>Recent Entries</dt>
<dd>
<ul class="recentEntries">
<?php wp_get_archives('type=postbypost&limit=5'); ?>
</ul>
</dd>


<div style="width:170px; padding:5px 5px 5px 5px; background-color:#ffffff; border:1px dotted #333333;">
<a href='javascript:location.href="http://b.hatena.ne.jp/add?mode=confirm&amp;url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/hatenasbm.gif" border="0" alt="" /> はてなに追加</a><br />
<a href='javascript:location.href="http://bookmarks.yahoo.co.jp/bookmarklet/showpopup?ei=UTF-8&amp;u="%2bencodeURIComponent(location.href)%2b"&amp;t="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/myyahoosbm.gif" border="0" alt="" /> MyYahoo!に追加</a><br />
<a href='javascript:location.href="http://del.icio.us/post?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/delicioussbm.png" border="0" alt="" /> del.icio.usに追加</a><br />
<a href='javascript:location.href="http://clip.livedoor.com/redirect?link="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);' style="font-size:12px; text-decoration:none;"><img src="http://www.ec-images.com/images/smo/sbm/livedoorsbm.gif" border="0" alt="" /> livedoorClipに追加</a><br />
<select name="jumpit" onChange="document.location.href=this.value">
<option selected value="#">その他ブックマークに追加</option>
<option value='javascript:location.href="http://www.google.com/bookmarks/mark?op=add&amp;bkmk="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Googleに追加</option>
<option value='javascript:location.href="http://www.technorati.com/faves?add="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Technoratiに追加</option>
<option value='javascript:location.href="http://buzzurl.jp/config/add/confirm?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Buzzurlに追加</option>
<option value='javascript:location.href="http://digg.com/submit?phase=2&amp;url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Diggに追加</option>
<option value='javascript:location.href="http://www.bloglines.com/sub?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Bloglinesに追加</option>
<option value='javascript:location.href="http://rss.drecom.jp/shortcut/add_clip?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>DRECOMに追加</option>
<option value='javascript:location.href="http://clip.nifty.com/create?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>niftyクリップに追加</option>
<option value='javascript:location.href="http://pookmark.jp/post?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>pookmarkに追加</option>
<option value='javascript:location.href="http://pingking.jp/bookmark/create?url="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>pingkingに追加</option>
<option value='javascript:location.href="http://www.choix.jp/submit?bookurl="%2bencodeURIComponent(location.href)%2b"&amp;title="%2bencodeURIComponent(document.title);'>Choixに追加</option>
</select>
</div>




	</dl>

<!--end navi-->
	<dl class="others">
		<dt>Search</dt>
		<dd>
			<form method="get" action="<?php bloginfo('home'); ?>/">
				<fieldset>
					<legend><label for="searchKeyword"><?php bloginfo('name'); ?>内の検索</label></legend>
					<div>
						<input type="text" class="inputField" id="searchKeyword"  name="s" size="10" onfocus="if (this.value == 'Keyword(s)') this.value = '';" onblur="if (this.value == '') this.value = 'Keyword(s)';" value="<?php if ( is_search() ) echo wp_specialchars($s, 1); else echo 'Keyword(s)'; ?>" />
						<input type="submit" class="submit" id="submit" value="Search" />
					</div>
				</fieldset>
			</form>
		</dd>
		<dt>Feeds</dt>
		<dd>
			<ul class="feeds">
				<li class="rss"><a href="<?php bloginfo('rss2_url'); ?>">All Entries(RSS2.0)</a></li>
				<li class="atom"><a href="<?php bloginfo('atom_url'); ?>">All Entries(Atom)</a></li>
				
			</ul>
		</dd>

	</dl><!--end others-->
</div><!--end utilities-->
