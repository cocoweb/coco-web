<?php	get_header(); ?>
</head>
<body class="individual double">
<div id="header">
	<p class="siteName"><a href="<?php bloginfo('home'); ?>" title="<?php bloginfo('name'); ?> Indexへ戻る"><?php bloginfo('name'); ?></a></p>
	<?php vicuna_description(); ?>
</div>

<div id="content">
	<div id="main">
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a> &gt; <span class="current">Error 404</span></p>
		<h1>Error 404 - Not Found</h1>
		<div class="entry">
<div class="textBody">
<p>Sorry, but you are looking for something that isn't here.</p>
</div>

		</div><!--end entry-->
		<p class="topicPath"><a href="<?php bloginfo('home'); ?>">Home</a> &gt; <span class="current">Error 404</span></p>
	</div><!-- end main-->

<?php	get_sidebar(); ?>

<?php	get_footer(); ?>
