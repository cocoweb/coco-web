		<div id="footer">
		
			<?php 
			$footer_widget_option = (themify_get('setting-footer_widgets') == "") ? "footerwidget-3col" : themify_get('setting-footer_widgets');
			if($footer_widget_option != ""){ ?>
					  <?php
					  $columns = array('footerwidget-4col' 	=> array('col col4-1','col col4-1','col col4-1','col col4-1'),
											 'footerwidget-3col'	=> array('col col3-1','col col3-1','col col3-1'),
											 'footerwidget-2col' 	=> array('col col4-2','col col4-2'),
											 'footerwidget-1col' 	=> array('') );
					  $x=0;
					  ?>
					<?php foreach($columns[$footer_widget_option] as $col): ?>
							<?php 
								 $x++;
								 if($x == 1){ 
									  $class = "first"; 
								 } else {
									  $class = "";	
								 }
							?>
							<div class="<?php echo $col;?> <?php echo $class; ?>">
								 <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Footer_Widget_'.$x) ) ?>
							</div>
					  <?php endforeach; ?>
			<?php } ?>
			
			<div class="footer-text credits">
				<div class="one"><?php if(themify_get('setting-footer_text_left') != ""){ echo themify_get('setting-footer_text_left'); } else { echo '&copy; <a href="'.get_option('home').'">'.get_bloginfo('name').'</a> '.date('Y'); } ?></div>
				<div class="two"><?php if(themify_get('setting-footer_text_right') != ""){ echo themify_get('setting-footer_text_right'); } else { echo 'Powered by <a href="http://wordpress.org">WordPress</a>  &bull; <a href="http://themify.me">Themify WordPress Themes</a>'; } ?></div>
			</div>
			<!-- /.footer-text -->
		
		</div>
		<!--/footer -->
	
	</div>
	<!--/pagewrap -->

</div>
<!-- /#bg -->

<?php
/**
 *  Stylesheets and Javascript files are enqueued in theme-functions.php
 */
?>

<?php wp_footer(); ?>

</body>
</html>
