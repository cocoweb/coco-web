<?php
// For Install Wizard

define('_LANG_INST_GUIDE_WPCONFIG','サーバー上に <code>wp-config.php</code> が存在しません。<br />WordPress のインストールにはこの設定ファイルが必要です。<br /><br />こちらの<a href="%ssetup-config.php">ウィザード</a>を利用してサーバー上で <code>wp-config.php</code> を作成することができます。<br /><br />ただし、この方法はすべての環境での動作を保障することができませんのでご了承下さい。ウィザードを利用できない場合は、<a href="http://wordpress.xwd.jp/local/">こちらのページ</a>と同梱されている <code>wp-config-sample.php</code> を参考に、テキストエディタでファイルを作成し、サーバーに転送してからブラウザで <code>wp-admin/install.php</code> にアクセスしてインストールを実行します。');

/* File Name wp-admin/setup-config.php */
define('_LANG_WA_CONFIG_GUIDE1','<p>ファイル <code>wp-config.php</code> は既に存在します。このファイル中の情報のうちのどれかをリセットする必要がある場合は、まずこのファイルをサーバー上から削除してください。</p></body></html>');
define('_LANG_WA_CONFIG_GUIDE2','このウィザードでは <code>wp-config-sample.php</code>ファイルを必要とします。<br />このファイルがサーバーにアップロードされているか再度確認してください。');
define('_LANG_WA_CONFIG_GUIDE3','インストール対象ディレクトリ <code>%s</code> に書き込み権限を与えてください。WordPress ディレクトリのパーミッションを変更出来ない場合は、<code>wp-config-sample.php</code> を参考に手動で <code>wp-config.php</code> を作成してください。');
define('_LANG_WA_CONFIG_GUIDE4','WordPress インストールウィザードへようこそ。<br />セッティングにはデータベース (MySQL) についての情報が必要です。<br />プロセス進行の前にこれらの情報を準備してください。');
define('_LANG_WA_CONFIG_DATABASE','データベース名');
define('_LANG_WA_CONFIG_USERNAME','ユーザー名');
define('_LANG_WA_CONFIG_PASSWORD','パスワード');
define('_LANG_WA_CONFIG_LOCALHOST','ホスト名');
define('_LANG_WA_CONFIG_PREFIX','テーブル接頭語');
define('_LANG_WA_CONFIG_ENCODE','ブログの文字コード');
define('_LANG_WA_CONFIG_GUIDE5','何らかの理由でこのウィザードが機能しない場合でも心配要りません。データベース設定のすべては別ファイルにて行うことが出来ます。同梱されている <code>wp-config-sample.php</code> をテキストエディタで開き、必要情報を記述したうえで <code>wp-config.php</code> とリネームして保存後に、サーバーへアップロードしてください。</p><p>あなたのデータベース情報が不明な場合は、このウィザードを進める前にレンタルサーバー(ホスティング)サービスに問い合わせてください。すべて準備ができている場合は<a href="setup-config.php?step=1">こちらをクリック</a>してください。');
define('_LANG_WA_CONFIG_GUIDE6','以下のフォームに、あなたのデータベース接続詳細を入力してください。<br />不明な場合はレンタルサーバー(ホスティング)サービスにお問い合わせ下さい。');
define('_LANG_WA_CONFIG_GUIDE7','WordPress の情報を格納するためのデータベース名');
define('_LANG_WA_CONFIG_GUIDE8','MySQL ユーザー名');
define('_LANG_WA_CONFIG_GUIDE9','MySQL パスワード');
define('_LANG_WA_CONFIG_GUIDE10','ほとんどの環境において、localhost のまま変更する必要はありません');
define('_LANG_WA_CONFIG_GUIDE11','複数の WordPress をインストールする場合は個々に変更してください');
define('_LANG_WA_CONFIG_GUIDE12','データベース接続に必要な情報がすべて揃いました。<br />引き続き <a href="install.php">WordPress のインストール</a>を実行してください。');
define('_LANG_WA_CONFIG_GUIDE17','<code>UTF-8</code> での運用に問題があるようでしたら <a href="http://sourceforge.jp/projects/wordpress/files/">ME2.0</a> 系をご利用ください。');

/* File Name wp-include/wp-db.php */
define('_LANG_WA_WPDB_GUIDE1','<b>データベースに接続できません。</b><br />これは、あなたの入力情報が正しくないことを意味します。<br />以下の内容を確認して再度接続を試みてください。');
define('_LANG_WA_WPDB_GUIDE2','ユーザー名、パスワードを正確に記入しましたか ?');
define('_LANG_WA_WPDB_GUIDE3','ホスト名を正確に記入しましたか ?');
define('_LANG_WA_WPDB_GUIDE4','データベースは作成済ですか ?');

/* File Name wp-admin/upgrade.php */
define('_LANG_UPG_STEP_INFO','<p>サーバー上に <code>wp-config.php</code> ファイルが存在しません<br />適切なデータベース接続情報を備えた <code>wp-config.php</code> ファイルを用意してください。</p>');
?>