#!/usr/bin/perl


# ↑ 上記をお使いのサーバーパスに書き換えて下さい。
#
#
# ↓ 以下は触らないで下さい。
#
##########################################################
#
#              最新記事表示プログラム
#                    Ver.0.9.5
#               LWPライブラリ未使用版
#          （記事の更新日は取得できません）
#
##########################################################

require('getHTTP.cgi');
require('setting.pl');
use Jcode;
use CGI;

if ($set_url ne '') { &check_http_referer; }

$q = new CGI;

$file  = $path . $q->param('file');

$url  = $q->param('url');
$key  = $q->param('key');
$kind = $q->param('kind');
$num  = $q->param('num');
$sort = $q->param('sort');
$code = $q->param('code');
$type = $q->param('type');
$dflg = $q->param('desc');
$orflg = $q->param('or');
$toflg = $q->param('to');

$lfh = my_flock() or die &onExitShowRss('Busy!');

$up_date = '';
open(FILE, "<$file");
while(<FILE>){
	push @lines, $_;
	if($_ =~ /<!--upcode:.*-->/){
	    $up_date = $_;
	    $up_date =~ s/<!--upcode://i;
	    $up_date =~ s/-->//i;
	}
}
close FILE;

if($up_date && $interval > 0){
    $up_date += $interval * 60 * 60;
    if($up_date > time){ &onExitShowRss('No'); }
}

if($key) {
	$url = $key;
	Jcode::convert(\$url,'utf8');
	$url =~ s/([^\w ])/'%'.unpack('H2', $1)/eg;
	$url =~ tr/ /+/;
	# Yahooニュース
	if($kind eq 'YN') {
		#$url = 'http://nsearch.yahoo.co.jp/bin/search?p=' . $url;
		$url = 'http://news.search.yahoo.co.jp/search?p=' . $url;
		#if($orflg) { $url .= '&or=1'; }
		if($orflg) { $url .= '&vaop=o'; }
		if($toflg) { $url .= '&to=1'; }
	}
	# Yahoo News
	elsif($kind eq 'YNE') { $url = 'http://news.search.yahoo.com/news/rss?toggle=1&ei=UTF-8&datesort=1&p=' . $url; }
	# Yahooブログ
	elsif($kind eq 'Y') {
		if($orflg) { $url = '(' . $url . ')'; }
		if($toflg) { $url = 'title%3A' . $url; }
		$url = 'http://blog-search.yahoo.co.jp/rss?sq=M&ei=UTF-8&p=' . $url . '&n=' . $num;
		if($sort) { $url .= '&so=dd'; }
		else {$url .= '&so=gd'; }
	}
	# Googleニュース（RSSページがなくなった？ので取得できなくなった）
	elsif($kind eq 'GN') { $url = 'http://news.google.com/news?hl=ja&ned=us&ie=UTF-8&output=rss&q=' . $url . '&num=' . $num; }
	# Google News（RSSページがなくなった？ので取得できなくなった）
	elsif($kind eq 'GNE') { $url = 'http://news.google.com/news?hl=en&ned=us&ie=UTF-8&output=rss&q=' . $url . '&num=' . $num; }
	# Googleブログ
	else {
		my $dmy = 'as_q=';
		if($orflg) { $dmy .= '&as_oq='; }
		if($toflg) { $dmy .= '&as_q=&bl_bt='; }
		$url = 'http://www.google.co.jp/blogsearch_feeds?hl=ja&lr=lang_ja&ie=utf-8&output=rss&' . $dmy . $url . '&num=' . $num;
		if($sort) { $url .= '&scoring=d'; }
	}
}

if(!$url){ &onExitShowRss('No URI is specified'); }

($http_response, $errorMessage) = &getHTTP($url);

$http_response =~ s/[\n\r]//g;

$code2 = 'sjis';
if ($code eq 'euc' || $code eq 'jis' || $code eq 'sjis' || $code eq 'utf8') { $code2 = $code; }

my @items;
if($kind eq 'YN') {
	@items = &parse_yn($http_response, $num);
} elsif ($type ne "atom") {
	@items = &parse_rss($http_response, $num);
} else {
	@items = &parse_atom($http_response, $num);
}

$rss_data = "<!--upcode:" . time . "-->\n";
if (@items) {
	$rss_data .= "<dl>\n";
	foreach (@items) {
	    $title = $_->{'title'};
	    Jcode::convert(\$title,$code2);
	    $title =~ s/&lt;b&gt;//ig;
	    $title =~ s/&lt;\/b&gt;//ig;
	    $tmp = $_->{'link'};
		$rss_data .= "<dt><a href=\"$tmp\"";
		if($target == 1){ $rss_data .= " target=\"_blank\""; }
		if($follow == 1){ $rss_data .= " rel=\"nofollow\""; }
		$rss_data .= ">$title</a></dt>\n";
	    $desc = $_->{'description'};
		if ($desc && $dflg) {
		    Jcode::convert(\$desc,$code2);
		    $desc =~ s/&lt;p&gt;.*//i;
		    $desc =~ s/&lt;b&gt;//ig;
		    $desc =~ s/&lt;\/b&gt;//ig;
		    $desc =~ s/<b>//ig;
		    $desc =~ s/<\/b>//ig;
			$rss_data .= "<dd>$desc</dd>\n";
		}
	}
	$rss_data .= "</dl>\n";
}

my $flag = 0;
my $check = 0;
Jcode::convert(\$InsertS,$code2);
Jcode::convert(\$InsertE,$code2);
open(OUT, ">$file");
foreach(@lines){
	if($flag == 1 and $check == 0){
		$check = 1;
		print OUT $rss_data;
		if($_ =~ /$InsertE/){
			$flag = 0;
			$check = 0;
			print OUT $_;
			next;
		}
		next;
	}
	if($_ =~ /$InsertS/){
		$flag = 1;
	}
	if($_ =~ /$InsertE/){
		$flag = 0;
		$check = 0;
		print OUT $_;
		next;
	}
		if($flag == 1 and $check == 1){
		next;
	}
	print OUT $_;
}
close OUT;

&onExitShowRss('OK');

exit;




sub check_http_referer {
	if ($ENV{'HTTP_REFERER'}) {
		$set_url =~ s/\./\\./g;
		$SENV{'HTTP_REFERER'} = $ENV{'HTTP_REFERER'};
		$SENV{'HTTP_REFERER'} =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
		if ($SENV{'HTTP_REFERER'} !~ /$set_url/i) {
			&onExitShowRss('Called from External Sever');
		}
	}
}

sub onExitShowRss {
	
	my_funlock($lfh);

	local($_) = $_[0];
	
	if ($code eq 'euc'){
		print "Content-type: text/html; charset=euc-jp\n\n";
	}elsif ($code eq 'jis'){
		print "Content-type: text/html; charset=iso-2022-jp\n\n";
	}elsif ($code eq 'utf8'){
		print "Content-type: text/html; charset=utf-8\n\n";
	}else{
		print "Content-type: text/html; charset=shift_jis\n\n";
	}
	print $_;
	print "\n";
	
	exit;
}

sub parse_yn {

	$re_url = q{\b(?:https?|shttp)://(?:(?:[-_.!~*'()a-zA-Z0-9;:&=+$,]|%[0-9A-Fa-f} .
              q{][0-9A-Fa-f])*@)?(?:(?:[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.)} .
              q{*[a-zA-Z](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?\.?|[0-9]+\.[0-9]+\.[0-9]+\.} .
              q{[0-9]+)(?::[0-9]*)?(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f]} .
              q{[0-9A-Fa-f])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-} .
              q{Fa-f])*)*(?:/(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f} .
              q{])*(?:;(?:[-_.!~*'()a-zA-Z0-9:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*)*)} .
              q{*)?(?:\?(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])} .
              q{*)?(?:#(?:[-_.!~*'()a-zA-Z0-9;/?:@&=+$,]|%[0-9A-Fa-f][0-9A-Fa-f])*} .
              q{)?};

	my ($rss, $num) = @_;
	my @items = ();
	return unless ($rss);
	$num = 0 unless ($num =~ /^\d+$/);
	foreach my $item ($rss =~ /<div class=\"l cf\">.*?<\/div>/gis) {
		my $parsed = {};
##		if($item =~ /<li class=\"article clearFix\"><a href=\"($re_url)\"\>(.*)<\/a>/){
#		if($item =~ /<a href=\"($re_url)\" class=\"yjMt\"\>(.*)<\/a>/){
#		    $parsed->{'link'} = $1;
#		    $title = $2;
#			if($title =~ /(\<img src=\"$re_url\".*\>)/){
#				$title =~ s/$1//g;
#			}
#		    $parsed->{'title'} = $title;
#		}
#		if($item =~ /<span class=\"yjSt\">(.*?)<\/span>/){
#			$desc = $1;
#		    $parsed->{'description'} = $desc;
#		}
#		if($item =~ /<span class=\"yjSt\sdate\">(.*?)<\/span>/){
#		    $parsed->{'item_date'} = $1;
#		}
#		if($item =~ /href=\".*?\*-.*?\/\/(.*?)';\">/){
#		    $parsed->{'link'} = 'http://'.$1;
#		}
		if($item =~ /<h2 class=\"t\"><a href=\"(.*?)\">/){
		    $parsed->{'link'} = $1;
		}
		if($item =~ /<h2 class=\"t\"><a href=.*?>(.*)<\/a><\/h2>/){
		    $parsed->{'title'} = $1;
		}
		if($item =~ /<p class=\"a\">(.*?)<\/p>/){
		    $parsed->{'description'} = $1;
		}
		if($item =~ /<span class=\"d\">(.*?)<\/span>/){
		    $parsed->{'item_date'} = $1;
		}
		push(@items, $parsed);
		last if ($num and @items >= $num);
	}
	return @items;
}

sub parse_rss {
	my ($rss, $num) = @_;
	my @items = ();
	return unless ($rss);
	$num = 0 unless ($num =~ /^\d+$/);
	foreach my $item ($rss =~ /<item\b.*?>.*?<\/item>/gis) {
		my $parsed = {};
		@tag = qw(title link description);
		foreach my $tag (@tag) {
			if ($item =~ /<$tag\b.*?>(.*?)<\/$tag>/is) {
				$parsed->{$tag} = &sanitize($1);
			}
		}
		push(@items, $parsed);
		last if ($num and @items >= $num);
	}
	return @items;
}

sub parse_atom {
	my ($rss, $num) = @_;
	my @items = ();
	return unless ($rss);
	$num = 0 unless ($num =~ /^\d+$/);
	foreach my $item ($rss =~ /<entry\b.*?>.*?<\/entry>/gis) {
		my $parsed = {};
		@tag = qw(title link summary);
		foreach my $tag (@tag) {
			if ($item =~ /<$tag\b.*?>(.*?)<\/$tag>/is) {
				$parsed->{$tag} = &sanitize($1);
			}
		}
		if ($item =~ /<link\b.*?href="(.*?)".*?>/is) {
			$parsed->{link} = $1;
		}
		$parsed->{'description'} = $parsed->{'summary'};
		push(@items, $parsed);
		last if ($num and @items >= $num);
	}
	return @items;
}

sub sanitize {
	my $str = shift;
	
#	if($str =~ /&lt;\/font&gt;&lt;br&gt;&lt;font size=-1&gt;(.*)&lt;\/font&gt;&lt;\/div&gt;/){
#		$str = $1;
#	}
	
	if($str =~ /&lt;\/font&gt;&lt;br&gt;&lt;font size=-1&gt;(.*?)&lt;\/font&gt;/){
		$str = $1;
	}
	
	if ($str =~ /\Q<![CDATA[\E(.*?)\Q]]>\E/gi) {
		$str = $1;
	}
	my $re_tag_    = q{[^"'<>]*(?:"[^"]*"[^"'<>]*|'[^']*'[^"'<>]*)*(?:>|(?=<)|$(?!\n))};
	my $re_comment = '<!(?:--[^-]*-(?:[^-]+-)*?-(?:[^>-]*(?:-[^>-]+)*?)??)*(?:>|$(?!\n)|--.*$)';
	my $re_tag     = qq{$re_comment|<$re_tag_};
	$str =~ s/$re_tag//g;
	$str =~ s/&amp;/&/gi;
	my %unescaped = ('lt' => '<', 'gt' => '>', 'quot' => '"', 'apos' => "'", 'copy' => '(c)', 'amp' => '&', 'hellip' => '...');
	my %escaped = ('<' => '&lt;', '>' => '&gt;', '"' => '&quot;', '&apos;' => "'", '&' => '&amp;');
	$str =~ s/&(lt|gt|quot|apos|copy|amp|hellip);/$unescaped{$1}/gio;
	$str =~ s/([<>"'&])/$escaped{$1}/go;
	return $str;
}

sub my_flock {
	my %lfh = (dir => './lockdir/', basename => 'lockfile', timeout => 60, trytime => 10, @_);
	
	$lfh{path} = $lfh{dir} . $lfh{basename};
	
	for (my $i = 0; $i < $lfh{trytime}; $i++, sleep 1) {
		return \%lfh if (rename($lfh{path}, $lfh{current} = $lfh{path} . time));
	}
	opendir(LOCKDIR, $lfh{dir});
	my @filelist = readdir(LOCKDIR);
	closedir(LOCKDIR);
	foreach (@filelist) {
		if (/^$lfh{basename}(\d+)/) {
			return \%lfh if (time - $1 > $lfh{timeout} and rename($lfh{dir} . $_, $lfh{current} = $lfh{path} . time));
			last;
		}
	}
	undef;
}

sub my_funlock {
	rename($_[0]->{current}, $_[0]->{path});
}

