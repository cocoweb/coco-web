<?php

require_once('./qdsmtp.0.2.0a/qdsmtp.php');
require_once('./qdmail.1.2.6b/qdmail.php');

//メール設定内容
$mail_subj = "【モバイル向けサイトから出張・採寸のお申し込みです】";
//$mail_to = array('j2cmc8-offer@hdview.htdb.jp','yoidore_gragra@yahoo.co.jp');
$mail_to =array(array('offer@coco-web.com'),array('j2cmc8-offer@hdview.htdb.jp'));
$mail_from = "";
$mail_from_name = "";

//$mail_to = "yoidore_gragra@yahoo.co.jp";
//$mail_to = array("test@coco-web.com","yoidore_gragra@yahoo.co.jp");
//$mail_from = "test@coco-web.com";
//$mail_from_name = "test@coco-web.com";


$param = array(
	'host'=>'smtp.coco-web.com',//メールサーバー
	'port'=> 587 , //これはSMTPAuthの例。認証が必要ないなら　25　でＯＫ。
	'from'=>'test@coco-web.com',//Return-path: になります。
	'protocol'=>'SMTP_AUTH',// 認証が必要ないなら、'SMTP'
	'user'=>'test@coco-web.com', //SMTPサーバーのユーザーID
	'pass' => '12Go56LL', //SMTPサーバーの認証パスワード
);

//メール記載内容

$topmes = date('Y-m-d')."\r\n■■■ スマートフォン(http://www.coco-web.com/sp/)からのお問い合わせ ■■■\r\n\r\n";

//「メールに記載する項目名」:string,「フォーム上のname」:string,「フォームから送られた値が配列か否か」:bool
$postitems = array(
array("お名前","name",),
array("ふりがな","kana",),
array("電話番号","tel",),
array("メールアドレス","email",),
array("郵便番号","pcode",),
array("都道府県","address1",),
array("住所","address2",),
array("駐車場の有無","parking",),
array("ご予約日（第一希望）","date1",),
array("ご予約日（第二希望）","date2",),
array("カーテン取付先の建物種類","type",),
array("カーテン取付先の間取り","floorplan",),
array("カーテン取付予定窓数","window",),
array("お好みのカーテンスタイル","curtainstyle",true),
array("ご予算","budget",),
array("他社お見積り","estimate",),
array("その他ご質問・ご要望等","question",)
);

if(isset($_POST[$postitems[0][1]])){

$mail_from = $_POST[$postitems[3][1]];

$mail_body = $topmes;
//offer.htmlより送信されたお客様情報
foreach($postitems as $postitem){
	
	//配列で値が渡された場合の処理
	if(isset($postitem[2])){
		$val = implode(",",$_POST[$postitem[1]]);
	}else{
		$val = $_POST[$postitem[1]];
	}

	$mail_body .= "[{$postitem[0]}]\r\n";
	$mail_body .= "{$val}\r\n";

}
//print $mail_body;
/*
foreach($_POST as $id => $value){
	print "<span>{$id}</span><span>{$value}</span>\r\n";
	$mail_body = ""
}
*/
/*
foreach($_POST['curtainstyle'] as $id => $value){
	print "<span>{$id}</span><span>{$value}</span>\r\n";
}
*/
//メール作成
$mail = new Qdmail();
$mail -> smtp(true);
$mail -> smtpServer($param);
$mail -> to( $mail_to );
$mail -> subject( $mail_subj );
$mail -> text( $mail_body );
$mail -> from( $mail_from , $mail_from_name );

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>出張採寸のお申し込み - 3万人が選んだオーダーカーテン</title>
<meta name="description" content="出張採寸のお申込ページ。オーダーカーテンのお仕立て専門店カーテンココではお客様のご自宅へ無料で出張採寸、提案見積まで3窓以上無料です。">
<meta name="keywords" content="出張,採寸,申し込み,オーダーカーテン">
<link rel="canonical" href="http://www.coco-web.com/toiawase.html" />
<link rel="stylesheet" href="./css/jquery.mobile-1.4.2.css" />
<link rel="stylesheet" href="./css/jqm-datebox.min.css" />
<link rel="stylesheet" href="./css/common.css" type="text/css">
<link rel="stylesheet" href="./css/base.css" type="text/css">
<link rel="stylesheet" href="./css/underlayer.css" type="text/css">
<link rel="stylesheet" href="./css/order.css" type="text/css">
<script src="./js/jquery-1.11.0.min.js"></script>
<script src="./js/jquery.mobile-1.4.2.min.js"></script>
</head>
<body>
<div data-role="page" data-title="お申し込み・お問い合わせありがとうございました" id="hoge">
<!--header-->
<div data-role="header" data-theme="b">
<a href="http://www.coco-web.com/sp/index.html" data-ajax="false"><img src="images/common/back.png" alt="TOP" /></a><h1>出張・採寸のお申し込み</h1>
</div>
<!--content-->
<div data-role="content" data-theme="a">
<?php
//メール送信
if($mail->send()){
	//メール送信失敗
	//$message = print_r($mail->errorStatment(false),true);
	$m = "出張・採寸のお申し込みありがとうございました。<br />";
	$m .= "後程、弊社オペレータよりご連絡致します。";
}else{
	//メール送信成功
	$m = "メール送信に失敗しました。";
}
print '<p id="returnmess">'.$m.'</p>';
?>

</div>
<!--ナビゲーション ここから-->
<nav>
  <h3><img src="images/common/title_menu.gif" width="44" height="14" alt="Menu" /></h3>
  <ul id="globalNavigation">
    <li><a href="http://www.coco-web.com/sp/index.html" data-ajax="false"><span class="allow">TOP</span></a></li>

    <li><a href="http://www.coco-web.com/sp/concept.html" data-ajax="false"><span class="allow">初めての方へ</span></a></li>
    <li><a href="http://www.coco-web.com/sp/recommend.html" data-ajax="false"><span class="allow">カーテンの種類はお決まりですか？</span></a></li>
    <li><a href="http://www.coco-web.com/sp/flow.html" data-ajax="false"><span class="allow">お申し込みの流れ</span></a></li>
    <li><a href="http://www.coco-web.com/sp/voice.html" data-ajax="false"><span class="allow">お客様の声</span></a></li>
    <li><a href="http://www.coco-web.com/sp/access.html" data-ajax="false"><span class="allow">直営ショールームのご紹介</span></a></li>
    <li><a href="http://www.coco-web.com/sp/mascom.html" data-ajax="false"><span class="allow">メディア紹介</span></a></li>
    <li><a href="http://www.coco-web.com/sp/faq.html" data-ajax="false"><span class="allow">よくある質問と回答</span></a></li>
    <li><a href="http://www.coco-web.com/sp/brand.html" data-ajax="false"><span class="allow">取り扱いブランド</span></a></li>
    <li><a href="http://www.coco-web.com/sp/order.html" data-ajax="false"><span class="allow">出張採寸のお申込</span></a></li>
    <li><a href="http://www.coco-web.com/wordpress" target="_blank" data-ajax="false"><span class="allow">ココブログ</span></a></li>
  </ul>
</nav>
<!--ナビゲーション ここまで-->

<!--footer-->
<footer id="globalFooter">
<!--  <p class="pagetop"><a href="#top">PAGE TOP</a></p>-->
  <section class="footerInfo">
    <p><img src="images/common/footer_logo.png" width="96" height="33" alt="オーダーカーテンのカーテンココ" /></p>
    <p>東京ショールーム<br>〒153-0042 東京都目黒区青葉台1-15-14 3F<br><a onClick="yahoo_report_conversion(undefined);goog_report_conversion('tel:0120499455')" data-ajax="false">TEL：0120-499-455</a></p>
    <p>横浜ショールーム<br>〒231-0023 神奈川県横浜市中区山下町84-5 5F<br><a onClick="yahoo_report_conversion(undefined);goog_report_conversion('tel:0452639870')" data-ajax="false">TEL：045-263-9870</a></p>
    <p>千葉船橋ショールーム<br>〒274-0063 千葉県船橋市習志野台8-12-10<br><a onClick="yahoo_report_conversion(undefined);goog_report_conversion('tel:0474669991')" data-ajax="false">TEL：047-466-9991</a></p>
  </section>
  <p class="copyright">copyright(c)2000-2014 Curtain COCO  All Rights Reserved.</p>
</footer>
<!--Google電話発信タグ-->
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 1072435630;
    w.google_conversion_label = "zcrwCLTt9AgQrqOw_wM";
    w.google_conversion_value = 0;
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script>

<!--Yahoo電話発信タグ-->
<script type="text/javascript">
  /* <![CDATA[ */
  yahoo_snippet_vars = function() {
    var w = window;
    w.yahoo_conversion_id = 1000014956;
    w.yahoo_conversion_label = "BI2tCOTKilUQjPvOzAM";
    w.yahoo_conversion_value = 0;
    w.yahoo_remarketing_only = false;
  }
  // IF YOU CHANGE THE CODE BELOW, THIS CONVERSION TAG MAY NOT WORK.
  yahoo_report_conversion = function(url) {
    yahoo_snippet_vars();
    window.yahoo_conversion_format = "3";
    window.yahoo_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
      if (typeof(url) != 'undefined') {
        window.location = url;
      }
    }
    var conv_handler = window['yahoo_trackConversion'];
    if (typeof(conv_handler) == 'function') {
      conv_handler(opt);
    }
  }
/* ]]> */
</script>
<script type="text/javascript"
  src="http://i.yimg.jp/images/listing/tool/cv/conversion_async.js">
</script>

<!--リマケとリタゲ-->
<script type="text/javascript" language="javascript">
var yahoo_retargeting_id = 'G3VD4STT2B';
var yahoo_retargeting_label = '';
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>


<!-- Google Code for &#12522;&#12510;&#12540;&#12465;&#12486;&#12451;&#12531;&#12464; &#12479;&#12464; -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1072435630;
var google_conversion_label = "5YHxCJSH-AQQrqOw_wM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1072435630/?value=0&amp;label=5YHxCJSH-AQQrqOw_wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


</div>
</body>
</html>
<?php } ?>
